package com.restau.proyect

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.loadImageBitmap
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*
import com.restau.proyect.IOD.Context.close
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.router.Navegacion
import com.restau.proyect.theme.DragAndDropTheme
import com.restau.proyect.viewmodel.ScreenModel
import com.restau.proyect.views.Screen
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.awt.Dimension
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.inputStream


@Composable
@Preview
fun FrameWindowScope.App(viewModelbase: ScreenModel) {
    this.window.minimumSize= Dimension(800,600)
    this.window.preferredSize= Dimension(800,600)
    Navegacion()
}

@OptIn(FlowPreview::class)
fun main() {
    val version = System.getProperty("app.version") ?: "Development"
    application {
        val screenModel: ScreenModel by viewmodels()
        val status by screenModel.status.collectAsState(false)
        val state by remember { mutableStateOf(WindowState(WindowPlacement.Maximized)) }
        if (status) {
            Window(onCloseRequest = {
                close()
                exitApplication()
            }, icon = appIcon, state = state, title = " Desktop sample $version") {
                LaunchedEffect(state) {
                    snapshotFlow { state.size }
                        .debounce(1000)
                        .onEach {
                            if (it.height.value < 500) {
                                state.size = it.copy(height = 500.dp)
                            }
                            if (it.width.value < 800) {
                                state.size = it.copy(width = 800.dp)
                            }
                        }
                        .launchIn(this)

                }
                DragAndDropTheme {
                    App(screenModel)
                }
            }
        } else {
            Window(
                onCloseRequest = ::exitApplication, state = WindowState(
                    WindowPlacement.Floating, position = WindowPosition.Aligned(
                        Alignment.Center
                    ), height = 500.dp, width = 500.dp, isMinimized = false
                ), enabled = false, undecorated = true, resizable = false, alwaysOnTop = false, visible = true
            ) {
                Screen()
            }
        }
    }
}


private val appIcon: Painter? by lazy {
    // app.dir is set when packaged to point at our collected inputs.
    val appDirProp = System.getProperty("app.dir")
    val appDir = appDirProp?.let { Path.of(it) }
    // On Windows we should use the .ico file. On Linux, there's no native compound image format and Compose can't render SVG icons,
    // so we pick the 128x128 icon and let the frameworks/desktop environment rescale.
    var iconPath = appDir?.resolve("app.ico")?.takeIf { it.exists() }
    iconPath = iconPath ?: appDir?.resolve("icon-square-128.png")?.takeIf { it.exists() }
    if (iconPath?.exists() == true) {
        BitmapPainter(iconPath.inputStream().buffered().use { loadImageBitmap(it) })
    } else {
        null
    }
}