package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Plato")
data class Plato(
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    override var id: Int? = null,
    @field:SerializedName("precio")
    var precio: Double? = null,
    @field:SerializedName("nombre")
    var nombre: String? = null,
    @field:SerializedName("descripcion")
    var descripcion: String? = null,
    @SerializedName("tiempo_estimado")
    var tiempo_estimado: Int? = null,
    @field:SerializedName("categoria")
    @OneToOne
    @JoinColumn(name = "id_categoria")
    var categoria: Categoria? = null,
    @field:SerializedName("idRestaurant")
    @ManyToOne
    @JoinColumn(name = "id_restaurant", referencedColumnName = "id")
    var idRestaurant: Restaurant? = null,
    @field:SerializedName("listplatos")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idplato")
    val listplatos: List<DetallePlatos>? = null,
) : BaseEntity, Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}