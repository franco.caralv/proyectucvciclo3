package com.restau.proyect.usecase.login.events

import com.restau.proyect.data.Person

sealed class LoginEvent {

    class Find(val person: Person) : LoginEvent()
    object ErrorFind : LoginEvent()


}
