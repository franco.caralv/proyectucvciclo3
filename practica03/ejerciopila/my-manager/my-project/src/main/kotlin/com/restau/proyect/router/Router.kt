package com.restau.proyect.router

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.ui.graphics.vector.ImageVector

sealed class Router(open val url: String, open val title: String, val flujo: Flujo) {

    fun castToModuleLogin(): ModuleLogin {
        if (this is ModuleLogin) {
            return this
        }
        throw Exception("Error no se puede hacer el cast module")
    }

    fun castToModuloSingle(): ModuloSingle? {
        if (this is ModuloSingle) {
            return this
        }
        return null
    }

    sealed class ModuleLogin(override val url: String, override val title: String) :
        Router(url, title, flujo = Flujo.Login)

    object Login : ModuleLogin("login", "login")
    object Registro : ModuleLogin("Registro", "Registro")
    sealed class ModuloSingle(override val url: String, override val title: String, val icon: ImageVector?) :
        Router(url, title, flujo = Flujo.Parque)

    object Parque : ModuloSingle("Parque", "Inicio", Icons.Outlined.RealEstateAgent)
    object Dasboard : ModuloSingle("Dasboard", "Dasboard", Icons.Outlined.Dashboard)
    object CrearCuenta : ModuloSingle("CrearCuenta", "Creacion de mozos", Icons.Outlined.PersonAdd)
    object CrearPlato : ModuloSingle("CrearPlato", "Platos y Modificadores", Icons.Outlined.Coffee)
    object ModificarPerson : ModuloSingle("ModificarPerson", "Modifica tus datos", Icons.Outlined.Update)

}

enum class Flujo {
    Login,
    Parque
}
