package com.restau.proyect.IOD

import androidx.compose.runtime.*
import com.restau.proyect.MyAppConfig
import com.restau.proyect.base.ViewModelbase
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext

object Context {
    val value: ConfigurableApplicationContext = AnnotationConfigApplicationContext(MyAppConfig::class.java)

    inline fun <reified T> viewmodel(): T {
        return value.getBean(T::class.java)
    }

    fun close() {
        value.close()
    }


}

@Composable
inline fun <reified T : ViewModelbase> viewmodels(): MutableState<T> {
    val state = remember { mutableStateOf(Context.viewmodel<T>()) }
    DisposableEffect(Unit) {
        onDispose {
            state.value.destroy()
        }
    }
    return state
}
