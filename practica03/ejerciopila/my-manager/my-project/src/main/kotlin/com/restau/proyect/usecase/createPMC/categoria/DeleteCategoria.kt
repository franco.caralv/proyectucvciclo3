package com.restau.proyect.usecase.createPMC.categoria

import com.restau.proyect.base.UseCaseBase
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Categoria
import com.restau.proyect.dominan.remote.CategoriaDB
import com.restau.proyect.usecase.createPMC.event.ResponseDeleteCategoria
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class DeleteCategoria(
    @Autowired private val liscategoriaPreferent: CategoriaDB,
) : UseCaseBase<Categoria, ResponseDeleteCategoria> {


    override suspend fun invoke(data: Categoria): ResponseDeleteCategoria {
        val value = liscategoriaPreferent.deleteCategoria(data)
        if (value is Customer.onSuccess) {
            ResponseDeleteCategoria.success(value.info)
        }
        return ResponseDeleteCategoria.Error
    }


}