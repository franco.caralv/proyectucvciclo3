package com.restau.proyect.usecase.dialog.Addmesa.Event

import com.restau.proyect.data.Mesa

sealed class MesasEvent {
    class mesaSucess(val mesa: Mesa) : MesasEvent()

    class mesasError(val mensaje: String) : MesasEvent()

}