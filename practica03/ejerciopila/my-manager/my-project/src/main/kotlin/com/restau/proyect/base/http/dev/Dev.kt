package com.restau.proyect.base.http.dev

import com.restau.proyect.base.http.ApiConfig
import com.restau.proyect.base.http.BaseClient
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.pueba
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Component

@Component
@Lazy
class Dev : ApiConfig {

    @Autowired
    lateinit var baseClient: BaseClient
    override suspend fun primerapeticion(
        onProcess: (suspend (response: Customer<List<pueba>>) -> Unit)?
    ): Customer<List<pueba>> {
        val responseType = object : ParameterizedTypeReference<List<pueba>>() {

        }
        baseClient.post("", pueba(""), responseType)
        return baseClient.get("mock", responseType) {
            if (it.statusCodeValue in 200..300) {
                onProcess?.invoke(Customer.onSuccess(it.body!!))
            }
            onProcess?.invoke(
                Customer.onError(
                    com.restau.proyect.base.http.Respnse.Error(
                        it.statusCodeValue,
                        it.body?.toString()
                    )
                )
            )

        }
    }


}
