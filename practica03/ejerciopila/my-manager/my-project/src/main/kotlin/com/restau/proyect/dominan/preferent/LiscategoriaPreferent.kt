package com.restau.proyect.dominan.preferent

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Categoria
import com.restau.proyect.dominan.preferent.extends.generarId
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
@Lazy
class LiscategoriaPreferent(
    @Autowired private val usuariopreferent: PersonLogin
) {
    @Value("\${app.secretKey}")
    private lateinit var key: String

    lateinit var listpersonpre: StorageService<List<Categoria>>

    private val _listCategoria = MutableStateFlow<List<Categoria>>(listOf())

    val listCategoria = _listCategoria.asStateFlow()

    private val nameDocumento = "categoria"

    private var fileocumento = "listcategorias"

    @PostConstruct
    fun cargandoModificadores() {
        fileocumento = "$fileocumento${usuariopreferent.person?.id ?: ""}"
        listpersonpre = StorageService(object : TypeToken<List<Categoria>>() {}, nameDocumento, key)
        listpersonpre.load(fileocumento)?.let {
            _listCategoria.value = it
        }


    }

    fun add(element: Categoria): Customer<List<Categoria>> {
        return try {
            _listCategoria.value = _listCategoria.value.toMutableList().apply {
                element.id=this.generarId()
                add(element)
            }
            listpersonpre.save(_listCategoria.value, fileocumento)
            Customer.onSuccess(_listCategoria.value)
        } catch (e: Exception) {
            Customer.onError(Error(400))
        }

    }



    fun deleteCategoria(categoria: Categoria): Customer<List<Categoria>>{
        return try {
            _listCategoria.value=  _listCategoria.value.toMutableList().filter { it.id!=categoria.id }.toList()
            listpersonpre.save(_listCategoria.value, fileocumento)
            Customer.onSuccess(_listCategoria.value)
        }catch (e:Exception){
            Customer.onError(Error(400))
        }

    }


}