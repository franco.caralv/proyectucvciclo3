package com.restau.proyect.usecase.parque

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.dominan.remote.ClienteDB
import com.restau.proyect.usecase.parque.events.SearchClientEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class SearchCliente(
    @Autowired private val clienteDB: ClienteDB
) {


    suspend operator fun invoke(dni: String): SearchClientEvent {
        val resultado = clienteDB.searchClienteForDni(dni)
        when (resultado) {
            is Customer.onSuccess -> {
                val cliente = resultado.info
                return SearchClientEvent.SuccessClient(cliente)
            }

            is Customer.onError -> {
                val error = resultado.error
                if (error.code == 500) {
                    return SearchClientEvent.BDError
                }
                return SearchClientEvent.ClientNodFound
            }
        }
    }

}