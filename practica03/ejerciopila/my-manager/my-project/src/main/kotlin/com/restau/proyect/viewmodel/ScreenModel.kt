package com.restau.proyect.viewmodel

import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.router.Router
import com.restau.proyect.usecase.screen.ValidadSreenRouter
import com.restau.proyect.util.Constans
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import javax.annotation.PostConstruct


@Controller
@org.springframework.context.annotation.Lazy
@Scope("prototype")
class ScreenModel(
    @Autowired private val validadSreenRouter: ValidadSreenRouter
) : ViewModelbase() {

    private val _status = MutableSharedFlow<Boolean>()
    val status = _status.asSharedFlow()

    private val _router = MutableStateFlow<Router>(Router.Login)
    val router = _router.asStateFlow()

    @PostConstruct
    fun init() = launch {
        _status.emit(false)
        _router.value = validadSreenRouter()
        delay(Constans.TIMEDELAY)
        _status.emit(true)
    }


}