package com.restau.proyect.usecase.createPMC.base

import com.restau.proyect.data.Modificador
import kotlinx.coroutines.flow.Flow

interface SearchModificador {

    operator fun invoke(): Flow<List<Modificador>>
}