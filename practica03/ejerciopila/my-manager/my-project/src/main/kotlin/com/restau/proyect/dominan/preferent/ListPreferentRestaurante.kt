package com.restau.proyect.dominan.preferent

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Person
import com.restau.proyect.data.Restaurant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
class ListPreferentRestaurante(
    @Autowired private val personLogin: PersonLogin,
    @Autowired private val listmesasPreferent: ListmesasPreferent
) {

    @Value("\${app.secretKey}")
    private lateinit var key: String

    private lateinit var restaurantpreferent: StorageService<Restaurant>

    lateinit var restaurant: Restaurant
        private set

    private lateinit var listrestaurantpreferent: StorageService<List<Restaurant>>

    private lateinit var listrestaurant: List<Restaurant>

    private val filname = "restaurante"
    private val listfilname = "listRestaurantes"

    @PostConstruct
    private fun caragando() {
        val persona = personLogin.person
        val listmesa = listmesasPreferent.listmesas
        restaurantpreferent =
             StorageService<Restaurant>(object : TypeToken<Restaurant>() {}, "listrestaurante", key)
        listrestaurantpreferent =
            StorageService<List<Restaurant>>(object : TypeToken<List<Restaurant>>() {}, "listrestaurante", key)
        persona?.let {
            restaurantpreferent.load("$filname${persona?.id}")?.let {
                restaurant = it
            } ?: run {
                restaurant = Restaurant(
                    1,
                    "Restaurante de ${persona?.name} ",
                    persona.ciudad,
                    persona,
                    listOf()
                )
                restaurantpreferent.save(restaurant, "$filname${persona?.id}")
            }
            listrestaurantpreferent.load("$listfilname${persona?.id}")?.let {
                listrestaurant = it
            } ?: run {
                listrestaurant = listOf(restaurant)
                listrestaurantpreferent.save(listrestaurant, listfilname)
            }
        }
    }


    fun save(persona: Person) {
        restaurantpreferent.load("$filname${persona.id}")?.let {
            restaurant = it
        } ?: run {
            restaurant = Restaurant(
                1,
                "Restaurante de ${persona.name} ",
                persona.ciudad,
                persona,
                listOf(),
                listOf()
            )
            restaurantpreferent.save(restaurant, "$filname${persona.id}")
        }
        listrestaurantpreferent.load("$listfilname${persona.id}")?.let {
            listrestaurant = it
        } ?: run {
            listrestaurant = listOf(restaurant)
            listrestaurantpreferent.save(listrestaurant, "$listfilname${persona.id}")
        }
    }


}