package com.restau.proyect.usecase.createAcount

import com.restau.proyect.data.Ciudad
import org.springframework.stereotype.Service


@Service
class CreateAcountSearchCiudad {


    operator fun invoke(): List<Ciudad> {

        return listOf(Ciudad.LIMA, Ciudad.CHICLAYO, Ciudad.TRUJILLO, Ciudad.PIURA)
    }

}