package com.restau.proyect.components

import androidx.compose.material.*
import androidx.compose.runtime.*
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.router.Controller
import com.restau.proyect.router.Router
import com.restau.proyect.viewmodel.DrawerModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


@Composable
fun ScaffoldSearch(
    content: @Composable () -> Unit
) {
    val drawerModel: DrawerModel by viewmodels()

    val scaffoldState = rememberScaffoldState(
        drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    )
    LaunchedEffect(Unit){
        delay(200)
        scaffoldState.drawerState.close()
    }

    val scope = rememberCoroutineScope()
    val navigationItems = listOf(
        Router.Parque,
        Router.Dasboard,
        Router.CrearCuenta,
        Router.CrearPlato,
        Router.ModificarPerson
    )

    val controller= remember { Controller }
    val elemento by controller.state.collectAsState()


    Scaffold(
        scaffoldState = scaffoldState,
        isFloatingActionButtonDocked = false,
        floatingActionButtonPosition = FabPosition.End,

        topBar = {
            TopBar(
                scope,
                scaffoldState,
                elemento.title
            )
        },
        drawerContent = {
            Drawer(scope, scaffoldState, items = navigationItems,
                onExit = {
                    GlobalScope.launch {
                        drawerModel.closesesion()
                    }
                })
        },
        drawerGesturesEnabled = false,

        ) {
        content()
    }

}