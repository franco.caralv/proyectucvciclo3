package com.restau.proyect.events.dialog.flujoAddMesa

import com.restau.proyect.data.Mesa

sealed class AddMesaFlujo {

    class Guardar(val onRequest: () -> Unit) : AddMesaFlujo()

    class ListenerMesa(val mesa: Mesa) : AddMesaFlujo()


}