package com.restau.proyect.usecase.createUser.Mozos

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Mozo
import com.restau.proyect.dominan.remote.MozosDB
import com.restau.proyect.usecase.createUser.event.EventAddMozo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class AddMozos(
    @Autowired private val mozosDB: MozosDB
) {

    suspend operator fun invoke(mozo: Mozo): EventAddMozo {
        val response = mozosDB.saveMozo(mozo)
        return when (response) {
            is Customer.onError -> {
                EventAddMozo.FaildMozo
            }

            is Customer.onSuccess -> {
                EventAddMozo.SuccessSaveMozo
            }
        }
    }

}