package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Ciudad")
data class Ciudad(
    @field:SerializedName("name")
    @Column
    val name: String? = null,
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int? = null,
    @field:SerializedName("codigo")
    @Column
    val codigo: String? = null,

    ) : Model() {
    companion object {
        val PIURA = Ciudad("Piura", 1, "20001")
        val LIMA = Ciudad("LIMA", 2, "20001")
        val TRUJILLO = Ciudad("Trujillo", 3, "20001")
        val CHICLAYO = Ciudad("Chiclayo", null, "20001")

    }

    override fun toString(): String {
        return toStringPrimitivo()
    }


}
