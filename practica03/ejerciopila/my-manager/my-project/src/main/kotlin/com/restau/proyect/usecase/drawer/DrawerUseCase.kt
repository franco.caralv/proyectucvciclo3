package com.restau.proyect.usecase.drawer

import com.restau.proyect.dominan.preferent.PersonLogin
import com.restau.proyect.router.Controller
import com.restau.proyect.router.Router
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class DrawerUseCase(
    @Autowired private val personLogin: PersonLogin
) {

    suspend operator fun invoke() {
        personLogin.delete()
        Controller.clear(Router.Login)
    }


}