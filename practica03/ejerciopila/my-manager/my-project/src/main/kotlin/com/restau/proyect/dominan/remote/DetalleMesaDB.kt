package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.DetalleMesa
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository


@Repository
@Lazy
class DetalleMesaDB(
    @Autowired private val restaurantDB: RestaurantDB,
    @Autowired val db: Database
) {


    suspend fun searchDetallemesaBymesa(idDetallemesa: Int, idMesa: Int): Customer<DetalleMesa?> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val detallemesa = db.find(DetalleMesa::class.java).where().eq("id", idDetallemesa).and().eq("id_mesass", idMesa).findOne()
                Customer.onSuccess(detallemesa)
            } catch (e: Exception) {
                Customer.onError(Error(500))
            }
        }




}