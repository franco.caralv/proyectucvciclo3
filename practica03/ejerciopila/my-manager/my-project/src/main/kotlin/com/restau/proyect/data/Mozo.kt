package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Mozo")
data class Mozo(
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    override var id: Int? = null,
    @field:SerializedName("nombre")
    var nombre: String? = null,
    @field:SerializedName("estado")
    var estado: String? = null,
    @field:SerializedName("tiempo")
    var tiempo: String? = null,
    @field:SerializedName("hora_entrada")
    var hora_entrada: String? = null,
    @field:SerializedName("hora_salida")
    var hora_salida: String? = null,
    @field:SerializedName("puntaje")
    var puntaje: String? = null,
    @field:SerializedName("totalpuntaje")
    var totalpuntaje: String? = null,
    @field:SerializedName("listdetallemesa")
    @OneToMany( mappedBy = "mozo")
    val listdetallemesa: List<DetalleMesa>? = null,
    @field:SerializedName("idRestaurant")
    @ManyToOne
    @JoinColumn(name = "id_restaurant", referencedColumnName = "id")
    var idRestaurant: Restaurant? = null,
) : BaseEntity, Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}



