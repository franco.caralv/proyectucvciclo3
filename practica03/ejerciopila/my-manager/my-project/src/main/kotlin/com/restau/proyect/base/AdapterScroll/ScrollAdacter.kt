package com.restau.proyect.base.AdapterScroll

import androidx.compose.foundation.ScrollbarAdapter
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import kotlin.math.abs

@Composable
fun rememberScrollbarGridAdapter(
    scrollState: LazyGridState, rows: Int
): ScrollbarAdapter {
    return remember(scrollState, rows) {
        ScrollbarAdapter(scrollState, rows)
    }
}


fun ScrollbarAdapter(
    scrollState: LazyGridState, rows: Int
): ScrollbarAdapter = LazyScrollbarGridAdapter(
    scrollState, rows
)

private class LazyScrollbarGridAdapter(
    private val scrollState: LazyGridState,
    private val rows: Int
) : ScrollbarAdapter {
    override val scrollOffset: Float
        get() = scrollState.firstVisibleItemIndex * averageItemSize +
                scrollState.firstVisibleItemScrollOffset


    override suspend fun scrollTo(containerSize: Int, scrollOffset: Float) {
        val distance = scrollOffset - this@LazyScrollbarGridAdapter.scrollOffset

        // if we scroll less than containerSize we need to use scrollBy function to avoid
        // undesirable scroll jumps (when an item size is different)
        //
        // if we scroll more than containerSize we should immediately jump to this position
        // without recreating all items between the current and the new position
        if (abs(distance) <= containerSize) {
            scrollState.scrollBy(distance)
        } else {
            snapTo(containerSize, scrollOffset)
        }
    }

    private suspend fun snapTo(containerSize: Int, scrollOffset: Float) {
        // In case of very big values, we can catch an overflow, so convert values to double and
        // coerce them
//        val averageItemSize = 26.000002f
//        val scrollOffsetCoerced = 2.54490608E8.toFloat()
//        val index = (scrollOffsetCoerced / averageItemSize).toInt() // 9788100
//        val offset = (scrollOffsetCoerced - index * averageItemSize) // -16.0
//        println(offset)

        val maximumValue = maxScrollOffset(containerSize).toDouble()
        val scrollOffsetCoerced = scrollOffset.toDouble().coerceIn(0.0, maximumValue)
        val averageItemSize = averageItemSize.toDouble()

        val index = (scrollOffsetCoerced / averageItemSize)
            .toInt()
            .coerceAtLeast(0)
            .coerceAtMost(itemCount - 1)

        val offset = (scrollOffsetCoerced - index * averageItemSize)
            .toInt()
            .coerceAtLeast(0)

        scrollState.scrollToItem(index = index, scrollOffset = offset)
    }


    override fun maxScrollOffset(containerSize: Int) =
        (averageItemSize * itemCount - containerSize).coerceAtLeast(0f)

    private val itemCount get() = scrollState.layoutInfo.totalItemsCount

    private val averageItemSize by derivedStateOf {
        scrollState
            .layoutInfo
            .visibleItemsInfo
            .asSequence()
            .map {
                it.size.height / rows
            }.average()
            .toFloat()
    }
}
