package com.restau.proyect.base.http

import com.restau.proyect.base.http.Respnse.Customer
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.ResponseEntity

interface BaseClient {

    suspend fun <T> get(
        url: String,
        responseType: ParameterizedTypeReference<T>,
        onProcess: (suspend (response: ResponseEntity<T>) -> Unit)? = null
    ): Customer<T>

    suspend fun <T, K> post(
        url: String,
        objrequest: T,
        responseType: ParameterizedTypeReference<K>,
        onProcess: (suspend (response: ResponseEntity<K>) -> Unit)? = null
    ): Customer<K>


}