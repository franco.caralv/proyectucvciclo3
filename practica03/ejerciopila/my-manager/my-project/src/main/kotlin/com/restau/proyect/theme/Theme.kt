package com.restau.proyect.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val MyTheme = lightColors(
    primary = greend,
    onPrimary = Tundora,
    secondary = Bali_hai,
    onSecondary = Color.Black,
    background = Color.White,
    primaryVariant = Tundora,
    error = Copper_rust,

)


@Composable
fun DragAndDropTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = MyTheme
    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}