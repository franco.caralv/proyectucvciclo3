package com.restau.proyect.usecase.createPMC.plato

import com.restau.proyect.base.UseCaseBase
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Plato
import com.restau.proyect.dominan.remote.PlatosDB
import com.restau.proyect.usecase.createPMC.event.ResponseDeletePlato
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class DeletePlato(
    @Autowired private val listPreferentPlato: PlatosDB
) : UseCaseBase<Plato, ResponseDeletePlato> {


    override suspend fun invoke(data: Plato): ResponseDeletePlato {
        val value= data.id?.let { listPreferentPlato.deletePlato(it) }
        if (value is Customer.onSuccess){

            return ResponseDeletePlato.Succes()
        }

        return ResponseDeletePlato.Error
    }


}