package com.restau.proyect.usecase.createPMC.event

sealed class ResponseDeletePlato {

    object Error:ResponseDeletePlato()
    class Succes():ResponseDeletePlato()

}