package com.restau.proyect.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.window.WindowDraggableArea
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.window.WindowScope
import com.restau.proyect.theme.greend
import com.restau.proyect.util.Constans.LOGOSINFONDO
import com.restau.proyect.util.Constans.LOGOSINFONDODESICRIPCION

@Composable
fun WindowScope.Screen() = WindowDraggableArea {
    Image(
        painter = painterResource(LOGOSINFONDO),
        LOGOSINFONDODESICRIPCION,
        modifier = Modifier.fillMaxWidth().fillMaxHeight().background(greend)
    )
}