package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Plato
import io.ebean.Database
import io.ebean.FetchConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class PlatosDB(
    @Autowired private val restaurant: RestaurantDB,
    @Autowired val db: Database
) {


    private val _listCategoria: MutableStateFlow<List<Plato>> = MutableStateFlow<List<Plato>>(listOf())
    val listCategoria: StateFlow<List<Plato>> = _listCategoria.asStateFlow()


    @PostConstruct
    fun init() {
        _listCategoria.value = listplatosbyuser(restaurant.restaurant?.id)
    }

    suspend fun guardarPlato(plato: Plato): Customer<List<Plato>> = withContext(Dispatchers.IO) {
        return@withContext try {

            plato.idRestaurant = restaurant.restaurant
            db.save(plato)
            _listCategoria.value = listplatosbyuser(restaurant.restaurant?.id)
            Customer.onSuccess(listCategoria.value)
        } catch (e: Exception) {
            Customer.onError(Error(500, mensaje = e.message))
        }

    }

    private fun listplatosbyuser(it: Int?): List<Plato> {
        return it?.let { it1 ->
            try {
                db.find(Plato::class.java).fetch("categoria", FetchConfig.ofQuery()).where().eq(
                    "t0.id_restaurant",
                    it1
                ).findList()
            } catch (e: Exception) {
                listOf()
            }
        } ?: listOf()
    }


    suspend fun deletePlato(id: Int): Customer<List<Plato>> = withContext(Dispatchers.IO) {
        return@withContext try {
            db.delete(Plato::class.java, id)
            _listCategoria.value = listplatosbyuser(restaurant.restaurant?.id)
            Customer.onSuccess(listCategoria.value)
        } catch (e: Exception) {
            Customer.onError(Error(500))
        }
    }
}