package com.restau.proyect.usecase.createPMC.categoria

import com.restau.proyect.data.Categoria
import com.restau.proyect.dominan.remote.CategoriaDB
import com.restau.proyect.usecase.createPMC.base.SearchCategoria
import kotlinx.coroutines.flow.Flow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class SearchCategoriaImp(
    @Autowired private val liscategoriaPreferent: CategoriaDB
):SearchCategoria {
    override fun invoke(): Flow<List<Categoria>> {
        return liscategoriaPreferent.listCategoria
    }
}