package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "DetalleMesa")
data class DetalleMesa (
    @field:SerializedName("id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name ="id")
    val id: Int? = null,
    @field:SerializedName("idmesa")
    @ManyToOne
    @JoinColumn(name = "id_mesa", referencedColumnName = "id")
    @Column(name ="id_mesa")
    var idmesa: Mesa? = null,
    @field:SerializedName("idcliente")
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    @Column(name ="id_cliente")
    var idcliente: Cliente? = null,
    @field:SerializedName("mozo")
    @ManyToOne
    @JoinColumn(name = "id_mozo", referencedColumnName = "id")
    @Column(name ="id_mozo")
    var mozo: Mozo? = null,
    @field:SerializedName("idpedido")
    @ManyToOne
    @JoinColumn(name = "id_pedido", referencedColumnName = "id")
    @Column(name ="id_pedido")
    val idpedido: Pedido? = null,
): Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}