package com.restau.proyect.util

import org.mindrot.jbcrypt.BCrypt
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import javax.swing.ImageIcon
import javax.swing.JOptionPane
import kotlin.random.Random

@Service
@Scope("singleton")
@org.springframework.context.annotation.Lazy
class Utility {

    fun isdev() = (System.getProperty("app.version") ?: "true") == "true"

    fun isprod()= !isdev()

    fun createIdhas() = randomString(Random.nextInt(0, 100)).sha256()

    fun createhas(string: String) = string.sha256()


    fun randomString(length: Int): String {
        val charPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        return (1..length).asSequence()
            .map { Random.nextInt(0, charPool.length) }
            .map(charPool::get)
            .toList()
            .joinToString("")

    }

    private fun String.sha256(): String {
        return BCrypt.hashpw(this, BCrypt.gensalt())
    }


    fun comparatehas(userInput: String, hashedPassword: String): Boolean = BCrypt.checkpw(userInput, hashedPassword)

    fun showMessage(string: String) {
        JOptionPane.showMessageDialog(null, string)
    }

    fun showMessageError(string: String, title: String = "") {
        JOptionPane.showMessageDialog(null, string, title, JOptionPane.ERROR)
    }


    fun showMessageAdvertencia(string: String, title: String = "") {
        JOptionPane.showMessageDialog(null, string, title, JOptionPane.WARNING_MESSAGE)
    }

    fun showMessageAdvertenciaInFileds(string: String, field: String = "") {
        showMessageAdvertencia(string, "El campo $field necesita datos")
    }


    fun showEleccion(string: String, title: String = "",onYes:()->Unit,onNo:()->Unit,onCancelar:()->Unit){
        val icon=ImageIcon()
        val respuesta= JOptionPane.showConfirmDialog(null,string,title,JOptionPane.YES_NO_CANCEL_OPTION,
            JOptionPane.INFORMATION_MESSAGE,)
        when (respuesta) {
            0 -> onYes()
            1 -> onNo()
            2 -> onCancelar()
        }
    }

    fun showAcceptarCancelar(string: String, title: String = "",onOk:()->Unit,onCancelar:()->Unit){
        val icon=ImageIcon()
        val respuesta= JOptionPane.showConfirmDialog(null,string,title,JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.INFORMATION_MESSAGE,)
        when (respuesta) {
            0 -> onOk()
          //1 -> onNo()
            2 -> onCancelar()
        }
    }


}