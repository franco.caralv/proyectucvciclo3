package com.restau.proyect.dominan.preferent


import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Person
import com.restau.proyect.util.Utility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
class ListPersonPreferent(
    @Autowired private val utility: Utility
) {


    @Value("\${app.secretKey}")
    lateinit var key: String

    lateinit var listpersonpre: StorageService<List<Person>>

    lateinit var listperson: List<Person>

    val filname = "listadepersonas"

    @PostConstruct
    private fun caragando() {
        listpersonpre =
            StorageService<List<Person>>(object : TypeToken<List<Person>>() {}, "listperson", key)
        listpersonpre.load(filname)?.let {
            listperson = it
        } ?: run {
            listperson = listOf()
        }
    }


    suspend fun add(person: Person): Customer<List<Person>> = withContext(Dispatchers.IO) {
        return@withContext try {
            person.id = listperson.size + 1
            person.password = utility.createhas(person.password ?: "")
            val list = listperson.toMutableList()
            list.find { it.password == person.password && person.correo == it.correo }?.let {
                return@withContext Customer.onError(Error(500, "Correo duplicado"))
            } ?: run {
                list.add(person)
                listpersonpre.save(list, filname)
                listperson = list
                Customer.onSuccess(listperson)
            }
        } catch (e: Exception) {
            Customer.onError(Error(500, e.message))
        }
    }


}