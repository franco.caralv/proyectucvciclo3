package com.restau.proyect.events.login

import androidx.compose.material.SnackbarHostState
import com.restau.proyect.data.Person

sealed class LoginEven {

    class EvenTexField(val person: Person) : LoginEven()
    object RegistraPersona : LoginEven()
    class SearchPersona(val person: Person, val snackbarHostState: SnackbarHostState) : LoginEven()

}