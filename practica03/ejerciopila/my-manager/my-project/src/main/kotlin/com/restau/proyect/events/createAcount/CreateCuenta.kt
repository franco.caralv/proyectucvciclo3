package com.restau.proyect.events.createAcount

import androidx.compose.material.SnackbarHostState
import com.restau.proyect.data.Person


sealed class CreateCuenta {

    class validarCorreo(val correo: String) : CreateCuenta()
    class Crear(val person: Person, val snackbarHostState: SnackbarHostState) : CreateCuenta()
    class ValidarPassword(val pass: String, val passnew: String) : CreateCuenta()
    object Loading : CreateCuenta()
    object None : CreateCuenta()
    class ListTex(val person: Person) : CreateCuenta()
}