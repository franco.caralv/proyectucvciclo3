package com.restau.proyect.usecase.parque

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.DetalleMesa
import com.restau.proyect.data.Mesa
import com.restau.proyect.dominan.remote.DetalleMesaDB
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class SearchDetalleMesa(
    @Autowired private val detalleMesaDB: DetalleMesaDB
) {


    suspend operator fun invoke(mesa: Mesa): DetalleMesa {
        return mesa.idDetalleTemp?.let { idetalle ->
            mesa.id?.let { mesaid ->
                val resutaldo = detalleMesaDB.searchDetallemesaBymesa(idetalle, mesaid)
                when (resutaldo) {
                    is Customer.onSuccess -> {
                        resutaldo.info ?: DetalleMesa()
                    }

                    is Customer.onError -> {
                        DetalleMesa()
                    }
                }
            } ?: DetalleMesa()
        } ?: DetalleMesa()

    }


}