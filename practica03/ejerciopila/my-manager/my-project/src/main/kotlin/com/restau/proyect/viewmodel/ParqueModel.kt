package com.restau.proyect.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.data.*
import com.restau.proyect.entity.ParamsStyledTable
import com.restau.proyect.events.parque.ParqueEvent
import com.restau.proyect.mappers.toParamsStyle
import com.restau.proyect.mappers.toPlatoParamsStyle
import com.restau.proyect.usecase.createPMC.base.SearchModificador
import com.restau.proyect.usecase.createPMC.base.SearchPlato
import com.restau.proyect.usecase.parque.*
import com.restau.proyect.usecase.parque.events.CountPedidoEvent
import com.restau.proyect.usecase.parque.events.SaveClientEvent
import com.restau.proyect.usecase.parque.events.SearchClientEvent
import com.restau.proyect.util.Utility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import javax.annotation.PostConstruct


@Controller
@org.springframework.context.annotation.Lazy
@Scope("prototype")
class ParqueModel(
    @Autowired private val listmesasUseCase: ListmesasUseCase,
    @Autowired private val utility: Utility,
    @Autowired private val searchCliente: SearchCliente,
    @Autowired private val searchMozoUseCase: SearchMozoUseCase,
    @Autowired private val saveClientUseCase: SaveClientUseCase,
    @Autowired private val searchModificador: SearchModificador,
    @Autowired private val searchPlato: SearchPlato,
    @Autowired private val pedidoCountUseCase: PedidoCountUseCase,
    @Autowired private val searchDetalleMesa: SearchDetalleMesa
) : ViewModelbase() {


    private val _listmesa = MutableStateFlow<List<Mesa>>(listOf())
    val listmesa = _listmesa.asStateFlow()


    private val _listmozos = MutableStateFlow<List<Mozo>>(listOf())
    val listmozos = _listmozos.asStateFlow()

    private val _listplatos = MutableStateFlow<List<Plato>>(listOf())
    val listplatos = _listplatos.asStateFlow()

    private val _listmodificador = MutableStateFlow<List<Modificador>>(listOf())
    val listmodificador = _listmodificador.asStateFlow()


    private val _listplatosadd = MutableStateFlow<List<Plato>>(listOf())
    val listplatosadd = _listplatosadd.asStateFlow()

    private val _listmodificadoradd = MutableStateFlow<List<Modificador>>(listOf())
    val listmodificadoradd = _listmodificadoradd.asStateFlow()

    private val _mesa = MutableStateFlow<Mesa?>(null)
    val mesa = _mesa.asStateFlow()

    private val _detalleMesa = MutableStateFlow<DetalleMesa?>(null)
    val detalleMesa = _detalleMesa.asStateFlow()

    private val _pedido = MutableStateFlow<Pedido?>(null)
    val pedido = _pedido.asStateFlow()

    private val _cliente = MutableStateFlow(Cliente())
    val cliente = _cliente.asStateFlow()

    var isCreateUser by mutableStateOf(false)

    var precioestimado by mutableStateOf(0.0)
    var tiempoestimado by mutableStateOf(0)


    val tablemodificador = MutableStateFlow<ParamsStyledTable?>(null)
    val tableplato = MutableStateFlow<ParamsStyledTable?>(null)

    private val _mozo = MutableStateFlow("")
    val mozo = _mozo.asStateFlow()

    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    @PostConstruct
    fun envidado() {
        listmesasUseCase().flowOn(Dispatchers.IO).onEach {
            _listmesa.value = it
        }.launchIn(lyceviewmodel)
        mozo.debounce(200L).flatMapLatest { searchMozoUseCase(it) }.flowOn(Dispatchers.IO).onEach {
            _listmozos.value = it
        }.launchIn(lyceviewmodel)
        searchModificador.invoke().onEach {
            _listmodificador.value = it
        }.launchIn(lyceviewmodel)
        searchPlato().onEach {
            _listplatos.value = it
        }.launchIn(lyceviewmodel)
        _listmodificadoradd.onEach {
            searchModificadorListener(it)
        }.launchIn(lyceviewmodel)
        _listplatosadd.onEach {
            searchPlatoListener(it)
        }.launchIn(lyceviewmodel)
    }

    private suspend fun loadingpedido() {
        val resultado = pedidoCountUseCase()
        when (resultado) {
            is CountPedidoEvent.SuccessCount -> {
                _pedido.value = _pedido.value?.copy(nombre = "Pedido #${resultado.count}")
            }

            is CountPedidoEvent.ErrorDB -> {

                _pedido.value = _pedido.value?.copy(nombre = "Pedido #0")
            }
        }
    }


    suspend fun validarEvent(it: ParqueEvent) {
        when (it) {
            is ParqueEvent.AddModificador -> {
                _listmodificadoradd.value = _listmodificadoradd.value.plus(it.detalleModificar)
            }

            is ParqueEvent.AddPlatos -> {
                _listplatosadd.value = _listplatosadd.value.plus(it.detalleplatos)
            }

            is ParqueEvent.MovientoLista -> {
                _listmesa.value = it.list
            }

            is ParqueEvent.DetallePedido -> {
                _detalleMesa.value = _detalleMesa.value?.copy(idmesa = _mesa.value, idcliente = _cliente.value)
                if (_detalleMesa.value?.mozo == null) {
                    utility.showMessage("Falta un mozo")
                    return
                }
                if (_detalleMesa.value?.idmesa == null) {
                    utility.showMessage("Falta una mesa")
                    return
                }
                if (_detalleMesa.value?.idcliente == null) {
                    utility.showMessage("Falta una cliente")
                    return
                }


            }

            is ParqueEvent.SelecMesa -> {
                _mesa.value = it.mesa
                if (it.mesa?.estadobj != EstadoMesa.Desocupado) {
                    it.mesa?.let { mesa ->
                        val detallemesa = searchDetalleMesa(mesa)
                        _detalleMesa.value = detallemesa
                        _listplatosadd.value = detallemesa.idpedido?.listplatos?.mapNotNull { it.idplato } ?: listOf()
                        _listmodificadoradd.value =
                            detallemesa.idpedido?.listmodificador?.mapNotNull { it.idModificador } ?: listOf()
                        _pedido.value = detallemesa.idpedido
                        _pedido.value?.obtenerTiempoEstimado()?.let { _tiempoestimado ->
                            tiempoestimado = _tiempoestimado
                        }
                        _pedido.value?.obtenerprecio()?.let { _precioestimado ->
                            precioestimado = _precioestimado
                        }
                    } ?: run {
                        _listplatosadd.value = listOf()
                        _listmodificadoradd.value = listOf()
                        _pedido.value = Pedido()
                        _detalleMesa.value = DetalleMesa()
                    }
                } else {
                    _listplatosadd.value = listOf()
                    _listmodificadoradd.value = listOf()
                    _pedido.value = Pedido()
                    _mesa.value = _mesa.value?.copy(mozo = null, idDetalleTemp = null)
                    _detalleMesa.value = DetalleMesa()
                }


            }

            is ParqueEvent.SelectUser -> {
                _cliente.value = it.usuario
            }

            is ParqueEvent.ChangeDni -> {
                isCreateUser = false
            }

            is ParqueEvent.SelectMozo -> {
                _detalleMesa.value = _detalleMesa.value?.copy(mozo = it.mozo)
            }

            is ParqueEvent.ChangePedido -> {
                if (it.isactivo) {
                    loadingpedido()
                } else {
                    _listplatosadd.value = listOf()
                    _listmodificadoradd.value = listOf()

                }
                tiempoestimado = _listplatosadd.value.mapNotNull { it.tiempo_estimado }
                    .sum() + _listmodificadoradd.value.mapNotNull { it.tiempo_estimado }.sum()
                precioestimado = _listplatosadd.value.mapNotNull { it.precio }
                    .sum() + _listmodificadoradd.value.mapNotNull { it.precio }.sum()

            }

            is ParqueEvent.SaveClient -> {
                val resultado = saveClientUseCase(_cliente.value)
                when (resultado) {
                    is SaveClientEvent.SuccesSaveClient -> {
                        isCreateUser = false
                        _cliente.value = resultado.cliente
                        utility.showMessage("En hora buena se creo al cliente")
                    }

                    is SaveClientEvent.ErrorClient -> {
                        utility.showMessage("Error al crear al cliente")
                    }
                }

            }

            is ParqueEvent.CountDni -> {
                val resultado = searchCliente(it.dni)
                when (resultado) {
                    is SearchClientEvent.SuccessClient -> {
                        utility.showMessage("¡En hora buena se encontro al cliente!")
                        _cliente.value = resultado.cliente
                    }

                    is SearchClientEvent.BDError -> {
                        utility.showMessage("Error al crear al usuario llamar a servicio tecnico")
                    }

                    is SearchClientEvent.ClientNodFound -> {
                        utility.showAcceptarCancelar(string = "¿Cliente no encontrado desea crear uno nuevo?",
                            title = "Crear Usuario",
                            onOk = {
                                isCreateUser = true
                                utility.showMessage("¡Genera tu nuevo cliente!")
                            },
                            onCancelar = {
                                utility.showMessage("Debe de crear un usuario para seguir con el flujo")
                            })
                    }
                }

            }

            is ParqueEvent.SearchMozo -> {
                _mozo.value = it.nombre
            }

        }


    }


    private fun searchPlatoListener(listplato: List<Plato>) {
        tableplato.value = listplato.toPlatoParamsStyle { indexfila, data ->
            val element = listplato.getOrNull(indexfila)
            element?.let {
                launch {
                    _listplatosadd.value = listplato.minus(it)
                }
            }
        }
        if (listplato.isEmpty())
            tableplato.value = null
    }

    private fun searchModificadorListener(listamod: List<Modificador>) {
        tablemodificador.value = listamod.toParamsStyle { indexfila, data ->
            val element = listamod.getOrNull(indexfila)
            element?.let {
                launch {
                    _listmodificadoradd.value = listamod.minus(it)
                }
            }
        }
        if (listamod.isEmpty())
            tablemodificador.value = null
    }


    fun emit(it: ParqueEvent) = launch {
        validarEvent(it)
    }


}