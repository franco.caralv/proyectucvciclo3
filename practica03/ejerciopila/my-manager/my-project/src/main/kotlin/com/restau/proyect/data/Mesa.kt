package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Mesa")
data class Mesa(
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null,
    @field:SerializedName("nombre")
    var nombre: String? = null,
    @Transient
    @field:SerializedName("mozo")
    var mozo: Mozo? = null,
    @field:SerializedName("estado")
    var estado: String? = null,
    @field:SerializedName("numerosillas")
    var numerosillas: String? = null,
    @field:SerializedName("listmesas")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idmesa")
    var listmesas: List<DetalleMesa>? = null,
    @field:SerializedName("idDetalleTemp")
    var idDetalleTemp: Int? = null,
    @field:SerializedName("tiempodeespera")
    var tiempodeespera: String? = null,
    @field:SerializedName("tiempoestemimado")
    var tiempoestemimado: String? = null,
    @field:SerializedName("idRestaurant")
    @ManyToOne
    @JoinColumn(name = "id_restaurant", referencedColumnName = "id")
    var idRestaurant: Restaurant? = null,
): Model() {

    @Transient
    private val hasestado = listOf(EstadoMesa.Ocupado, EstadoMesa.Desocupado, EstadoMesa.Limpiando)

    override fun toString(): String {
        return toStringPrimitivo()
    }

    val estadobj: EstadoMesa?
        get() {
            estado?.let {
                return hasestado.firstOrNull {
                    it.id == estado
                }
            }
            return null
        }


}