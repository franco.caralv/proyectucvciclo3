package com.restau.proyect.usecase.createPMC.event

import com.restau.proyect.data.Categoria

sealed class ResponseDeleteCategoria {

    object Error:ResponseDeleteCategoria()
    class success(list: List<Categoria>):ResponseDeleteCategoria()

}