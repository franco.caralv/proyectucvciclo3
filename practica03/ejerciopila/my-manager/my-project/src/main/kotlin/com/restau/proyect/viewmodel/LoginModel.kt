package com.restau.proyect.viewmodel

import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.data.Person
import com.restau.proyect.events.login.LoginEven
import com.restau.proyect.router.Controller
import com.restau.proyect.router.Router
import com.restau.proyect.usecase.login.CreatePersonLogin
import com.restau.proyect.usecase.login.SearchPasswordAndEmail
import com.restau.proyect.usecase.login.events.LoginEvent
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import javax.annotation.PostConstruct


@Scope("prototype")
@org.springframework.stereotype.Controller
@org.springframework.context.annotation.Lazy
class LoginModel(
    @Autowired private val search: SearchPasswordAndEmail,
    @Autowired private val createPersonLogin: CreatePersonLogin
) : ViewModelbase() {

    private val _person = MutableStateFlow<Person>(Person("", ""))
    val persom = _person.asStateFlow()
    private val _event = MutableSharedFlow<LoginEven>()
    val event = _event.asSharedFlow()


    @PostConstruct
    private fun escuchadodeeven() = launch {
        event.collect(::oyentedeeventos)
    }

    private suspend fun oyentedeeventos(it: LoginEven) {
        when (it) {
            is LoginEven.EvenTexField -> {
                _person.emit(it.person)
            }

            is LoginEven.RegistraPersona -> {
                Controller.plus(Router.Registro)
            }

            is LoginEven.SearchPersona -> {
                when (val value = search(it.person)) {
                    is LoginEvent.ErrorFind -> {
                        it.snackbarHostState.showSnackbar("Error no se encontro a la persona")
                    }

                    is LoginEvent.Find -> {
                        createPersonLogin(value.person)
                        it.snackbarHostState.showSnackbar("En hora buena se encontro a la persona")
                        Controller.clear(Router.Parque)
                    }
                }

            }
        }
    }

    fun events(it: LoginEven) = launch {
        _event.emit(it)
    }


}