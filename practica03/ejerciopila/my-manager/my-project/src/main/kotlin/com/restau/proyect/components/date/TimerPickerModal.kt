package com.restau.proyect.components.date

import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.LockClock
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.awt.SwingPanel
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.raven.swing.TimePicker
import javax.swing.BoxLayout
import javax.swing.JPanel

@Composable
fun TimerPickerModal(
    modifier: Modifier = Modifier,
    onclick: (datos: String) -> Unit,
    onEvent: ((it: String) -> Unit)? = null
) {
    val jPanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.Y_AXIS)
    }
    IconButton({
        if (jPanel!=null)
        TimePicker().apply {

            addEventTimePicker {
                onEvent?.invoke(it)
            }
            addActionListener {
                if (it.actionCommand == "OK") {
                    onclick(selectedTime)
                }
            }
        }.showPopup(jPanel, jPanel.x, jPanel.y)
    }, modifier = modifier) {
        Icon(Icons.Outlined.LockClock, "")
    }
    SwingPanel(
        background = Color.White,
        modifier = Modifier.size(1.dp).focusable(false),
        factory = {
            jPanel
        }
    )

}



