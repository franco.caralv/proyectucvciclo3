package com.restau.proyect.base

interface UseCaseBase<T, K> {

    suspend operator fun invoke(data: T): K

}