package com.restau.proyect.data

import com.google.gson.annotations.SerializedName

data class pueba(
    @SerializedName("createdAt")
    val createdAt: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("avatar")
    val avatar: String? = null,
    @SerializedName("id")
    val id: String? = null
)