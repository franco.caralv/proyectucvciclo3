package com.restau.proyect.usecase.parque

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Cliente
import com.restau.proyect.dominan.remote.ClienteDB
import com.restau.proyect.usecase.parque.events.SaveClientEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class SaveClientUseCase(
    @Autowired private val clienteDB: ClienteDB
) {


    suspend operator fun invoke(cliente: Cliente): SaveClientEvent {
        val respuesta = clienteDB.saveCliente(cliente)
        return when (respuesta) {
            is Customer.onSuccess -> {
                SaveClientEvent.SuccesSaveClient(respuesta.info)
            }

            is Customer.onError -> {
                SaveClientEvent.ErrorClient
            }
        }
    }
}


