package com.restau.proyect.usecase.createAcount.event

import com.restau.proyect.data.Person

sealed class PersonListEvent {


    class PersonError(val error: String) : PersonListEvent()

    class PersonSuccess(val data: Person) : PersonListEvent()

}
