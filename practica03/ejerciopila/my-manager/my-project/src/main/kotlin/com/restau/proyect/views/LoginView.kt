package com.restau.proyect.views

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.components.TexfieldPassword
import com.restau.proyect.components.TextFieldEmail
import com.restau.proyect.events.login.LoginEven
import com.restau.proyect.theme.greend
import com.restau.proyect.util.Constans.ICON_RESTAURAN
import com.restau.proyect.util.Constans.ICON_RESTAURANDESCRIPCION
import com.restau.proyect.util.Constans.LOGOSINFONDO
import com.restau.proyect.util.Constans.LOGOSINFONDODESICRIPCION
import com.restau.proyect.util.Constans.LOGOSINFONDOYTITULO
import com.restau.proyect.util.Constans.LOGOSINFONDOYTITULODESCRIPCION
import com.restau.proyect.viewmodel.LoginModel

@Composable
@Preview
fun LoginView() {
    val loginModel: LoginModel by viewmodels()
    val snackbarHostState = remember { SnackbarHostState() }
    val personstate by loginModel.persom.collectAsState()
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        val with = maxWidth.value
        SnackbarHost(
            hostState = snackbarHostState,
            modifier = Modifier.align(Alignment.BottomCenter)
        )
        if (with > 800)
            Row(modifier = Modifier.fillMaxSize()) {
                Image(
                    painterResource(LOGOSINFONDO), LOGOSINFONDODESICRIPCION, alignment = Alignment.Center,
                    modifier = Modifier.fillMaxWidth().fillMaxHeight().weight(2f).padding(vertical = 20.dp)
                )
                Card(
                    elevation = 15.dp,
                    shape = RoundedCornerShape(15.dp),
                    modifier = Modifier.weight(1f).padding(25.dp).fillMaxSize()
                ) {
                    Column(
                        modifier = Modifier.fillMaxSize().padding(20.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Image(
                            painterResource(ICON_RESTAURAN), ICON_RESTAURANDESCRIPCION,
                            modifier = Modifier.heightIn(25.dp, 150.dp).widthIn(25.dp, 150.dp)
                        )
                        Spacer(Modifier.height(100.dp))
                        createformulario(personstate.correo ?: "", personstate.password ?: "", loginModel, {
                            loginModel.events(LoginEven.EvenTexField(personstate.copy(password = it)))
                        }, { loginModel.events(LoginEven.EvenTexField(personstate.copy(correo = it))) }, {
                            loginModel.events(LoginEven.SearchPersona(personstate, snackbarHostState))
                        })
                    }
                }

            }
        else
            Column(modifier = Modifier.fillMaxSize().padding(20.dp)) {
                Image(
                    painterResource(LOGOSINFONDOYTITULO), LOGOSINFONDOYTITULODESCRIPCION, alignment = Alignment.Center,
                    modifier = Modifier.fillMaxWidth().weight(2f).padding(vertical = 20.dp)
                )
                createformulario(personstate.correo ?: "", personstate.password ?: "", loginModel, {
                    loginModel.events(LoginEven.EvenTexField(personstate.copy(password = it)))
                }, { loginModel.events(LoginEven.EvenTexField(personstate.copy(correo = it))) }, {
                    loginModel.events(LoginEven.SearchPersona(personstate, snackbarHostState))
                })
            }


    }


}


@OptIn(ExperimentalComposeUiApi::class)
@Composable
private fun createformulario(
    name: String,
    password: String,
    loginModel: LoginModel,
    onPasswod: (it: String) -> Unit,
    onName: (it: String) -> Unit,
    onEnter: () -> Unit
) {

    TextFieldEmail(name, "Name") {
        onName(it)
    }
    TexfieldPassword(password, "Password") {
        onPasswod(it)
    }
    Button(
        {
            onEnter()
        },
        modifier = Modifier.fillMaxWidth()
    ) {
        Text("Login")
    }
    Box(modifier = Modifier.fillMaxWidth(), Alignment.TopEnd) {
        Text(
            "Crear Cuenta",
            color = greend,
            modifier = Modifier.clickable {
                loginModel.events(LoginEven.RegistraPersona)
            }
        )
    }
}

