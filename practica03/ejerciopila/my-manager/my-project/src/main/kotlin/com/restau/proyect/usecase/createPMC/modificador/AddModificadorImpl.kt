package com.restau.proyect.usecase.createPMC.modificador

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Modificador
import com.restau.proyect.dominan.remote.ModificadordDB
import com.restau.proyect.dominan.remote.RestaurantDB
import com.restau.proyect.usecase.createPMC.base.AddModificador
import com.restau.proyect.usecase.createPMC.event.ResponseModificador
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service


@Service
@Primary
@Lazy
class AddModificadorImpl(
    @Autowired private val lisModificadoresPreferent: ModificadordDB,
    @Autowired private val listPreferentRestaurante: RestaurantDB
) : AddModificador {


    override suspend fun invoke(modificador: Modificador): ResponseModificador {
        errorSinData(modificador)?.let {
            return ResponseModificador.ErrorSinData(it)
        }
        val data = lisModificadoresPreferent.add(modificador)
        return when (data) {
            is Customer.onError -> {
                ResponseModificador.ErrorData
            }

            is Customer.onSuccess -> {
                val filtrado = data.info
                ResponseModificador.SuccessData(filtrado)
            }
        }

    }

    private fun errorSinData(modificador: Modificador): String? {
        if (modificador.descripcion.isNullOrEmpty()) {
            return "Debe tener descripcion o llenar el campo como NA"
        }
        if (modificador.nombre.isNullOrEmpty()) {
            return "Debe tener nombre "
        }
        if (modificador.precio == 0.0 || modificador.precio == null) {
            return "Debe tener precio "
        }
        return null
    }


}