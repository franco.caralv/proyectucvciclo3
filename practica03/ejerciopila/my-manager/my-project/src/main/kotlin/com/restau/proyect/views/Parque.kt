package com.restau.proyect.views

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.HideImage
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogState
import androidx.compose.ui.window.rememberDialogState
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.base.AdapterScroll.rememberScrollbarGridAdapter
import com.restau.proyect.components.AutoCompleteTextField
import com.restau.proyect.components.ParamsData
import com.restau.proyect.components.h.H2
import com.restau.proyect.components.h.H3
import com.restau.proyect.components.h.H4
import com.restau.proyect.components.p.P
import com.restau.proyect.components.table.StyledTable
import com.restau.proyect.data.*
import com.restau.proyect.events.parque.ParqueEvent
import com.restau.proyect.theme.*
import com.restau.proyect.util.Constans
import com.restau.proyect.viewmodel.ParqueModel
import kotlinx.coroutines.Dispatchers
import org.burnoutcrew.reorderable.*


@Composable
@Preview
fun Parque() {

    var columns by remember { mutableStateOf(4) }
    val stateVertical = rememberLazyListState(0)
    val viewmodel: ParqueModel by viewmodels()
    val listmesas by viewmodel.listmesa.collectAsState(Dispatchers.Unconfined)
    val mesaselect by viewmodel.mesa.collectAsState(Dispatchers.Default)
    val mesa by viewmodel.cliente.collectAsState()
    val listMozos by viewmodel.listmozos.collectAsState()

    var verpedido by remember { mutableStateOf(false) }
    val listState =
        rememberReorderableLazyGridState(dragCancelledAnimation = NoDragCancelledAnimation(), onMove = { from, to ->
            val listanew = listmesas.toMutableList().apply {
                add(to.index, removeAt(from.index))
            }
            viewmodel.emit(ParqueEvent.MovientoLista(listanew))
        })

    Column(modifier = Modifier.fillMaxSize()) {
        Row(modifier = Modifier.weight(1f).fillMaxSize()) {
            Box(
                modifier = Modifier.weight(if (mesaselect != null) 0.6f else 1f).fillMaxSize()
            ) {
                LazyVerticalGrid(
                    modifier = Modifier.fillMaxSize().onSizeChanged {
                        columns = (it.width / 300)
                    }.reorderable(listState).detectReorderAfterLongPress(listState),
                    columns = GridCells.Fixed(columns), // Define el número de columnas
                    state = listState.gridState,
                    contentPadding = PaddingValues(16.dp),
                ) {
                    itemsIndexed(listmesas, { index, item -> index }) { index, item ->
                        // Agrega tu contenido aquí, como un Card o Image
                        ReorderableItem(listState,
                            index = index,
                            key = index,
                            modifier = Modifier.fillMaxWidth().clickable {
                                viewmodel.emit(ParqueEvent.SelecMesa(item))
                            }) { isDragging ->
                            val elevation = animateDpAsState(if (isDragging) 16.dp else 0.dp)
                            Card(
                                modifier = Modifier.padding(10.dp).height(200.dp).widthIn(100.dp, 400.dp)
                                    .shadow(elevation.value),
                                elevation = 20.dp,
                                backgroundColor = item.estadobj?.color ?: greend,
                                shape = RoundedCornerShape(5.dp)
                            ) {
                                Column(modifier = Modifier.fillMaxSize().padding(5.dp)) {
                                    Icon(
                                        painterResource(Constans.TABLERESTAURANT),
                                        "table",
                                        modifier = Modifier.fillMaxWidth().align(Alignment.CenterHorizontally)
                                    )
                                    Text(
                                        "La mesa #${item.id}",
                                        textAlign = TextAlign.Center,
                                        modifier = Modifier.fillMaxWidth(),
                                        fontFamily = FontFamily.POPPINS(),
                                    )
                                    Icon(
                                        item.estadobj?.icon ?: Icons.Default.HideImage,
                                        "",
                                        Modifier.fillMaxWidth().align(Alignment.CenterHorizontally)
                                    )

                                    if (item.mozo != null) Text(
                                        buildAnnotatedString {
                                            append("El Mozo :")
                                            withStyle(style = SpanStyle(color = Color.DarkGray)) {
                                                append(" '${item.mozo?.nombre}' ")
                                            }
                                        },
                                        maxLines = 1, softWrap = false, overflow = TextOverflow.Ellipsis,
                                        fontFamily = FontFamily.POPPINS(),

                                        )

                                    if (!item.tiempodeespera.isNullOrBlank())
                                        Text(
                                            buildAnnotatedString {
                                                append("El Tiempo de espera :")
                                                withStyle(style = SpanStyle(color = Color.DarkGray)) {
                                                    append(" ${item.tiempodeespera} minutos")
                                                }
                                            },
                                            maxLines = 1,
                                            softWrap = false,
                                            fontFamily = FontFamily.POPPINS(),
                                            overflow = TextOverflow.Ellipsis
                                        )
                                    if (!item.tiempoestemimado.isNullOrBlank()) Text(
                                        buildAnnotatedString {
                                            append("El Tiempo de espera :")
                                            withStyle(style = SpanStyle(color = Color.DarkGray)) {
                                                append(" ${item.tiempoestemimado} minutos")
                                            }
                                        },
                                        maxLines = 1,
                                        softWrap = false,
                                        fontFamily = FontFamily.POPPINS(),
                                        overflow = TextOverflow.Ellipsis
                                    )
                                    Text(
                                        buildAnnotatedString {
                                            append("Nombre de mesa :")
                                            withStyle(style = SpanStyle(color = Color.DarkGray)) {
                                                append(" ${item.nombre ?: ""} ")
                                            }
                                        },
                                        fontFamily = FontFamily.POPPINS(),
                                    )
                                    Text(
                                        buildAnnotatedString {
                                            append("El Estado :")
                                            withStyle(style = SpanStyle(color = Color.DarkGray)) {
                                                append(" ${item.estadobj?.estado ?: ""} ")
                                            }
                                        },
                                        fontFamily = FontFamily.POPPINS(),
                                    )
                                    Text(
                                        buildAnnotatedString {
                                            append("Numero de Sillas")
                                            withStyle(style = SpanStyle(color = Color.DarkGray)) {
                                                append(" num ${item.numerosillas ?: ""} ")
                                            }
                                        },
                                        fontFamily = FontFamily.POPPINS(),
                                    )

                                }
                            }
                        }

                    }

                }
                VerticalScrollbar(
                    modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
                    adapter = rememberScrollbarGridAdapter(listState.gridState, columns)
                )
            }
            if (mesaselect != null) Card(
                modifier = Modifier.weight(0.4f).padding(20.dp).background(Color.White).fillMaxWidth().fillMaxHeight(),
                elevation = 20.dp,
                shape = RoundedCornerShape(10.dp)
            ) {
                Box(
                    modifier = Modifier.fillMaxSize().padding(10.dp)
                ) {
                    LazyColumn(
                        Modifier.fillMaxSize().padding(end = 12.dp), stateVertical,
                    ) {
                        item {
                            Row(
                                modifier = Modifier.fillParentMaxWidth(),
                                horizontalArrangement = Arrangement.SpaceBetween
                            ) {
                                Spacer(Modifier)
                                Icon(Icons.Filled.Close, "", Modifier.size(35.dp).clickable {
                                    viewmodel.emit(ParqueEvent.SelecMesa(null))
                                })
                            }
                            H2(
                                "Informacion General",
                                color = greendvariante,
                                textAlign = TextAlign.Center,
                                modifier = Modifier.fillParentMaxWidth().padding(5.dp)
                            )
                            P(
                                buildAnnotatedString {
                                    append("El tiempo estimado: ")
                                    withStyle(
                                        style = SpanStyle(
                                            azul
                                        )
                                    ) {
                                        append("${viewmodel.tiempoestimado} minutos")
                                    }
                                },
                                textAlign = TextAlign.Center,
                                maxLines = 1,
                                modifier = Modifier.fillParentMaxWidth().padding(5.dp)
                            )
                            P(
                                buildAnnotatedString {
                                    append("El pago total: ")
                                    withStyle(
                                        style = SpanStyle(
                                            azul
                                        )
                                    ) {
                                        append("${viewmodel.precioestimado}")
                                    }
                                },
                                textAlign = TextAlign.Center,
                                maxLines = 1,
                                modifier = Modifier.fillParentMaxWidth().padding(5.dp)
                            )
                            if (!mesaselect?.tiempodeespera.isNullOrBlank())
                                P(
                                    buildAnnotatedString {
                                        append("El tiempo Transcurrido: ")
                                        withStyle(
                                            style = SpanStyle(
                                                rojointenso
                                            )
                                        ) {
                                            append(" ${mesaselect?.tiempodeespera ?: ""} minutos")
                                        }
                                    },
                                    textAlign = TextAlign.Center,
                                    maxLines = 1,
                                    modifier = Modifier.fillParentMaxWidth().padding(5.dp)
                                )
                            TextField(mesaselect?.numerosillas ?: "", {

                            }, label = {
                                Text("Numero de Sillas", fontFamily = FontFamily.POPPINS())
                            }, modifier = Modifier.fillMaxWidth().padding(5.dp))
                            ModalByClienteShow(viewmodel, mesa)
                            AutoCompleteTextField(
                                ParamsData(
                                    listMozos,
                                    label = "Mozo",
                                    onItemSelected = {
                                        viewmodel.emit(ParqueEvent.SelectMozo(it))
                                    },
                                    atributo = Mozo::nombre,
                                    onSearch = {
                                        viewmodel.emit(ParqueEvent.SearchMozo(it))
                                    },
                                    modifier = Modifier.fillParentMaxWidth().padding(5.dp)
                                )
                            )
                            AutoCompleteTextField(
                                ParamsData(
                                    listOf(
                                        EstadoMesa.Desocupado,
                                        EstadoMesa.Limpiando,
                                        EstadoMesa.Molesto,
                                        EstadoMesa.Ocupado,
                                        EstadoMesa.Sucio
                                    ),
                                    label = "Estado",
                                    onItemSelected = {

                                    },
                                    atributo = EstadoMesa::estado,
                                    onSearch = {},
                                    modifier = Modifier.fillParentMaxWidth().padding(5.dp)
                                )
                            )
                            Button(
                                {
                                    viewmodel.emit(ParqueEvent.ChangePedido(true))
                                    verpedido = true
                                },
                                modifier = Modifier.fillParentMaxWidth().padding(5.dp),
                                shape = RoundedCornerShape(5.dp)
                            ) {
                                Text("Ver detalle de pedido", fontFamily = FontFamily.POPPINS())
                            }
                            Button(
                                {

                                },
                                modifier = Modifier.fillParentMaxWidth().padding(5.dp),
                                shape = RoundedCornerShape(5.dp)
                            ) {
                                Text("Guardar", fontFamily = FontFamily.POPPINS())
                            }


                        }
                    }

                    VerticalScrollbar(
                        modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
                        adapter = rememberScrollbarAdapter(stateVertical)
                    )
                }
            }

        }
    }
    if (verpedido)
        CrearPedido(viewmodel, verpedido, {
            verpedido = !verpedido
        })
}


@Composable
fun CrearPedido(
    viewmodel: ParqueModel,
    visible: Boolean,
    onRequest: () -> Unit,
    state: DialogState = rememberDialogState(
        width = 800.dp,
        height = 700.dp,
    ),
) {
    val listmodificar by viewmodel.listmodificador.collectAsState()
    val listplatos by viewmodel.listplatos.collectAsState()
    val stateVertical = rememberLazyListState(0)
    val pedido by viewmodel.pedido.collectAsState()
    val tablemodificador by viewmodel.tablemodificador.collectAsState()
    val tableplato by viewmodel.tableplato.collectAsState()
    Dialog(
        {
            viewmodel.emit(ParqueEvent.ChangePedido(false))
            onRequest()
        },
        state,
        visible, undecorated = false, resizable = false,
        title = "Agrega tus pedidos"
    ) {
        Box(
            modifier = Modifier.fillMaxSize().padding(10.dp)
        ) {
            LazyColumn(
                Modifier.fillMaxSize().padding(end = 12.dp), stateVertical,
            ) {
                item {
                    H3(pedido?.nombre ?: "")
                    Row {
                        AutoCompleteTextField(
                            ParamsData(
                                listplatos,
                                label = "Plato",
                                onItemSelected = {
                                    viewmodel.emit(ParqueEvent.AddPlatos(it))
                                },
                                atributo = Plato::nombre,
                                onSearch = {},
                                modifier = Modifier.weight(1f).padding(5.dp),
                                clearfield = true
                            )
                        )
                        AutoCompleteTextField(
                            ParamsData(
                                listmodificar,
                                label = "Modificador",
                                onItemSelected = {
                                    viewmodel.emit(ParqueEvent.AddModificador(it))
                                },
                                atributo = Modificador::nombre,
                                onSearch = {

                                },
                                modifier = Modifier.weight(1f).padding(5.dp),
                                clearfield = true
                            )
                        )
                    }
                }
                item {
                    Card(
                        shape = RoundedCornerShape(15.dp),
                        elevation = 25.dp,
                        modifier = Modifier.padding(15.dp)
                    ) {
                        Column {
                            H3(
                                "Lista de Platos",
                                modifier = Modifier.fillParentMaxWidth().padding(10.dp),
                                textAlign = TextAlign.Center
                            )
                            tableplato?.let {
                                StyledTable(it)
                            } ?: run {
                                Box(
                                    modifier = Modifier.fillParentMaxWidth().background(Color.LightGray).padding(20.dp)
                                ) {
                                    H4("Se encuentra vacio")
                                }
                            }
                        }

                    }
                    Card(
                        shape = RoundedCornerShape(15.dp),
                        elevation = 25.dp,
                        modifier = Modifier.padding(15.dp)
                    ) {
                        Column {
                            H3(
                                "Lista de Modificares",
                                modifier = Modifier.fillParentMaxWidth().padding(10.dp),
                                textAlign = TextAlign.Center
                            )
                            tablemodificador?.let {
                                StyledTable(it)
                            } ?: run {
                                Box(
                                    modifier = Modifier.fillParentMaxWidth().background(Color.LightGray).padding(20.dp)
                                ) {

                                    H4("Se encuentra vacio")
                                }
                            }

                        }

                    }
                }
                item {
                  Box(modifier = Modifier.fillParentMaxWidth()) {
                      Button({
                          viewmodel.emit(ParqueEvent.ChangePedido(true))
                          onRequest()
                      }, modifier = Modifier.align(Alignment.CenterEnd)){
                          P("Guardar")
                      }
                  }
                }
            }
            VerticalScrollbar(
                modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
                adapter = rememberScrollbarAdapter(stateVertical)
            )
        }
    }
}


@Composable
private fun LazyItemScope.ModalByClienteShow(
    viewmodel: ParqueModel,
    mesa: Cliente
) {
    AnimatedVisibility(viewmodel.isCreateUser) {
        Spacer(
            modifier = Modifier.Companion.fillParentMaxWidth().height(3.dp).background(Color.Gray.copy(alpha = 0.5f))
        )
    }

    TextField(mesa.dni ?: "", { newValue ->
        if (newValue.isEmpty() || newValue.all { it.isDigit() }) {
            if (newValue.length < 9) {
                viewmodel.emit(ParqueEvent.SelectUser(mesa.copy(dni = newValue)))
            }
            if (newValue.length == 8)
                viewmodel.emit(ParqueEvent.CountDni(newValue))
            else
                viewmodel.emit(ParqueEvent.ChangeDni)
        }
    }, label = {
        Text("DNI", fontFamily = FontFamily.POPPINS())
    }, modifier = Modifier.fillMaxWidth().padding(5.dp))
    AnimatedVisibility(viewmodel.isCreateUser) {
        Column {
            TextField(mesa.nombre ?: "", {
                viewmodel.emit(ParqueEvent.SelectUser(mesa.copy(nombre = it)))
            }, label = {
                Text("Nombre Cliente", fontFamily = FontFamily.POPPINS())
            }, modifier = Modifier.fillMaxWidth().padding(5.dp))
            TextField(mesa.numero_telefonico ?: "", { newValue ->
                if (newValue.isEmpty() || newValue.all { it.isDigit() }) {
                    viewmodel.emit(ParqueEvent.SelectUser(mesa.copy(numero_telefonico = newValue)))
                }
            }, label = {
                Text("Numero telefonico", fontFamily = FontFamily.POPPINS())
            }, modifier = Modifier.fillMaxWidth().padding(5.dp))
            Button({
                viewmodel.emit(ParqueEvent.SaveClient)
            }, modifier = Modifier.Companion.fillParentMaxWidth()) {
                P("Guardar Cliente")
            }
            Spacer(
                modifier = Modifier.Companion.fillParentMaxWidth().height(3.dp)
                    .background(Color.Gray.copy(alpha = 0.5f))
            )
        }
    }
}


