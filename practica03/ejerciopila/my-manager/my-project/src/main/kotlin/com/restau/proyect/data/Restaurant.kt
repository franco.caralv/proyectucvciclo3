package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Restaurant")
data class Restaurant(
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int? = null,
    @field:SerializedName("name")
    val name: String? = null,
    @field:SerializedName("ciudad")
    @OneToOne
    val ciudad: Ciudad? = null,
    @field:SerializedName("person_id")
    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    var persona: Person? = null,
    @field:SerializedName("mesas")
    @OneToMany(cascade = [CascadeType.ALL])
    val listmesas: List<Mesa>? = null,
    @field:SerializedName("listmozos")
    @OneToMany(cascade = [CascadeType.ALL])
    val listmozos: List<Mozo>? = null,
    @field:SerializedName("listPlato")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idRestaurant")
    var listPlato: List<Plato>? = null,
    @field:SerializedName("listcategoria")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idRestaurant")
    var listcategoria: List<Categoria>? = null,
    @field:SerializedName("listmodificadores")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idRestaurant")
    var listmodificadores: List<Modificador>? = null,
) : Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }

    override fun equals(other: Any?): Boolean {

        return super.equals(other)
    }
}