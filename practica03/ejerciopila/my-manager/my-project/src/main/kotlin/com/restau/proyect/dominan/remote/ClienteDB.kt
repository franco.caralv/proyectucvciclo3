package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Cliente
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class ClienteDB(
    @Autowired private val restaurantDB: RestaurantDB,
    @Autowired val db: Database
) {

    val restaurant
        get() = restaurantDB.restaurant

    private val _listClient = MutableStateFlow<List<Cliente>>(listOf())

    val listClient = _listClient.asStateFlow()

    @PostConstruct
    fun init() {
        searchCLient()

    }

    suspend fun searchClienteForDni(dni: String): Customer<Cliente> = withContext(Dispatchers.IO) {
        return@withContext try {
            val cliente = db.find(Cliente::class.java).where().eq("dni", dni).findOne()
            cliente?.let {
                Customer.onSuccess(it)
            } ?: run {
                Customer.onError(Error(400, "Cliente no encontrado"))
            }
        } catch (e: Exception) {
            Customer.onError(Error(500, e.message))
        }
    }   

    fun searchCLient() {
        _listClient.value = db.find(Cliente::class.java).findList()
    }


    suspend fun saveCliente(cliente: Cliente): Customer<Cliente> = withContext(Dispatchers.IO) {
        return@withContext try {
            db.save(cliente.copy(id = null))
            _listClient.value = _listClient.value.plus(cliente)
            cliente.let {
                Customer.onSuccess(it)
            }
        } catch (e: Exception) {
            println(e.message)
            Customer.onError(Error(500, e.message))
        }
    }


}