package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Categoria
import io.ebean.Database
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class CategoriaDB(
    @Autowired private val restaurant: RestaurantDB,
    @Autowired val db: Database
) {
    private val _listCategoria = MutableStateFlow<List<Categoria>>(listOf())

    val listCategoria = _listCategoria.asStateFlow()

    @PostConstruct
    fun init() {
        _listCategoria.value = categoriasbyrestautante(restaurant.restaurant?.id)
    }

    private fun categoriasbyrestautante(id: Int?): List<Categoria> {
        return id?.let {
            db.find(Categoria::class.java).setAutoTune(false).where().eq("t0.id_restaurant", it).findList()
        } ?: listOf()
    }

    fun add(element: Categoria): Customer<List<Categoria>> {
        return try {
            element.idRestaurant = restaurant.restaurant
            db.save(element)
            _listCategoria.value = categoriasbyrestautante(restaurant.restaurant?.id)
            Customer.onSuccess(_listCategoria.value)
        } catch (e: Exception) {
            println(e.message)
            Customer.onError(Error(500))
        }
    }

    fun deleteCategoria(categoria: Categoria): Customer<List<Categoria>> {
        return try {
            db.delete(categoria)
            _listCategoria.value = categoriasbyrestautante(restaurant.restaurant?.id)
            Customer.onSuccess(_listCategoria.value)
        } catch (e: Exception) {
            Customer.onError(Error(400))
        }

    }


}