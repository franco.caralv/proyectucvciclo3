package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Mozo
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
@Lazy
class MozosDB(
    @Autowired private val restaurantDB: RestaurantDB,
    @Autowired val db: Database,
) {

    val restaurant
        get() = restaurantDB.restaurant

    private val _listMozos = MutableStateFlow<List<Mozo>>(listOf())

    val listMozos = _listMozos.asStateFlow()

    @PostConstruct
    fun init() {
        cargarMozos()
    }

    private fun cargarMozos() {
        restaurant?.id?.let {
            _listMozos.value = db.find(Mozo::class.java)
                .select("id, nombre, estado, tiempo, hora_entrada, hora_salida, puntaje, totalpuntaje, id_restaurant")
                .where()
                .eq("t0.id_restaurant", it).findList()
        }
    }

    suspend fun saveMozo(mozo: Mozo): Customer<List<Mozo>> = withContext(Dispatchers.IO) {
        return@withContext try {
            mozo.idRestaurant = restaurant
            db.save(mozo.copy(id = null))
            cargarMozos()
            Customer.onSuccess(_listMozos.value)
        } catch (e: Exception) {
            println("Me encuentro en saveMozo ")
            println(e.message)
            Customer.onError(Error(500, e.message))
        }
    }


    suspend fun deleteMozos(mozo: Mozo): Customer<List<Mozo>> = withContext(Dispatchers.IO) {
        return@withContext try {
            db.delete(mozo)
            cargarMozos()
            Customer.onSuccess(_listMozos.value)
        } catch (e: Exception) {
            println("Me encuentro en deleteMozos ")
            println(e.message)
            Customer.onError(Error(500, e.message))
        }
    }


}