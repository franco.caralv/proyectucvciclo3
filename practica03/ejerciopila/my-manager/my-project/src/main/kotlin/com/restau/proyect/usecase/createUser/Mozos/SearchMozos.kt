package com.restau.proyect.usecase.createUser.Mozos

import com.restau.proyect.data.Mozo
import com.restau.proyect.dominan.remote.MozosDB
import kotlinx.coroutines.flow.StateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class SearchMozos(
    @Autowired private val mozosDB: MozosDB
) {


    operator fun invoke(): StateFlow<List<Mozo>> {
        return mozosDB.listMozos
    }
}