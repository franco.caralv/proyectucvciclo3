package com.restau.proyect.data.extenciones

inline fun <reified T> T.toStringPrimitivo(): String {
    return T::class.java.methods.filter {
        it.name.startsWith("get") && (it.returnType.typeName == String::class.java.typeName || it.returnType.typeName == "java.lang.Integer"
                || it.returnType.typeName == "java.lang.Double" || it.returnType.typeName == "java.lang.Float" || it.returnType.typeName == "java.lang.Long"
                || it.returnType.typeName == "java.lang.Boolean" || it.returnType.typeName == "java.lang.Short"
                )
    }.joinToString(", ", "${T::class.simpleName}(", ")") {
        "${it.name.drop(3).lowercase()}=${it.invoke(this)}"
    }

}