package com.restau.proyect.usecase.createPMC.base

import com.restau.proyect.data.Categoria
import kotlinx.coroutines.flow.Flow

interface SearchCategoria {

    operator fun invoke(): Flow<List<Categoria>>
}