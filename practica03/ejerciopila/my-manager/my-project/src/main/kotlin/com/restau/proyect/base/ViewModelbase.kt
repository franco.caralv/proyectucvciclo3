package com.restau.proyect.base

import kotlinx.coroutines.*
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy


abstract class ViewModelbase {

    private val exptionhandle = CoroutineExceptionHandler { it, throwable ->
        println("Error dectetado de en el ${throwable.localizedMessage} y el error es ${throwable.cause} ${throwable.message}    ")

    }

    protected var lyceviewmodel = CoroutineScope(SupervisorJob())

    @PostConstruct
    fun postconstructor() {
        lyceviewmodel = CoroutineScope(SupervisorJob())
    }


    @PreDestroy
    fun destroy() {
        lyceviewmodel.cancel()
    }

    protected fun launch(onProcess: suspend () -> Unit): Job {
        return lyceviewmodel.launch(Dispatchers.IO + NonCancellable) { onProcess() }
    }


}