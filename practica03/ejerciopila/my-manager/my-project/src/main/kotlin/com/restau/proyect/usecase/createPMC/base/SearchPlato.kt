package com.restau.proyect.usecase.createPMC.base

import com.restau.proyect.data.Plato
import kotlinx.coroutines.flow.Flow

interface SearchPlato {

    operator fun invoke(): Flow<List<Plato>>
}