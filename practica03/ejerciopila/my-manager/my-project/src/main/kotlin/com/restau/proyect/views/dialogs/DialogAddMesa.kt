package com.restau.proyect.views.dialogs

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.TooltipArea
import androidx.compose.foundation.TooltipPlacement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogState
import androidx.compose.ui.window.rememberDialogState
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.data.EstadoMesa
import com.restau.proyect.events.dialog.flujoAddMesa.AddMesaFlujo
import com.restau.proyect.theme.POPPINS
import com.restau.proyect.viewmodel.dialog.DialogoAddMesaModel
import kotlinx.coroutines.Dispatchers

import java.awt.Toolkit


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DialogAddMesa(
    visible: Boolean, onRequest: () -> Unit, state: DialogState = rememberDialogState(
        width = 500.dp,
        height = 400.dp,
    )
) {
    val focusManager = LocalFocusManager.current
    val viewmodel: DialogoAddMesaModel by viewmodels()
    val mesa by viewmodel.mesa.collectAsState(Dispatchers.Unconfined)
    Dialog(
        {
            onRequest()
        },
        state,
        visible, undecorated = false, resizable = false,
        title = "Agrega tus mesas =) "

    ) {
        Column(modifier = Modifier.fillMaxHeight().fillMaxWidth().padding(20.dp)) {
            OutlinedTextField(
                mesa.nombre ?: "",
                { newValue ->
                    viewmodel.emite(AddMesaFlujo.ListenerMesa(mesa.copy(nombre = newValue)))
                },
                label = {
                    Text("Nombre de la mesa",
                        fontFamily = FontFamily.POPPINS(),)
                },
                modifier = Modifier.fillMaxWidth(), singleLine = true,
            )
            OutlinedTextField(
                mesa.numerosillas ?: "",
                { newValue ->
                    if (newValue.all { it.isDigit() }) {
                        viewmodel.emite(AddMesaFlujo.ListenerMesa(mesa.copy(numerosillas = newValue)))
                    } else {
                        Toolkit.getDefaultToolkit().beep()
                    }
                },
                label = {
                    Text("Numero de sillas ",
                        fontFamily = FontFamily.POPPINS(),)
                },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
            )
            OutlinedTextField(EstadoMesa.Desocupado.estado.toString(), {

            }, label = {
                Text("Estado",
                    fontFamily = FontFamily.POPPINS(),)
            }, modifier = Modifier.fillMaxWidth(),
                enabled = false
            )
            TooltipArea(
                tooltip = {
                    // composable tooltip content
                    Surface(
                        modifier = Modifier.shadow(4.dp),
                        color = Color(255, 255, 210),
                        shape = RoundedCornerShape(4.dp)
                    ) {
                        Text(
                            text = "El numero de sillas no pude pasar de 20",
                            modifier = Modifier.padding(10.dp)
                        )
                    }
                },
                delayMillis = 600, // in milliseconds
                tooltipPlacement = TooltipPlacement.CursorPoint(
                    alignment = Alignment.BottomEnd
                )
            ) {
                val enable = if (!mesa.numerosillas.isNullOrBlank()) {
                    (mesa.numerosillas)?.toInt()!! < 20
                } else {
                    false
                } && !mesa.nombre.isNullOrBlank()
                Button({
                    viewmodel.emite(AddMesaFlujo.Guardar(onRequest))
                }, modifier = Modifier.fillMaxWidth(), enabled = enable) {
                    Text("Guardar",
                        fontFamily = FontFamily.POPPINS(),)
                }
            }

            Button({
                onRequest()
            }, modifier = Modifier.fillMaxWidth()) {
                Text("Cerrar",
                    fontFamily = FontFamily.POPPINS(),)
            }

        }
    }


}




