package com.restau.proyect.views

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.components.date.TimerPickerModal
import com.restau.proyect.components.h.H3
import com.restau.proyect.components.h.H4
import com.restau.proyect.components.p.P
import com.restau.proyect.components.table.StyledTable
import com.restau.proyect.usecase.createUser.event.EventCreateUser
import com.restau.proyect.viewmodel.CreateUserModel

@Composable
@Preview
fun CreateUserMozo() {

    val createUserModel: CreateUserModel by viewmodels()
    val mozo by createUserModel.mozo.collectAsState()
    val tableMozo by createUserModel.tableMozo.collectAsState()

    BoxWithConstraints(modifier = Modifier.fillMaxWidth().fillMaxHeight()) {

        Box(modifier = Modifier.fillMaxWidth().fillMaxHeight()) {
            val stateVertical = rememberLazyListState(0)
            LazyColumn(Modifier.fillMaxWidth().fillMaxHeight().padding(horizontal = 20.dp, vertical = 5.dp)) {
                item {
                    H3(
                        "En este apartado podras crear tus MOZOS",
                        Modifier.fillMaxWidth().padding(10.dp),
                        textAlign = TextAlign.Start,
                        fontWeight = FontWeight.SemiBold
                    )
                }
                item {
                    Card(modifier = Modifier.fillParentMaxWidth().wrapContentHeight().heightIn(150.dp)) {
                        Column(modifier = Modifier.fillMaxWidth().padding(20.dp)) {
                            H4("Crear Tus Mozos")
                            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {

                                OutlinedTextField(mozo.nombre ?: "", {
                                    createUserModel.events(EventCreateUser.listenerField(mozo.copy(nombre = it)))
                                }, label = {
                                    P("Nombre")
                                }, singleLine = true, modifier = Modifier.weight(1f).padding(horizontal = 10.dp))
                                OutlinedTextField(mozo.totalpuntaje?:"0", {}, label = {
                                    P("Puntaje")
                                }, enabled = false,
                                    maxLines = 1,
                                    singleLine = true, modifier = Modifier.weight(1f).padding(horizontal = 10.dp)
                                )
                                OutlinedTextField(mozo.estado?:"", {}, label = {
                                    P("Estado")
                                },enabled = false, singleLine = true, modifier = Modifier.weight(1f).padding(horizontal = 10.dp))

                            }
                            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                                OutlinedTextField(mozo.hora_entrada?:"", {}, label = {
                                    P("Hora de Entrada")
                                }, singleLine = true, trailingIcon = {
                                    TimerPickerModal(onclick = {
                                        createUserModel.events(EventCreateUser.listenerField(mozo.copy(hora_entrada = it)))
                                    })
                                }, modifier = Modifier.weight(1f).padding(horizontal = 10.dp))
                                OutlinedTextField(mozo.hora_salida?:"", {}, label = {
                                    P("Hora de salida")
                                }, singleLine = true, trailingIcon = {
                                    TimerPickerModal(onclick = {
                                        createUserModel.events(EventCreateUser.listenerField(mozo.copy(hora_salida = it)))
                                    })
                                }, modifier = Modifier.weight(1f).padding(horizontal = 10.dp))
                            }
                            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                                Button({
                                    createUserModel.events(EventCreateUser.clearField)
                                }, modifier = Modifier.padding(5.dp)) {
                                    P("Borrar")
                                }
                                Button({
                                    createUserModel.events(EventCreateUser.saveMozo)
                                }, modifier = Modifier.padding(5.dp)) {
                                    P("Guardar")
                                }
                            }
                        }

                    }
                }
                item {
                    Card(
                        shape = RoundedCornerShape(15.dp),
                        elevation = 25.dp,
                        modifier = Modifier.padding(15.dp)
                    ) {
                        Column {
                            H3(
                                "Lista de Mozos",
                                modifier = Modifier.fillParentMaxWidth().padding(10.dp),
                                textAlign = TextAlign.Center
                            )
                            tableMozo?.let {
                                StyledTable(it)
                            }?:run {
                                Box(modifier = Modifier.fillParentMaxWidth().background(Color.LightGray).padding(20.dp)) {
                                    H4("Se encuentra vacio")
                                }
                            }



                        }

                    }
                }


            }
            VerticalScrollbar(
                modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
                adapter = rememberScrollbarAdapter(stateVertical)
            )
        }
    }


}
