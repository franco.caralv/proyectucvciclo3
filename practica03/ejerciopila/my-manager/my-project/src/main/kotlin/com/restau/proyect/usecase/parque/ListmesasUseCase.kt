package com.restau.proyect.usecase.parque

import com.restau.proyect.data.Mesa
import com.restau.proyect.dominan.remote.MesasDB
import com.restau.proyect.dominan.remote.RestaurantDB
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class ListmesasUseCase(
    @Autowired private val listmesasPreferent: MesasDB,
    @Autowired private val listPreferentRestaurante: RestaurantDB
) {

     operator fun invoke(): Flow<List<Mesa>> {
        return listmesasPreferent.listmesasoyente.map { lis ->
            lis
        }
    }

}