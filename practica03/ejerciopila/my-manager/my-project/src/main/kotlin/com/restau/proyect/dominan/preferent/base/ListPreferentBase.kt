package com.restau.proyect.dominan.preferent.base

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.dominan.preferent.PersonLogin
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.annotation.PostConstruct


abstract class ListPreferentBase<T>(
    protected open val usuariopreferent: PersonLogin
) {

    protected abstract val typeToken: TypeToken<T>

    protected abstract var key: String
    protected abstract val nameDocumento: String
    protected abstract var fileocumento: String
    protected abstract var listpersonpre: StorageService<T>?
    protected abstract val _listCategoria: MutableStateFlow<T>

    abstract val listCategoria: StateFlow<T>

    @PostConstruct
    open fun cargandoModificadores() {
        fileocumento = "$fileocumento${usuariopreferent.person?.id ?: ""}"
        listpersonpre = StorageService(typeToken, nameDocumento, key)
        listpersonpre?.load(fileocumento)?.let {
            _listCategoria.value = it
        }

    }

}