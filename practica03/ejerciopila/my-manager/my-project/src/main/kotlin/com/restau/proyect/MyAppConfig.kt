package com.restau.proyect

import com.restau.proyect.base.http.ApiConfig
import com.restau.proyect.base.http.BaseClient
import com.restau.proyect.base.http.client.ClientImpl
import com.restau.proyect.base.http.dev.Dev
import com.restau.proyect.base.http.pro.Pro
import com.restau.proyect.data.Ciudad
import io.ebean.DB
import io.ebean.Database
import org.springframework.context.annotation.*
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate
import java.time.Duration
import java.util.*


@Configuration
@ComponentScan(basePackages = ["com.restau.proyect"])
@PropertySource("classpath:application.properties", ignoreResourceNotFound = true)
open class MyAppConfig {


    @Bean
    @Scope("singleton")
    @Lazy
    @Primary
    open fun obtenerApiconfig(): ApiConfig {
        val version = System.getProperty("app.version") ?: "true"
        return if (version == "true") {
            Dev()
        } else {
            Pro()
        }
    }

    @Bean
    @Scope("singleton")
    @Lazy
    @Primary
    open fun obtenerclient(): BaseClient {
        return ClientImpl()
    }


    @Bean
    @Lazy
    @Primary
    open fun restTemplate(): RestTemplate {
        //webclient
        val requestFactory = SimpleClientHttpRequestFactory()
        // Configura el tiempo de espera de conexión y lectura a 30 segundos
        requestFactory.setConnectTimeout(Duration.ofSeconds(30).toMillis().toInt())
        requestFactory.setReadTimeout(Duration.ofSeconds(30).toMillis().toInt())
        val restTemplate = RestTemplate(requestFactory)

        restTemplate.messageConverters.add(MappingJackson2HttpMessageConverter())
        return restTemplate
    }

    @Bean
    open fun instancededb(): Database {

        return DB.getDefault()
    }


    @Bean

    open fun initciandoCiudad(db: Database) :String{
        if (db.find(Ciudad::class.java).findList().isEmpty()) {
            listOf(Ciudad.PIURA, Ciudad.LIMA, Ciudad.TRUJILLO, Ciudad.CHICLAYO).map {
                it.save()
            }
        }
        return ""
    }

    /* @Bean
    open fun dataSourceConfigcreate(): Database? {
         val cfg = DatabaseConfig()

         val properties = Properties()
         properties["ebean.db.ddl.generate"] = "true"
         properties["ebean.db.ddl.run"] = "true"
         properties["datasource.db.username"] = "sa"
         properties["datasource.db.password"] = ""
         properties["datasource.db.databaseUrl"] = "jdbc:h2:mem:app2"
         properties["datasource.db.databaseDriver"] = "org.h2.Driver"

         cfg.loadFromProperties(properties)
         val server = DatabaseFactory.create(cfg)

         return server
     }*/


}
