package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Mesa
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class MesasDB(
    @Autowired private val restaurantDB: RestaurantDB,
    @Autowired val db: Database
) {

    val restaurant
        get() = restaurantDB.restaurant


    private val _listmesasoyente = MutableStateFlow<List<Mesa>>(listOf())

    val listmesasoyente = _listmesasoyente.asStateFlow()

    @PostConstruct
    fun init() {
        searchoftablesbyrestaurant()
    }

    private fun searchoftablesbyrestaurant() {
        restaurant?.id?.let {

            _listmesasoyente.value = db.find(Mesa::class.java).where().eq("id_restaurant", it).findList()
            println(_listmesasoyente.value)
        }

    }

    suspend fun add(mesa: Mesa): Customer<Mesa> = withContext(Dispatchers.IO) {
        return@withContext try {
            mesa.idRestaurant = restaurant
            db.save(mesa)
            searchoftablesbyrestaurant()
            Customer.onSuccess(mesa)
        } catch (e: Exception) {
            Customer.onError(Error(500, e.message))
        }
    }


}