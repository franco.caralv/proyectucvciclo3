package com.restau.proyect.viewmodel.dialog

import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.data.Mesa
import com.restau.proyect.events.dialog.flujoAddMesa.AddMesaFlujo
import com.restau.proyect.usecase.dialog.Addmesa.AddMesaUseCase
import com.restau.proyect.usecase.dialog.Addmesa.Event.MesasEvent
import com.restau.proyect.usecase.dialog.Addmesa.SearchRestaurante
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import javax.annotation.PostConstruct


@Controller
@Lazy
@Scope("prototype")
class DialogoAddMesaModel(
    @Autowired private val addMesaUseCase: AddMesaUseCase,
    @Autowired private val searchRestaurante: SearchRestaurante
) : ViewModelbase() {

    private val _mesa = MutableStateFlow(Mesa(estado = "3", numerosillas = "4"))
    val mesa = _mesa.asStateFlow()

    private val flujo = MutableStateFlow<AddMesaFlujo?>(null)

    @PostConstruct
    fun init() = launch {
        flujo.filterNotNull().collect(::validarFlujo)
    }

    private suspend fun validarFlujo(flujo: AddMesaFlujo) {
        when (flujo) {
            is AddMesaFlujo.Guardar -> {
                guardado(addMesaUseCase(mesa.value, searchRestaurante())) {
                    flujo.onRequest()
                }
            }

            is AddMesaFlujo.ListenerMesa -> {
                listener(flujo.mesa)
            }
        }
    }

    private fun guardado(addMesaUseCase: MesasEvent, onRequest: () -> Unit) = launch {
        when (addMesaUseCase) {
            is MesasEvent.mesaSucess -> {
                onRequest()
            }

            is MesasEvent.mesasError -> {

            }
        }
    }


    private fun listener(mesa: Mesa) {
        _mesa.value = mesa
    }


    fun emite(addflujo: AddMesaFlujo) {
        flujo.value = addflujo
    }


}