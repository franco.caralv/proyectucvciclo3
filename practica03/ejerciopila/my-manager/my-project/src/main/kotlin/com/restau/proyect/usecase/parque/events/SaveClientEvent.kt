package com.restau.proyect.usecase.parque.events

import com.restau.proyect.data.Cliente

sealed class SaveClientEvent {

    class SuccesSaveClient(val cliente: Cliente):SaveClientEvent()
    object ErrorClient:SaveClientEvent()

}