package com.restau.proyect.dominan.preferent

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Mesa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
class ListmesasPreferent(
    @Autowired private val personLogin: PersonLogin,
) {

    @Value("\${app.secretKey}")
    private lateinit var key: String

    private lateinit var listmesaspre: StorageService<List<Mesa>>

    lateinit var listmesas: List<Mesa>
        private set

    private val _listmesasoyente = MutableStateFlow<List<Mesa>>(listOf())

    val listmesasoyente = _listmesasoyente.asStateFlow()

    private var filname = "listademesas${personLogin.person?.id}"

    @PostConstruct
    private fun caragando() {
        listmesaspre =  StorageService<List<Mesa>>(object : TypeToken<List<Mesa>>() {}, "listmesas", key)

        listmesaspre.load(filname)?.let {
            listmesas = it
        } ?: run {
            listmesas = listOf()
        }

        _listmesasoyente.value = listmesas
    }

    suspend fun add(mesa: Mesa, id: Int): Customer<Mesa> = withContext(Dispatchers.IO) {
        return@withContext try {
            mesa.id = (listmesas.size + 1)
            val list = listmesas.toMutableList()
            list.add(mesa)
            listmesaspre.save(list, filname)
            listmesas = list.toList()
            _listmesasoyente.value = listmesas
            Customer.onSuccess(mesa)
        } catch (e: Exception) {
            Customer.onError(Error(500, "Fallando en el guardado"))
        }
    }

    fun cargandolistas() {
        filname = "listademesas${personLogin.person?.id}"
        listmesaspre.load(filname)?.let {
            listmesas = it
        } ?: run {
            listmesas = listOf()
        }
        _listmesasoyente.value = listmesas

    }

}