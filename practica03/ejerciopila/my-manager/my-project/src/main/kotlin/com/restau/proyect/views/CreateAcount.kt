package com.restau.proyect.views

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.components.AutoCompleteTextField
import com.restau.proyect.components.ParamsData
import com.restau.proyect.data.Ciudad
import com.restau.proyect.events.createAcount.CreateCuenta
import com.restau.proyect.router.Controller
import com.restau.proyect.theme.POPPINS
import com.restau.proyect.theme.greend
import com.restau.proyect.viewmodel.CreateAcountModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


@Composable
@Preview
fun CreateAcount() {
    val viewmodel: CreateAcountModel by viewmodels()
    val listciudad by viewmodel.listciudad.collectAsState()
    val person by viewmodel.person.collectAsState()
    val newpassword by viewmodel.newpassword.collectAsState()
    val password by viewmodel.password.collectAsState()
    val enable by viewmodel.enable.collectAsState()
    val enablecorreo by viewmodel.enablecorreo.collectAsState()
    val enablepass by viewmodel.enablepass.collectAsState()
    val focusManager = LocalFocusManager.current
    var isvisiblepass by rememberSaveable {
        mutableStateOf(true)
    }
    var isvisiblenew by rememberSaveable {
        mutableStateOf(true)
    }
    val snackbarHostState = remember { SnackbarHostState() }
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        SnackbarHost(
            hostState = snackbarHostState,
            modifier = Modifier.align(Alignment.BottomCenter)
        )
        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
            Row(Modifier.fillMaxWidth().padding(horizontal = 15.dp, vertical = 20.dp)) {
                Icon(Icons.Outlined.ArrowBack, "", Modifier.clickable {
                    GlobalScope.launch {
                        Controller.pop()
                    }
                }, greend)
            }
            Card(
                elevation = 20.dp, shape = RoundedCornerShape(15.dp),
                modifier = Modifier.heightIn(250.dp, 500.dp).widthIn(250.dp, 500.dp)
                    .fillMaxHeight().fillMaxWidth()
                    .padding(20.dp)
            ) {
                Column(
                    modifier = Modifier.fillMaxSize().padding(25.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.SpaceEvenly
                ) {
                    OutlinedTextField(
                        person.name ?: "",
                        {
                            viewmodel.emitevent(
                                CreateCuenta.ListTex(
                                    person.copy(name = it)
                                )
                            )
                        },
                        label = {
                            Text("Name", fontFamily = FontFamily.POPPINS(),)
                        },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1
                    )
                    OutlinedTextField(person.correo ?: "", {
                        viewmodel.emitevent(
                            CreateCuenta.validarCorreo(
                                it
                            )
                        )
                    }, maxLines = 1, label = {
                        Text("Correo", fontFamily = FontFamily.POPPINS(),)
                    }, modifier = Modifier.fillMaxWidth(),
                        isError = !enablecorreo,
                        keyboardActions = KeyboardActions(
                            onNext = {
                                focusManager.moveFocus(FocusDirection.Down)
                            }
                        ),
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Email,
                            imeAction = ImeAction.Next
                        )

                    )
                    OutlinedTextField(
                        password,
                        {
                            viewmodel.emitevent(CreateCuenta.ValidarPassword(it, newpassword))
                        },
                        maxLines = 1,
                        label = {
                            Text("Password", fontFamily = FontFamily.POPPINS(),)
                        },
                        modifier = Modifier.fillMaxWidth(),
                        isError = !enablepass,
                        trailingIcon = {
                            IconButton(onClick = {
                                isvisiblepass = !isvisiblepass
                            }) {
                                Icon(
                                    painter = if (isvisiblepass) painterResource("drawables/iconos/visibilidad-desactivada.png") else painterResource(
                                        "drawables/iconos/boton-de-visibilidad.png"
                                    ),
                                    contentDescription = "",
                                    modifier = Modifier.size(25.dp)
                                )
                            }
                        },
                        visualTransformation = if (isvisiblepass) PasswordVisualTransformation() else VisualTransformation.None,
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Password,
                            imeAction = ImeAction.Done
                        ),
                        keyboardActions = KeyboardActions(
                            onDone = {
                                focusManager.moveFocus(FocusDirection.Down)
                            }
                        ),
                    )
                    OutlinedTextField(
                        newpassword,
                        {
                            viewmodel.emitevent(CreateCuenta.ValidarPassword(password, it))
                        },
                        maxLines = 1,
                        label = {
                            Text("Repeat Password", fontFamily = FontFamily.POPPINS(),)
                        },
                        modifier = Modifier.fillMaxWidth(),
                        isError = !enablepass,
                        trailingIcon = {
                            IconButton(onClick = {
                                isvisiblenew = !isvisiblenew
                            }) {
                                Icon(
                                    painter = if (isvisiblenew) painterResource("drawables/iconos/visibilidad-desactivada.png") else painterResource(
                                        "drawables/iconos/boton-de-visibilidad.png"
                                    ),
                                    contentDescription = "",
                                    modifier = Modifier.size(25.dp)
                                )
                            }
                        },
                        visualTransformation = if (isvisiblenew) PasswordVisualTransformation() else VisualTransformation.None,
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Password,
                            imeAction = ImeAction.Done
                        ),
                        keyboardActions = KeyboardActions(
                            onDone = {
                                focusManager.moveFocus(FocusDirection.Down)
                            }
                        ),
                    )
                    AutoCompleteTextField(
                        ParamsData(
                            listciudad,
                            label = "Ciudad",
                            atributo = Ciudad::name,
                            modifier = Modifier.fillMaxWidth(),
                            onItemSelected = {
                                viewmodel.emitevent(CreateCuenta.ListTex(person.copy(ciudad = it)))
                            }, onSearch = {

                            })
                    )
                    Button(
                        {
                            viewmodel.emitevent(CreateCuenta.Crear(person, snackbarHostState))
                        }, modifier = Modifier.fillMaxWidth().padding(horizontal = 20.dp).zIndex(0f),
                        enabled = enable
                    ) {
                        Text("Crear")
                    }
                }
            }
        }

    }
}

