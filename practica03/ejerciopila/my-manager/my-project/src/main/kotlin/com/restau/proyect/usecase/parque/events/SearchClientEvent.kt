package com.restau.proyect.usecase.parque.events

import com.restau.proyect.data.Cliente

sealed class SearchClientEvent {

    object ClientNodFound:SearchClientEvent()

    class SuccessClient(val cliente: Cliente):SearchClientEvent()

    object BDError:SearchClientEvent()

}