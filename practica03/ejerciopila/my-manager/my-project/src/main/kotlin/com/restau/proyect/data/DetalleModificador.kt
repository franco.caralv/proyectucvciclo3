package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "DetalleModificador")
data class DetalleModificador(
    @field:SerializedName("id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    val id: Int? = null,
    @field:SerializedName("idModificador")
    @ManyToOne
    @JoinColumn(name = "id_modificador", referencedColumnName = "id")
    var idModificador: Modificador? = null,
    @field:SerializedName("idpedido")
    @ManyToOne
    @JoinColumn(name = "id_pedido", referencedColumnName = "id")
    var idpedido: Pedido? = null,
) : Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}