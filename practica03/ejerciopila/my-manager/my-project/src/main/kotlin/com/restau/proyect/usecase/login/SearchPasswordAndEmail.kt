package com.restau.proyect.usecase.login

import com.restau.proyect.data.Person
import com.restau.proyect.usecase.createAcount.SearchCuenta
import com.restau.proyect.usecase.login.events.LoginEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class SearchPasswordAndEmail(
    @Autowired private val searchCuenta: SearchCuenta
) {
    operator fun invoke(person: Person): LoginEvent {
        val (search, value) = searchCuenta(person)
        return if (search) {
            value?.let { LoginEvent.Find(it) } ?: LoginEvent.ErrorFind
        } else {
            LoginEvent.ErrorFind
        }

    }

}