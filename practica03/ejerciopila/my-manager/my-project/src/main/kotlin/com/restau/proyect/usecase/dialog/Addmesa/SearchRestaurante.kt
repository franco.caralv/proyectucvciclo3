package com.restau.proyect.usecase.dialog.Addmesa

import com.restau.proyect.data.Restaurant
import com.restau.proyect.dominan.remote.RestaurantDB
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class SearchRestaurante(
    @Autowired private val listPreferentRestaurante: RestaurantDB
) {


    operator fun invoke(): Restaurant {
        return listPreferentRestaurante.restaurant ?: Restaurant(name = "No se pudo cargar el restaurant")
    }


}