package com.restau.proyect.usecase.createUser.event

sealed class EventAddMozo {

    object SuccessSaveMozo:EventAddMozo()
    object FaildMozo:EventAddMozo()

}