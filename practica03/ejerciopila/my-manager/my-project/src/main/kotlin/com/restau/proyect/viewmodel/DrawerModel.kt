package com.restau.proyect.viewmodel

import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.usecase.drawer.DrawerUseCase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller


@Scope("prototype")
@Controller
@org.springframework.context.annotation.Lazy
class DrawerModel(
    @Autowired private val drawerUseCase: DrawerUseCase
) : ViewModelbase() {

    suspend fun closesesion() {
        drawerUseCase()
    }

}