package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Cliente")
data class Cliente(
    @field:SerializedName("id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    val id: Int? = null,
    @field:SerializedName("nombre")
    val nombre: String? = null,
    @field:SerializedName("numero_telefonico")
    @Column(nullable = true)
    val numero_telefonico: String? = null,
    @field:SerializedName("dni")
    @Column(unique = true)
    val dni: String? = null,
    @field:SerializedName("descripcion")
    val descripcion: String? = null,
    @field:SerializedName("listdetallemesa")
    @OneToMany(cascade = [CascadeType.PERSIST], mappedBy = "idcliente")
    val listdetallemesa: List<DetalleMesa>? = null,
) : Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}