package com.restau.proyect.viewmodel

import com.restau.proyect.base.UseCaseBase
import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.data.Categoria
import com.restau.proyect.data.Modificador
import com.restau.proyect.data.Plato
import com.restau.proyect.entity.ParamsStyledTable
import com.restau.proyect.events.createPlato.CreatePlatoEventos
import com.restau.proyect.mappers.toCateParamsStyle
import com.restau.proyect.mappers.toParamsStyle
import com.restau.proyect.mappers.toPlatoParamsStyle
import com.restau.proyect.usecase.createPMC.base.AddModificador
import com.restau.proyect.usecase.createPMC.base.SearchCategoria
import com.restau.proyect.usecase.createPMC.base.SearchModificador
import com.restau.proyect.usecase.createPMC.base.SearchPlato
import com.restau.proyect.usecase.createPMC.event.*
import com.restau.proyect.util.Utility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import javax.annotation.PostConstruct


@Controller
@Lazy
@Scope("prototype")
class CrearPlatoModel(
    @Autowired private val addModificador: AddModificador,
    @Autowired private val searchCategoria: SearchCategoria,
    @Autowired private val searchModificador: SearchModificador,
    @Autowired private val deleteModificador: UseCaseBase<Modificador, Unit>,
    @Autowired private val addPlatoImpl: UseCaseBase<Plato, ReponsePLato>,
    @Autowired private val addCategoria: UseCaseBase<Categoria, ResponseCategoria>,
    @Autowired private val deleteCategoria: UseCaseBase<Categoria, ResponseDeleteCategoria>,
    @Autowired private val deletePlato: UseCaseBase<Plato, ResponseDeletePlato>,
    @Autowired private val searchPlato: SearchPlato,
    @Autowired private val utility: Utility,
) : ViewModelbase() {


    val tablemodificador = MutableStateFlow<ParamsStyledTable?>(null)
    val tablecategoria = MutableStateFlow<ParamsStyledTable?>(null)
    val tableplato = MutableStateFlow<ParamsStyledTable?>(null)
    private val _modificador = MutableStateFlow(Modificador())
    val modificador = _modificador.asStateFlow()

    private val _plato = MutableStateFlow(Plato())
    val plato = _plato.asStateFlow()

    private val _categoria = MutableStateFlow(Categoria())
    val categoria = _categoria.asStateFlow()

    private val _listCategoria = MutableStateFlow<List<Categoria>>(listOf())
    val listCategoria = _listCategoria.asStateFlow()

    @PostConstruct
    fun inicio() {
        searchModificador().onEach { listamod ->
            searchModificadorListener(listamod)
        }.flowOn(Dispatchers.IO).launchIn(lyceviewmodel)
        searchCategoria().onEach {
            searchCategoriaListener(it)
        }.flowOn(Dispatchers.IO).launchIn(lyceviewmodel)
        searchPlato().onEach { listplato ->
            searchPlatoListener(listplato)
        }.flowOn(Dispatchers.IO).launchIn(lyceviewmodel)
    }

    private fun searchModificadorListener(listamod: List<Modificador>) {
        tablemodificador.value = listamod.toParamsStyle { indexfila, data ->
            val element = listamod.getOrNull(indexfila)
            element?.let {
                launch {
                    deleteModificador.invoke(it)
                }
            }
        }
        if (listamod.isEmpty())
            tablemodificador.value = null
    }

    private fun searchPlatoListener(listplato: List<Plato>) {
        tableplato.value = listplato.toPlatoParamsStyle { indexfila, data ->
            val element = listplato.getOrNull(indexfila)
            element?.let {
                launch {
                    deletePlato.invoke(it)
                }
            }
        }
        if (listplato.isEmpty())
            tableplato.value = null
    }

    private fun searchCategoriaListener(it: List<Categoria>) {
        _listCategoria.value = it
        tablecategoria.value = it.toCateParamsStyle { indexfila, data ->
            val element = it.getOrNull(indexfila)
            element?.let {
                launch {
                    val response = deleteCategoria.invoke(it)
                    when (response) {
                        is ResponseDeleteCategoria.Error -> {

                        }

                        is ResponseDeleteCategoria.success -> {

                        }
                    }
                }
            }

        }
        if (it.isEmpty())
            tablecategoria.value = null
    }


    private suspend fun listenerofevents(it: CreatePlatoEventos) {
        when (it) {
            is CreatePlatoEventos.GuardarPlato -> {
                saveplato()
            }

            is CreatePlatoEventos.OyenteDeTexPlato -> {
                _plato.value = it.plato
            }

            is CreatePlatoEventos.GuardarModifcador -> {
                when (val response = addModificador(_modificador.value)) {
                    is ResponseModificador.SuccessData -> {
                        emit(CreatePlatoEventos.BorrarModificador)
                    }

                    is ResponseModificador.ErrorData -> {
                        println("Error ")
                    }

                    is ResponseModificador.ErrorSinData -> {
                        utility.showMessageAdvertencia(response.discripcion,"FALLA DE DATOS")
                    }
                }
            }

            is CreatePlatoEventos.Guardarcategoria -> {

                when (addCategoria(categoria.value)) {
                    is ResponseCategoria.Sucess -> {
                        emit(CreatePlatoEventos.Borrarcategoria)
                    }

                    is ResponseCategoria.Error -> {
                        emit(CreatePlatoEventos.Borrarcategoria)
                    }

                }

            }

            is CreatePlatoEventos.OyenteDeTexCategoria -> {

                _categoria.value = it.categoria
            }

            is CreatePlatoEventos.OyenteDeTexModificador -> {
                _modificador.value = it.modificador
            }

            is CreatePlatoEventos.BorrarModificador -> {
                _modificador.value = Modificador()
            }

            is CreatePlatoEventos.BorrarPlato -> {
                _plato.value = Plato()
            }

            is CreatePlatoEventos.Borrarcategoria -> {
                _categoria.value = Categoria()
            }
        }
    }

    private suspend fun CrearPlatoModel.saveplato() {
        val response = addPlatoImpl(plato.value)
        when (response) {
            is ReponsePLato.ErrordeFile -> {
                emit(CreatePlatoEventos.BorrarPlato)
            }

            is ReponsePLato.Error -> {

            }

            is ReponsePLato.Sucess -> {
                emit(CreatePlatoEventos.BorrarPlato)
            }

            is ReponsePLato.ErrorCampos -> {
                utility.showMessageAdvertencia(response.descriptor)

            }
        }
    }

    fun emit(evento: CreatePlatoEventos) = launch {
        listenerofevents(evento)
    }


}