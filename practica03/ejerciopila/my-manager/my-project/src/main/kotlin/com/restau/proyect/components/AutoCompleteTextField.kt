@file:Suppress("UNUSED_EXPRESSION")

package com.restau.proyect.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.restau.proyect.theme.POPPINS
import kotlin.reflect.KProperty1

data class ParamsData<T>(
    val items: List<T>,
    val onItemSelected: (T) -> Unit,
    val modifier: Modifier = Modifier,
    val label: String? = null,
    val placeholder: String? = null,
    val atributo: KProperty1<T, *>,
    val trailingIcon: @Composable (() -> Unit) = {},
    val onSearch: (search: String) -> Unit = {},
    val clearfield: Boolean = false
)


/*
@Composable
inline fun <reified T> AutoCompleteTextField(
    params: ParamsData<T>,
    textStyle: TextStyle = MaterialTheme.typography.body1,
    colors: TextFieldColors = TextFieldDefaults.textFieldColors(),
) {
    var searchText by remember { mutableStateOf("") }
    var expanded by remember { mutableStateOf(false) }

    val filteredItems = remember(params.items, searchText) {
        params.items.filter {
            return@filter params.atributo.get(it).toString().contains(searchText, ignoreCase = true)
        }
    }

    Column(modifier = Modifier.then(params.modifier), horizontalAlignment = Alignment.Start) {

        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(1f).onFocusChanged {
                expanded = it.isFocused
            },
            value = searchText,
            onValueChange = {
                searchText = it
                expanded = true
                params.onSearch(it)
            },
            label = { params.label?.let { Text(text = it) } },
            placeholder = { params.placeholder?.let { Text(text = it) } },
            trailingIcon = {
                params.trailingIcon()
            },
            textStyle = textStyle,
            colors = colors,
        )
        if (expanded && filteredItems.isNotEmpty()) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .zIndex(1f)
            ) {
                Card(
                    modifier = Modifier.fillMaxWidth(),
                    shape = MaterialTheme.shapes.medium,
                    elevation = 8.dp
                ) {
                    DropdownMenu(
                        expanded,
                        { expanded = false },
                        focusable = false,
                        modifier = Modifier.wrapContentWidth()
                    ) {
                        filteredItems.forEach { item ->
                            DropdownMenuItem(
                                {
                                    params.onItemSelected(item)
                                    expanded = false
                                    searchText = params.atributo.get(item).toString()
                                }, modifier = Modifier
                                    .wrapContentWidth()
                                    .background(MaterialTheme.colors.surface)
                            ) {
                                Text(
                                    text = params.atributo.get(item).toString(),
                                    modifier = Modifier
                                        .wrapContentWidth()
                                        .padding(16.dp)
                                )


                            }
                            Divider(
                                color = MaterialTheme.colors.onSurface.copy(alpha = 0.2f)
                            )
                        }
                    }
                }
            }

        }
    }
}
*/


@Composable
inline fun <reified T> AutoCompleteTextField(
    params: ParamsData<T>,
    textStyle: TextStyle = MaterialTheme.typography.body1,
    colors: TextFieldColors = TextFieldDefaults.textFieldColors(),
) {
    var searchText by remember { mutableStateOf("") }
    var expanded by remember { mutableStateOf(false) }

    val filteredItems = remember(params.items, searchText) {
        params.items.filter {
            return@filter params.atributo.get(it).toString().contains(searchText, ignoreCase = true)
        }
    }

    Column(modifier = Modifier.then(params.modifier), horizontalAlignment = Alignment.Start) {
        val textFieldWidth = remember { mutableStateOf(0.dp) }

        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth(1f)
                .onFocusChanged {
                    expanded = it.isFocused
                }
                .onSizeChanged { coordinates ->
                    textFieldWidth.value = (coordinates.width * 0.8).dp
                },
            value = searchText,
            onValueChange = {
                searchText = it
                expanded = true
                params.onSearch(it)
            },
            label = { params.label?.let { Text(text = it) } },
            placeholder = { params.placeholder?.let { Text(text = it) } },
            trailingIcon = {
                params.trailingIcon()
            },
            textStyle = textStyle,
            colors = colors,
        )
        if (expanded && filteredItems.isNotEmpty()) {
            Box(
                modifier = Modifier
                    .width(textFieldWidth.value)
                    .wrapContentHeight()
                    .zIndex(1f)
            ) {
                Card(
                    modifier = Modifier.width(textFieldWidth.value),
                    shape = MaterialTheme.shapes.medium,
                    elevation = 8.dp
                ) {
                    DropdownMenu(
                        expanded,
                        { expanded = false },
                        focusable = false,
                        modifier = Modifier
                            .width(textFieldWidth.value)
                            .heightIn(max = 200.dp)
                    ) {
                        filteredItems.forEach { item ->
                            DropdownMenuItem(
                                {
                                    params.onItemSelected(item)
                                    expanded = false
                                    searchText = if (params.clearfield) {
                                        ""
                                    }else{
                                        params.atributo.get(item).toString()
                                    }
                                }, modifier = Modifier
                                    .wrapContentWidth()
                                    .background(MaterialTheme.colors.surface)
                            ) {
                                Text(
                                    text = params.atributo.get(item).toString(),
                                    fontFamily = FontFamily.POPPINS(),
                                    modifier = Modifier
                                        .wrapContentWidth()
                                        .padding(16.dp)
                                )
                            }
                            Divider(
                                color = MaterialTheme.colors.onSurface.copy(alpha = 0.2f)
                            )
                        }
                    }
                }
            }
        }
    }
}

