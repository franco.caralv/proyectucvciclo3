package com.restau.proyect.usecase.createAcount

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Person
import com.restau.proyect.dominan.remote.PersonaDB
import com.restau.proyect.usecase.createAcount.event.PersonListEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class CrearCuenta(
    @Autowired private val listPersonPreferent: PersonaDB,
    @Autowired private val searchCuenta: SearchCuenta
) {


    suspend operator fun invoke(person: Person): PersonListEvent {
        return if (!searchCuenta(person).first) {
            when (val value = listPersonPreferent.savePersona(person)) {
                is Customer.onSuccess -> {
                    PersonListEvent.PersonSuccess(value.info)
                }

                is Customer.onError -> {
                    PersonListEvent.PersonError(value.error.mensaje.toString())
                }
            }
        } else {
            PersonListEvent.PersonError("Error no se puede crear el usuario valida el correo o la contraseña")
        }


    }


}