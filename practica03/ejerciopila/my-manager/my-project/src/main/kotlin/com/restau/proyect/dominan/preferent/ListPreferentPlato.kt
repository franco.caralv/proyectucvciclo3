package com.restau.proyect.dominan.preferent

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Plato
import com.restau.proyect.dominan.preferent.base.ListPreferentBase
import com.restau.proyect.dominan.preferent.extends.generarId
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository


@Repository
@Lazy
class ListPreferentPlato(
    @Autowired override val usuariopreferent: PersonLogin,
) : ListPreferentBase<List<Plato>>(usuariopreferent) {

    override val typeToken: TypeToken<List<Plato>> = object : TypeToken<List<Plato>>() {}

    @Value("\${app.secretKey}")
    override lateinit var key: String

    override val nameDocumento: String = "platps"
    override var fileocumento: String = "listplatos"
    override var listpersonpre: StorageService<List<Plato>>? = null
    override val _listCategoria: MutableStateFlow<List<Plato>> = MutableStateFlow<List<Plato>>(listOf())
    override val listCategoria: StateFlow<List<Plato>> = _listCategoria.asStateFlow()


    fun guardarPlato(plato: Plato): Customer<List<Plato>> {
        return try {
            _listCategoria.value = _listCategoria.value.toMutableList().apply {
                add(plato.copy(id = this.generarId()))
            }
            listpersonpre?.save(_listCategoria.value, fileocumento)
            Customer.onSuccess(_listCategoria.value)
        } catch (e: Exception) {
            Customer.onError(Error(500))
        }

    }



    fun deletePlato(id:Int?):Customer<List<Plato>>{
        return try {
            _listCategoria.value = _listCategoria.value.toMutableList().filter { it.id!=id }.toList()
            listpersonpre?.save(_listCategoria.value, fileocumento)
            Customer.onSuccess(_listCategoria.value)
        } catch (e: Exception) {
            Customer.onError(Error(500))
        }
    }


}