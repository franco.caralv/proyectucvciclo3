package com.restau.proyect.usecase.createPMC.modificador

import com.restau.proyect.data.Modificador
import com.restau.proyect.dominan.remote.ModificadordDB
import com.restau.proyect.dominan.remote.RestaurantDB
import com.restau.proyect.usecase.createPMC.base.SearchModificador
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.util.logging.Level
import java.util.logging.Logger


@Service
@Lazy
class SearchModificadormlp(
    @Autowired private val lisModificadoresPreferent: ModificadordDB,
    @Autowired private val listPreferentRestaurante: RestaurantDB
):SearchModificador {

   override operator fun invoke(): Flow<List<Modificador>> {
        val restaurante = listPreferentRestaurante.restaurant
        return lisModificadoresPreferent.listModificador.map { lismodi ->
            lismodi
        }.filterNotNull().catch {
            Logger.getLogger("Error $it").apply {
                level = Level.INFO
            }
        }
    }

}