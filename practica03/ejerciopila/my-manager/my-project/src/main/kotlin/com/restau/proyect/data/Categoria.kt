package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import io.ebean.annotation.Length
import javax.persistence.*

@Entity
@Table(name = "Categoria")
data class Categoria(
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    override var id: Int? = null,
    @field:SerializedName("nombre")
    @Column
    @Length(40)
    var nombre: String? = null,
    @field:SerializedName("descripcion")
    @Column
    @Length(100)
    var descripcion: String? = null,
    @field:SerializedName("porcentajepopular")
    @Column
    var porcentajepopular: Double? = null,
    @field:SerializedName("idRestaurant")
    @ManyToOne
    @JoinColumn(name = "id_restaurant", referencedColumnName = "id")
    var idRestaurant: Restaurant? = null,
    @field:SerializedName("plato")
    @OneToOne(mappedBy = "categoria")
    var plato: Plato? = null
) : BaseEntity, Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}
