@file:Suppress("IMPLICIT_CAST_TO_ANY")

package com.restau.proyect.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.outlined.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import com.restau.proyect.theme.POPPINS


@ExperimentalComposeUiApi
@Composable
fun TextFieldEmail(
    email: String,
    label: String = "Your Email",
    onValueChange: (valor: String) -> Unit
) {
    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current
    OutlinedTextField(value = email, label = {
        Text(text = label,
            fontFamily = FontFamily.POPPINS(),)
    }, leadingIcon = {
        Icon(Icons.Filled.Person, contentDescription = "CORREO ELECTRONICO")
    }, onValueChange = {
        onValueChange(it)
    }, modifier = Modifier.fillMaxWidth(),
        maxLines = 1,
        keyboardActions = KeyboardActions(
            onNext = {
                focusManager.moveFocus(FocusDirection.Down)
                keyboardController?.hide()
            }
        ),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Email,
            imeAction = ImeAction.Next
        )
    )
}

@ExperimentalComposeUiApi
@Composable
fun TextfieldBusqueda(value: String, onValueChange: (it: String) -> Unit, onClick: () -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current
    OutlinedTextField(value = value, onValueChange = {
        onValueChange(it)
    }, label = {
        TextRegural(text = "Search for your order ")
    }, modifier = Modifier.fillMaxWidth(),
        trailingIcon = {
            Button(
                onClick = {
                    onClick()
                    keyboardController?.hide()
                },
                modifier = Modifier.padding(horizontal = 10.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = MaterialTheme.colors.primary,
                    contentColor = Color.Black
                )
            ) {
                Icon(
                    Icons.Outlined.Search,
                    contentDescription = "",
                )
            }

        })
}


@ExperimentalComposeUiApi
@Composable
fun TexfieldPassword(
    password: String,
    label: String = "Your Password",
    modifier: Modifier = Modifier.fillMaxWidth(),
    onChange: (it: String) -> Unit
) {
    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current
    var isvisible by rememberSaveable {
        mutableStateOf(true)
    }
    OutlinedTextField(value = password, leadingIcon = {
        Icon(Icons.Default.Lock, contentDescription = "")
    }, trailingIcon = {
        IconButton(onClick = {
            isvisible = !isvisible
        }) {
            Icon(
                painter = if (isvisible) painterResource("drawables/iconos/visibilidad-desactivada.png") else painterResource(
                    "drawables/iconos/boton-de-visibilidad.png"
                ),
                contentDescription = "",
                modifier = Modifier.size(25.dp)
            )
        }
    }, label = {
        Text(text = label)
    }, onValueChange = {
        onChange(it)
    }, modifier = modifier,
        keyboardActions = KeyboardActions(
            onDone = {
                focusManager.moveFocus(FocusDirection.Down)
                focusManager.moveFocus(FocusDirection.Down)
                keyboardController?.hide()
            }
        ),
        maxLines = 1,
        visualTransformation = if (isvisible) PasswordVisualTransformation() else VisualTransformation.None,
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Done
        )
    )
}


@Composable
fun TextRegural(
    text: String,
    modifier: Modifier = Modifier,
    color: Color = Color.Unspecified,
    fontSize: TextUnit = TextUnit.Unspecified,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration? = null,
    textAlign: TextAlign? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    minLines : Int = 1,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    style: TextStyle = LocalTextStyle.current
) {
    Text(
        text,
        modifier,
        color,
        fontSize,
        fontStyle,
        fontWeight,
        fontFamily,
        letterSpacing,
        textDecoration,
        textAlign,
        lineHeight,
        overflow,
        softWrap,
        maxLines,
        onTextLayout,
        style
    )

}



