package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Person")
data class Person(
    @field:SerializedName("name")
    var name: String? = null,
    @field:SerializedName("password")
    var password: String? = null,
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null,
    @field:SerializedName("correo")
    var correo: String? = null,
    @field:SerializedName("ciudad")
    @OneToOne
    var ciudad: Ciudad? = null,
    @field:SerializedName("restaurant")
    @OneToMany(mappedBy = "persona", cascade = [CascadeType.ALL])
    var restaurant: List<Restaurant>? = null,
) : Model(){
    override fun toString(): String {
        return  toStringPrimitivo()
    }
}
