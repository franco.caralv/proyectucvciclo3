package com.restau.proyect.dominan.remote

import com.restau.proyect.data.Person
import com.restau.proyect.data.Restaurant
import com.restau.proyect.dominan.preferent.PersonLogin
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class RestaurantDB(
    @Autowired private val personLogin: PersonLogin,
    @Autowired private val db: Database,
) {
    var restaurant: Restaurant? = null


    @PostConstruct
    fun init() {
        personLogin.person?.id?.let {
            db.find(Restaurant::class.java).where().eq("person_id", it).findOne()?.let { restaurant ->
                this.restaurant = restaurant
            }
        }
    }

    suspend fun newsaveRestaurant(persona: Person, _restaurant: Restaurant = Restaurant()): Restaurant =
        withContext(Dispatchers.IO) {
            _restaurant.persona = persona
            persona.restaurant = persona.restaurant?.toMutableList().apply {
                this?.add(_restaurant)
            } ?: listOf(_restaurant)
            db.save(persona)
            restaurant = getlastRestaurant()
            return@withContext getlastRestaurant() ?: _restaurant
        }


    private fun getlastRestaurant(): Restaurant? {
        return db.find(Restaurant::class.java).findList().lastOrNull()
    }

}