package com.restau.proyect.usecase.createPMC.event

import com.restau.proyect.data.Plato

sealed class ReponsePLato {

    object Error:ReponsePLato()
    object ErrordeFile:ReponsePLato()
    class Sucess(listPlato:List<Plato>):ReponsePLato()
    class ErrorCampos(val descriptor:String):ReponsePLato()
}