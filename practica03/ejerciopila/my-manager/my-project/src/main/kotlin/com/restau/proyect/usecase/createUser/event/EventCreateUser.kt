package com.restau.proyect.usecase.createUser.event

import com.restau.proyect.data.Mozo

sealed class EventCreateUser {

    class listenerField(val mozo: Mozo) : EventCreateUser()
    object saveMozo : EventCreateUser()
    class deleteMozo(val mozo: Mozo) : EventCreateUser()
    object clearField : EventCreateUser()

}