package com.restau.proyect.base.http.client

import com.restau.proyect.base.http.BaseClient
import com.restau.proyect.base.http.Respnse.Customer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate


@Component
@Lazy
class ClientImpl : BaseClient {

    @Value("\${app.urlBase}")
    lateinit var urlBase: String

    @Autowired
    lateinit var httpClient: RestTemplate

    override suspend fun <T> get(
        url: String,
        responseType: ParameterizedTypeReference<T>,
        onProcess: (suspend (response: ResponseEntity<T>) -> Unit)?
    ): Customer<T> {
        return withContext(Dispatchers.IO) {
            val headers = HttpHeaders()
            headers.accept = listOf(MediaType.APPLICATION_JSON)
            val httpEntity = HttpEntity<Any>(headers)
            try {
                val http = httpClient.exchange(
                    if (url.uppercase().contains("http".uppercase())) url else "$urlBase$url",
                    HttpMethod.GET,
                    httpEntity,
                    responseType
                )
                onProcess?.invoke(http)
                if (http.statusCode == HttpStatus.OK) {
                    return@withContext Customer.onSuccess(http.body!!)
                }
                return@withContext Customer.onError(
                    com.restau.proyect.base.http.Respnse.Error(
                        http.statusCodeValue,
                        http.body.toString()
                    )
                )
            } catch (e: Exception) {
                return@withContext Customer.onError<T>(com.restau.proyect.base.http.Respnse.Error(500))
            }

        }

    }


    override suspend fun <T, K> post(
        url: String,
        objrequest: T,
        responseType: ParameterizedTypeReference<K>,
        onProcess: (suspend (response: ResponseEntity<K>) -> Unit)?
    ): Customer<K> {
        return withContext(Dispatchers.IO) {
            val headers = HttpHeaders()
            headers.contentType = MediaType.APPLICATION_JSON
            val httpEntity = HttpEntity(objrequest, headers)
            try {
                val responseEntity: ResponseEntity<K> = httpClient.exchange(
                    if (url.uppercase().contains("http".uppercase())) url else "$urlBase$url",
                    HttpMethod.POST,
                    httpEntity,
                    responseType
                )
                onProcess?.invoke(responseEntity)
                if (responseEntity.statusCode == HttpStatus.OK) {
                    return@withContext Customer.onSuccess(responseEntity.body!!)
                }
                return@withContext Customer.onError(
                    com.restau.proyect.base.http.Respnse.Error(
                        responseEntity.statusCodeValue,
                        responseEntity.body.toString()
                    )
                )
            } catch (e: Exception) {
                return@withContext Customer.onError(com.restau.proyect.base.http.Respnse.Error(500, e.message))
            }
        }
    }


}