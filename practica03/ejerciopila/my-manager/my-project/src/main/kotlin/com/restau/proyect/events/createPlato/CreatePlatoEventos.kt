package com.restau.proyect.events.createPlato

import com.restau.proyect.data.Categoria
import com.restau.proyect.data.Modificador
import com.restau.proyect.data.Plato

sealed class CreatePlatoEventos {

    class OyenteDeTexPlato(val plato: Plato):CreatePlatoEventos()
    class OyenteDeTexCategoria(val categoria: Categoria):CreatePlatoEventos()
    class OyenteDeTexModificador(val modificador: Modificador):CreatePlatoEventos()
    object GuardarPlato:CreatePlatoEventos()
    object BorrarPlato:CreatePlatoEventos()
    object GuardarModifcador:CreatePlatoEventos()
    object BorrarModificador:CreatePlatoEventos()
    object Guardarcategoria:CreatePlatoEventos()
    object Borrarcategoria:CreatePlatoEventos()

}