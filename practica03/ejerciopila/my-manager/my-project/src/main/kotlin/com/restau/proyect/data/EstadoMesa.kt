package com.restau.proyect.data

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AutoGraph
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import com.google.gson.annotations.SerializedName
import com.restau.proyect.theme.*


sealed class EstadoMesa(
    @field:SerializedName("estado")
    var estado: String? = null,
    @field:SerializedName("id")
    var id: String? = null,
    @field:SerializedName("color")
    val color: Color? = null,
    @field:SerializedName("icon")
    val icon: ImageVector? = null,
) {
    object Ocupado : EstadoMesa("Ocupado", "1", oro, Icons.Default.AutoGraph)
    object Limpiando : EstadoMesa("Limpiando", "2", azul, Icons.Default.AutoGraph)
    object Desocupado : EstadoMesa("Desocupado", "3", verdeclaro, Icons.Default.AutoGraph)
    object Sucio : EstadoMesa("Sucio", "4", naranja, Icons.Default.AutoGraph)
    object Molesto : EstadoMesa("Molesto", "5", rojointenso, Icons.Default.AutoGraph)
}