package com.restau.proyect.views.dialogs

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogState
import androidx.compose.ui.window.rememberDialogState
import com.restau.proyect.components.p.P
import com.restau.proyect.events.createPlato.CreatePlatoEventos
import com.restau.proyect.viewmodel.CrearPlatoModel


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DialogoCreateCategoria(
    viewmodel: CrearPlatoModel,
    visible: Boolean, onRequest: () -> Unit,
    state: DialogState = rememberDialogState(
        width = 500.dp,
        height = 400.dp,
    ),
) {

    val category by viewmodel.categoria.collectAsState()


    Dialog(
        { onRequest() },
        state,
        visible, undecorated = false, resizable = false,
        title = "Agrega tus catrgorias =) "
    ) {
        Column(modifier = Modifier.fillMaxWidth().fillMaxHeight().padding(10.dp)) {
            OutlinedTextField(category.nombre ?: "", {
                viewmodel.emit(CreatePlatoEventos.OyenteDeTexCategoria(category.copy(nombre = it)))
            }, modifier = Modifier.fillMaxWidth(), label = {
                P("Nombre")
            },singleLine = true)
           /* OutlinedTextField((category.precio ?: 0.0).toString(), { newValue ->
                if (newValue.isEmpty() || newValue.all { it.isDigit() || it == '.' }) {
                    val newPrice = newValue.toDoubleOrNull()
                    viewmodel.emit(
                        CreatePlatoEventos.OyenteDeTexCategoria(
                            category.copy(
                                precio = if (newPrice != 0.0) newPrice else null
                            )
                        )
                    )
                }
            }, modifier = Modifier.fillMaxWidth(), label = {
                P("Precio")
            })*/
            OutlinedTextField(category.descripcion ?: "", {
                viewmodel.emit(CreatePlatoEventos.OyenteDeTexCategoria(category.copy(descripcion = it)))
            }, modifier = Modifier.fillMaxWidth(), label = {
                P("Descripcion")
            },singleLine = true)
            Button({
                viewmodel.emit(CreatePlatoEventos.Guardarcategoria)
                onRequest()
            }, modifier = Modifier.fillMaxWidth()) {
                P("Crear Categoria")
            }
            Button({
                viewmodel.emit(CreatePlatoEventos.Borrarcategoria)
            }, modifier = Modifier.fillMaxWidth()) {
                P("Borrar Categoria")
            }
        }
    }

}