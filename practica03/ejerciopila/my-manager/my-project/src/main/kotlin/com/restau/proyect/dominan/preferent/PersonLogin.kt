package com.restau.proyect.dominan.preferent

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Person
import io.ebean.Database
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
@Lazy
class PersonLogin(
    @Autowired val db: Database
) {


    @Value("\${app.secretKey}")
    lateinit var key: String

    lateinit var personstorage: StorageService<Person>

    val filname = "person"

    val person: Person?
        get() {
            return personstorage.load(filname)
        }

    @PostConstruct
    fun init() {
        personstorage = StorageService<Person>(object : TypeToken<Person>() {}, "listperson", key)
    }


    fun checkperson(): Boolean {
        println(person)
        person?.id?.let {
            val persona = db.find(Person::class.java).where().eq("id", it).findOne()
            println(persona)

            if (persona == null)
                delete()
            return persona != null
        }
        return false
    }


    operator fun plus(per: Person) {
        personstorage.save(per.copy(restaurant = null, ciudad = null), filname)
    }

    fun delete() {
        personstorage.delete(filname)
    }


}