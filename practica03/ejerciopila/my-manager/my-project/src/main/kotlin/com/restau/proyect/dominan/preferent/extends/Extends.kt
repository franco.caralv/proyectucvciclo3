package com.restau.proyect.dominan.preferent.extends

import com.restau.proyect.data.BaseEntity

fun <E:BaseEntity> MutableList<E>.generarId():Int{
    return if (this.isEmpty()) 1 else this.last().id
         ?.let { (it + 1) } ?: 1
}





