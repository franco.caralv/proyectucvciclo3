package com.restau.proyect.entity

import androidx.compose.ui.graphics.Color
import com.restau.proyect.theme.Tundora

data class ParamsStyledTable(
    var data: List<List<Any>>,
    var columnHeaders: List<String>,
    var columnSortFunctions: List<((List<Any>) -> Int)>,
    var colorheader: Color = Tundora.copy(0.8f),
    var isconfiguracion: Boolean = true,
    val onClickIcon: (indexfila:Int, data: Any) -> Unit = { index,data->

    }
)

