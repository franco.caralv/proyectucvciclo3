package com.restau.proyect.usecase.createPMC.base

import com.restau.proyect.data.Modificador
import com.restau.proyect.usecase.createPMC.event.ResponseModificador

interface AddModificador {


    suspend operator fun invoke(modificador: Modificador): ResponseModificador
}