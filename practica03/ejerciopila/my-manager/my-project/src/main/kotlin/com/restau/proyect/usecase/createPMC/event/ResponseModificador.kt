package com.restau.proyect.usecase.createPMC.event

import com.restau.proyect.data.Modificador

sealed class ResponseModificador {

    object ErrorData:ResponseModificador()

    class SuccessData(list: List<Modificador>):ResponseModificador()

    class ErrorSinData(val discripcion:String):ResponseModificador()

}