package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Modificador
import io.ebean.Database
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class ModificadordDB(
    @Autowired private val restaurant: RestaurantDB,
    @Autowired val db: Database,
) {
    private val _listModificador = MutableStateFlow<List<Modificador>>(listOf())

    val listModificador = _listModificador.asStateFlow()

    @PostConstruct
    fun cargandoModificadores() {
        _listModificador.value = modificadorbyrestaurant(restaurant.restaurant?.id)
    }

    private fun modificadorbyrestaurant(id: Int?): List<Modificador> {
        return id?.let {
            db.find(Modificador::class.java).where().eq("t0.id_restaurant", it).findList()
        } ?: listOf()
    }

    fun add(modificador: Modificador): Customer<List<Modificador>> {
        return try {
            modificador.idRestaurant = restaurant.restaurant
            db.save(modificador)
            _listModificador.value = modificadorbyrestaurant(restaurant.restaurant?.id)
            return Customer.onSuccess(_listModificador.value)
        } catch (e: Exception) {
            println("e"+e.message)
            Customer.onError(Error(500, "No se logro guardar"))
        }

    }


    fun delete(modificador: Modificador): Customer<List<Modificador>> {
        return try {
            db.delete(modificador)
            _listModificador.value = modificadorbyrestaurant(restaurant.restaurant?.id)
            return Customer.onSuccess(_listModificador.value)
        } catch (e: Exception) {
            println("Me encuentro en delete ")
            println(e.message)
            Customer.onError(Error(500, "No se logro guardar"))
        }
    }


}