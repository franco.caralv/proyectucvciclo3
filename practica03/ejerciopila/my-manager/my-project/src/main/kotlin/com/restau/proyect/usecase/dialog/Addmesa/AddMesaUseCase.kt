package com.restau.proyect.usecase.dialog.Addmesa

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Mesa
import com.restau.proyect.data.Restaurant
import com.restau.proyect.dominan.remote.MesasDB
import com.restau.proyect.usecase.dialog.Addmesa.Event.MesasEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class AddMesaUseCase(
    @Autowired private val listmesasPreferent: MesasDB
) {


    suspend operator fun invoke(mesa: Mesa, restaurant: Restaurant): MesasEvent {

        return when (val value = listmesasPreferent.add(mesa)) {
            is Customer.onSuccess -> {
                MesasEvent.mesaSucess(value.info)
            }

            is Customer.onError -> {
                return MesasEvent.mesasError(value.error.mensaje.toString())
            }
        }
    }


}