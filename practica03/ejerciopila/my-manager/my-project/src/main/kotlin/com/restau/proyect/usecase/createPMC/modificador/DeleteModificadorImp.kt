package com.restau.proyect.usecase.createPMC.modificador

import com.restau.proyect.base.UseCaseBase
import com.restau.proyect.data.Modificador
import com.restau.proyect.dominan.remote.ModificadordDB
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class DeleteModificadorImp(
    @Autowired private val lisModificadoresPreferent: ModificadordDB,
) : UseCaseBase<Modificador, Unit> {


    override suspend fun invoke(data: Modificador) {
        lisModificadoresPreferent.delete(data)
    }


}