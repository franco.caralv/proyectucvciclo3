package com.restau.proyect.viewmodel

import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.data.Ciudad
import com.restau.proyect.data.Person
import com.restau.proyect.events.createAcount.CreateCuenta
import com.restau.proyect.router.Controller
import com.restau.proyect.usecase.createAcount.CrearCuenta
import com.restau.proyect.usecase.createAcount.CreateAcountSearchCiudad
import com.restau.proyect.usecase.createAcount.event.PersonListEvent
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import javax.annotation.PostConstruct


@Scope("prototype")
@org.springframework.stereotype.Controller
@org.springframework.context.annotation.Lazy
class CreateAcountModel(
    @Autowired private val createAcount: CreateAcountSearchCiudad,
    @Autowired private val createCuenta: CrearCuenta
) : ViewModelbase() {


    private val _listciudad = MutableStateFlow(listOf<Ciudad>())
    val listciudad = _listciudad.asStateFlow()

    private val _person = MutableStateFlow(Person())
    val person = _person.asStateFlow()

    private val _password = MutableStateFlow("")
    val password = _password.asStateFlow()

    private val _newpassword = MutableStateFlow("")
    val newpassword = _newpassword.asStateFlow()

    private val _enable = MutableStateFlow(false)
    val enable = _enable.asStateFlow()

    private val _loading = MutableStateFlow(false)
    val loading = _loading.asStateFlow()


    private val _enablepass = MutableStateFlow(false)
    val enablepass = _enablepass.asStateFlow()

    private val _enablecorreo = MutableStateFlow(false)
    val enablecorreo = _enablecorreo.asStateFlow()


    @PostConstruct
    private fun escuchadodeeven() = launch {
        _listciudad.value = createAcount()
    }

    private suspend fun listenerevent(it: CreateCuenta) {
        when (it) {
            is CreateCuenta.Crear -> {
                _enable.value = false
                emitevent(CreateCuenta.Loading)
                when (val mesasje = createCuenta(it.person.copy(password = password.value))) {
                    is PersonListEvent.PersonSuccess -> {
                        it.snackbarHostState.showSnackbar("Se creo correctamente")
                        _enable.value = true
                        emitevent(CreateCuenta.None)
                        Controller.pop()
                    }

                    is PersonListEvent.PersonError -> {
                        _enable.value = true
                        it.snackbarHostState.showSnackbar(mesasje.error)
                        emitevent(CreateCuenta.None)
                    }
                }
            }

            is CreateCuenta.None -> {
                _loading.value = false
            }

            is CreateCuenta.Loading -> {
                _enable.value = false
                _loading.value = true
            }

            is CreateCuenta.ValidarPassword -> {
                _enablepass.value = it.pass == it.passnew
                _newpassword.value = it.passnew
                _password.value = it.pass
                _enable.value = _enablepass.value && _enablecorreo.value
            }

            is CreateCuenta.validarCorreo -> {
                _enablecorreo.value = validarCorreo(it.correo)
                _enable.value = _enablepass.value && _enablecorreo.value
                emitevent(CreateCuenta.ListTex(_person.value.copy(correo = it.correo)))
            }

            is CreateCuenta.ListTex -> {
                _person.value = it.person
                _enable.value = _enablepass.value && _enablecorreo.value
            }
        }
    }

    private fun validarCorreo(correo: String): Boolean {
        val regex = Regex("^\\w+([.-]?\\w+)*@\\w+([.-]?\\w+)*(\\.\\w{2,3})+\$")
        return regex.matches(correo)
    }

    fun emitevent(event: CreateCuenta)=launch {
        listenerevent(event)
    }


}