package com.restau.proyect.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.ScaffoldState
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Logout
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.restau.proyect.router.Controller
import com.restau.proyect.router.Flujo
import com.restau.proyect.router.Router
import com.restau.proyect.theme.POPPINS
import com.restau.proyect.theme.azul
import com.restau.proyect.util.Constans
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Composable
fun Drawer(
    scope: CoroutineScope,
    scaffoldState: ScaffoldState,
    items: List<Router>,
    onExit: () -> Unit = {

    }
) {
    Column(verticalArrangement = Arrangement.SpaceBetween, modifier = Modifier.fillMaxHeight()) {
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Image(
                painter = painterResource(Constans.LOGOSINFONDO),
                contentDescription = "Bg Image",
                modifier = Modifier
                    .fillMaxHeight(0.4f)
                    .fillMaxWidth(0.4f),
                contentScale = ContentScale.FillBounds
            )

        }
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(15.dp)
        )
        val router by Controller.state.collectAsState(Dispatchers.IO)

        LazyColumn(modifier = Modifier.fillMaxHeight(0.8f)) {
            items(items) {
                if (it.flujo == Flujo.Parque) {
                    DrawerItem(item = it.castToModuloSingle(), selected = router.url == it.url) {
                        scope.launch(Dispatchers.IO) {
                            scaffoldState.drawerState.close()
                            if (Controller.lastElement().url != it?.url && it != null)
                                Controller.plus(it)
                        }
                    }
                }
            }
        }


        Row(modifier = Modifier.align(Alignment.End).fillMaxWidth()
            .height(56.dp)
            .padding(6.dp)
            .clip(RoundedCornerShape(12))
            .padding(8.dp)
            .clickable {
                scope.launch {
                    scaffoldState.drawerState.close()
                    onExit()
                }
            }) {
            Icon(
                modifier = Modifier.size(32.dp),
                imageVector = Icons.Outlined.Logout,
                contentDescription = "",
            )
            Spacer(modifier = Modifier.width(12.dp))
            Text(
                text = "Exit",
                fontFamily = FontFamily.POPPINS(),
                style = androidx.compose.ui.text.TextStyle(fontSize = 18.sp),
            )
        }

    }
}

@Composable
fun DrawerItem(
    item: Router.ModuloSingle?,
    selected: Boolean,
    onItemClick: (Router.ModuloSingle?) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
            .padding(6.dp)
            .clip(RoundedCornerShape(12))
            .background(if (selected) azul.copy(alpha = 0.25f) else Color.Transparent)
            .padding(8.dp)
            .clickable { onItemClick(item) },
        verticalAlignment = Alignment.CenterVertically
    ) {
        if (item?.icon != null)
            Icon(
                modifier = Modifier.size(32.dp),
                imageVector = item.icon,
                contentDescription = item.title,
                tint = if (selected) azul else Color.Gray
            )
        Spacer(modifier = Modifier.width(12.dp))
        Text(
            text = item?.title ?: "",
            fontFamily = FontFamily.POPPINS(),
            style = androidx.compose.ui.text.TextStyle(fontSize = 18.sp),
            color = if (selected) azul else Color.Black
        )
    }
}
