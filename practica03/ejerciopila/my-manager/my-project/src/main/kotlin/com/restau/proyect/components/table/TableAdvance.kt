package com.restau.proyect.components.table

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.restau.proyect.entity.ParamsStyledTable


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun StyledTable(
    params: ParamsStyledTable
) {
    val sortedData = remember(params.data) { mutableStateOf(params.data) }
    val sortOrder = remember { mutableStateListOf(*Array(params.columnHeaders.size) { true }) }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.LightGray)
            .padding(16.dp)
    ) {
        Row {
            params.columnHeaders.forEachIndexed { index, columnHeader ->
                Card(
                    onClick = {
                        sortOrder[index] = !sortOrder[index]
                        val sortedList = if (sortOrder[index]) {
                            sortedData.value.sortedBy(params.columnSortFunctions[index])
                        } else {
                            sortedData.value.sortedByDescending(params.columnSortFunctions[index])
                        }
                        sortedData.value = sortedList
                    },
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp).heightIn(35.dp, 40.dp),
                    backgroundColor = params.colorheader,
                    contentColor = params.colorheader,
                    shape = RoundedCornerShape(4.dp),
                ) {
                    Box(Modifier, Alignment.Center) {
                        Text(text = columnHeader, color = Color.White, textAlign = TextAlign.Center)
                    }
                }
            }
            if (params.isconfiguracion)
                Card(
                    modifier = Modifier
                        .weight(1f)
                        .padding(8.dp).heightIn(35.dp, 40.dp),
                    backgroundColor = params.colorheader,
                    contentColor = params.colorheader,
                    shape = RoundedCornerShape(4.dp),
                ) {
                    Box(Modifier, Alignment.Center) {
                        Text(text = "ACCIONES", color = Color.White, textAlign = TextAlign.Center)
                    }
                }
        }
        sortedData.value.forEachIndexed { index, row ->
            Row(verticalAlignment = Alignment.CenterVertically) {
                row.forEach { cell ->
                    Text(
                        text = cell.toString(),
                        modifier = Modifier
                            .weight(1f)
                            .padding(8.dp)
                            .background(Color.White, RoundedCornerShape(4.dp))
                            .padding(8.dp),
                        textAlign = TextAlign.Center
                    )
                }
                if (params.isconfiguracion)
                    IconButton(
                        {
                            params.onClickIcon(index, row.first())
                        },
                        modifier = Modifier
                            .weight(1f)
                            .padding(8.dp)
                            .background(Color.White, RoundedCornerShape(4.dp))
                            .padding(8.dp).height(20.dp),
                    ) {
                        Icon(Icons.Outlined.Delete, "")
                    }
            }
        }
    }
}


@Preview
@Composable
fun StyledTablePreview() {
    val data = listOf(
        listOf(1, "A", "Apple"),
        listOf(2, "B", "Banana"),
        listOf(3, "C", "Cherry"),
        listOf(4, "D", "Dragonfruit")
    )

    val columnHeaders = listOf("ID", "Letter", "Fruit")

    val columnSortFunctions = listOf<(List<Any>) -> Int>(
        { row -> row[0] as Int },
        { row -> (row[1] as String).single().toInt() },
        { row -> (row[2] as String).first().toInt() }
    )
    val params = ParamsStyledTable(data, columnHeaders, columnSortFunctions) { indexfila, data ->

    }

    StyledTable(params)
}
//En este ejemplo, hemos introducido el uso de colores, formas y alineación para mejorar el aspecto de la tabla. Hemos añadido un color de fondo gris claro al componente principal de la columna, hemos utilizado un botón oscuro para los encabezados de las columnas y hemos añadido esquinas redondeadas a los botones y a las celdas de la tabla. También, hemos centrado verticalmente el texto dentro de las celdas de la tabla para mejorar el alineamiento.






