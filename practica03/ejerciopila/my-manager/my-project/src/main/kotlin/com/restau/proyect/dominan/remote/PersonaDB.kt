package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Person
import com.restau.proyect.dominan.preferent.PersonLogin
import com.restau.proyect.util.Utility
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository

@Repository
@Lazy
class PersonaDB(
    @Autowired val db: Database,
    @Autowired private val utility: Utility,
    @Autowired private val personLogin: PersonLogin,
) {

    var listperson: List<Person> = listOf()
        private set
        get() {
            return db.find(Person::class.java).findList()
        }


    suspend fun savePersona(persona: Person): Customer<Person> = withContext(Dispatchers.IO) {
        return@withContext try {
            persona.password = utility.createhas(persona.password ?: "")
            db.save(persona.copy(id = null))
            Customer.onSuccess(db.find(Person::class.java).findList().last())
        } catch (e: Exception) {
            println("Me encuentro en savePersona ")
            println(e.message)
            Customer.onError(Error(500, mensaje = e.message))
        }
    }

    suspend fun searchpersonawithRestaurante(id: Int): Customer<Person> = withContext(Dispatchers.IO) {
        try {
            val persona =
                db.find(Person::class.java).fetch("Restaurant", " * ").where().eq("id", id.toString()).findOne()

            persona?.let {
                return@withContext Customer.onSuccess(
                    it
                )
            }
            return@withContext Customer.onError(Error(400))
        } catch (e: Exception) {
            println("Me encuentro en searchpersonawithRestaurante ")
            println(e.message)
            return@withContext Customer.onError(Error(500, mensaje = e.message))
        }
    }


}