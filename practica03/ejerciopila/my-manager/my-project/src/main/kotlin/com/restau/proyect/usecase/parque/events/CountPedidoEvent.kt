package com.restau.proyect.usecase.parque.events

sealed class CountPedidoEvent {

    object ErrorDB:CountPedidoEvent()
    class SuccessCount(val count:Int):CountPedidoEvent()

}