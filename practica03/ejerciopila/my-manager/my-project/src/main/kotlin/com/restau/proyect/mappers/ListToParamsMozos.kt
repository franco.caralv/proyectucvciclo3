package com.restau.proyect.mappers

import com.restau.proyect.data.Mozo
import com.restau.proyect.entity.ParamsStyledTable

fun List<Mozo>.toParamsStyleMozo(
    onClickIcon: (indexfila: Int, data: Any) -> Unit = { index, data ->

    }
): ParamsStyledTable {
    val data = this.map {
        listOf<Any>(it.id ?: "", it.nombre ?: "", it.hora_entrada ?: "", it.hora_salida ?: "",it.estado?:"",it.totalpuntaje?:"")
    }

    val columnHeaders = listOf("ID", "NOMBRE", "H.ENTRADA", "H.SALIDA","ESTADO","T.PUNTAJE")

    val columnSortFunctions = listOf<(List<Any>) -> Int>(
        { row -> (row[0] as Int?) ?: 0 },
        { row -> (row[1] as String).singleOrNull()?.toInt() ?: 0 },
        { row -> (row[2] as String).firstOrNull()?.toInt() ?: 0 }, { row -> (row[3] as String).firstOrNull()?.toInt() ?: 0 },
        { row -> (row[4] as String).firstOrNull()?.toInt() ?: 0 },
        { row -> (row[5] as String).firstOrNull()?.toInt() ?: 0 },
    )
    return ParamsStyledTable(data, columnHeaders, columnSortFunctions) { indexfila, data ->
        onClickIcon(indexfila, data)
    }
}