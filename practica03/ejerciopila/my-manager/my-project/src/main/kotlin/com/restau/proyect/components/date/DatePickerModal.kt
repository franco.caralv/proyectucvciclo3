package com.restau.proyect.components.date

import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.DateRange
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.awt.SwingPanel
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.raven.datechooser.DateChooser
import com.raven.datechooser.SelectedAction
import java.time.LocalDate

import javax.swing.BoxLayout
import javax.swing.JPanel


@Composable
fun DatePickerModal(
    modifier: Modifier = Modifier,
    onclick: (datos: LocalDate) -> Unit,
    onEvent: ((it: SelectedAction, date: LocalDate) -> Unit)? = null
) {
    val jPanel = JPanel().apply {
        layout = BoxLayout(this, BoxLayout.Y_AXIS)
    }
    IconButton({
        DateChooser().apply {
            addEventDateChooser { date1, date2 ->
                val date = LocalDate.of(date2.year, date2.month, date2.day)
                onEvent?.invoke(date1, date)
                if (SelectedAction.DAY_SELECTED == date1.action) {
                    onclick(date)
                }
            }
        }.showPopup(jPanel, jPanel.x, jPanel.y)
    }, modifier = modifier) {
        Icon(Icons.Outlined.DateRange, "")
    }
    SwingPanel(
        background = Color.White,
        modifier = Modifier.size(1.dp),
        factory = {
            jPanel
        }
    )

}
