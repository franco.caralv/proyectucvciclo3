package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Pedido")
data class Pedido(
    @SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null,
    @SerializedName("precio")
    var precio: Double? = null,
    @SerializedName("nombre")
    var nombre: String? = null,
    @SerializedName("descripcion")
    var descripcion: String? = null,
    @field:SerializedName("listdetalle")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idcliente")
    val listdetalle: List<DetalleMesa>? = null,
    @field:SerializedName("listmodificador")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idpedido")
    val listmodificador: List<DetalleModificador>? = null,
    @field:SerializedName("listplatos")
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "idpedido")
    val listplatos: List<DetallePlatos>? = null,
) : Model() {

    override fun toString(): String {
        return toStringPrimitivo()
    }

    fun obtenerTiempoEstimado(): Int? {
        return listplatos?.mapNotNull { it.idplato?.tiempo_estimado }?.sum()
    }

    fun obtenerprecio(): Double? {
        val precioplatos = listplatos?.mapNotNull { it.idplato?.precio }?.sum()?:0.0
        val preciomodificador = listmodificador?.mapNotNull { it.idModificador?.precio }?.sum()?:0.0
        return precioplatos +preciomodificador
    }



}
