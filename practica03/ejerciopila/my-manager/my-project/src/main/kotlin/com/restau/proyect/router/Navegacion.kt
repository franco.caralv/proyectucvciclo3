package com.restau.proyect.router

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.FrameWindowScope
import com.restau.proyect.components.ScaffoldSearch
import com.restau.proyect.views.*
import kotlinx.coroutines.Dispatchers

@Composable
fun FrameWindowScope.Navegacion() {
    val controlador = remember { Controller.state }
    val cambio = remember { Controller.cambio }
    val collectAsState by controlador.collectAsState(Dispatchers.IO)
    val cambiostate = cambio.collectAsState(true, Dispatchers.IO)
    var flujo by remember { mutableStateOf<Flujo>(collectAsState.flujo) }
    var router by remember { mutableStateOf<Router>(collectAsState) }

    LaunchedEffect(collectAsState) {
        flujo = collectAsState.flujo
        router = collectAsState

    }

    if (flujo == Flujo.Login) {
        AnimatedVisibility(
            visible = cambiostate.value,
            enter = slideInVertically(),
            exit = slideOutVertically(),
            modifier = Modifier.fillMaxSize()
        ) {
            when (router.castToModuleLogin()) {
                is Router.Login -> {
                    LoginView()
                }

                is Router.Registro -> {
                    CreateAcount()
                }
            }
        }
    } else if (flujo == Flujo.Parque) {
        ScaffoldSearch {
            AnimatedVisibility(
                visible = cambiostate.value,
                enter = slideInVertically(),
                exit = slideOutVertically(),
                modifier = Modifier.fillMaxSize()
            ) {
                when (router.castToModuloSingle()) {
                    is Router.Parque -> {
                        Parque()
                    }

                    is Router.CrearCuenta -> {
                        CreateUserMozo()
                    }

                    is Router.CrearPlato -> {
                        CreatePlato()
                    }

                    is Router.Dasboard -> {
                        Dasboard()
                    }
                    is Router.ModificarPerson->{

                    }

                    else -> {

                    }
                }
            }
        }
    }

}

