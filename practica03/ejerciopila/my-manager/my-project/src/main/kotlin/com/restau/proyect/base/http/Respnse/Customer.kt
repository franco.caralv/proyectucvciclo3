package com.restau.proyect.base.http.Respnse

sealed class Customer<out T> {
    data class onSuccess<out T>(val info: T) : Customer<T>()
    data class onError<out T>(val error: Error) : Customer<T>()

}

data class Error(val code: Int? = null, val mensaje: String? = null)
