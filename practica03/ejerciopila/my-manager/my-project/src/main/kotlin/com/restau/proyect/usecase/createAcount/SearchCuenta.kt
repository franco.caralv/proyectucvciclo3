package com.restau.proyect.usecase.createAcount

import com.restau.proyect.data.Person
import com.restau.proyect.dominan.remote.PersonaDB
import com.restau.proyect.util.Utility
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class SearchCuenta(
    @Autowired private val listPersonPreferent: PersonaDB,
    @Autowired private val utility: Utility
) {

    operator fun invoke(person: Person): Pair<Boolean, Person?> {

        val _person = listPersonPreferent.listperson.find {
            utility.comparatehas(person.password ?: "", it.password ?: "") && person.correo == it.correo
        }
        println(_person)
        return Pair(_person != null, _person)
    }


}