package com.restau.proyect.usecase.parque

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.dominan.remote.PedidoDB
import com.restau.proyect.usecase.parque.events.CountPedidoEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class PedidoCountUseCase(
    @Autowired private val pedidoDB: PedidoDB
) {

    suspend operator fun invoke(): CountPedidoEvent {
        val resultado = pedidoDB.countPedido()
        return when (resultado) {
            is Customer.onSuccess -> {
                CountPedidoEvent.SuccessCount(resultado.info)
            }

            is Customer.onError -> {
                CountPedidoEvent.ErrorDB
            }
        }
    }


}