package com.restau.proyect.events.parque

import com.restau.proyect.data.*

sealed class ParqueEvent {

    class MovientoLista(val list: List<Mesa>) : ParqueEvent()

    class SelecMesa(val mesa: Mesa?) : ParqueEvent()

    class SelectUser(val usuario: Cliente) : ParqueEvent()

    class CountDni(val dni: String) : ParqueEvent()

    object ChangeDni : ParqueEvent()

    class SearchMozo(val nombre: String) : ParqueEvent()

    object SaveClient : ParqueEvent()

    class ChangePedido(val isactivo:Boolean) : ParqueEvent()

    class SelectMozo(val mozo: Mozo) : ParqueEvent()

    class DetallePedido(val pedido: Pedido?): ParqueEvent()

    class AddModificador(val detalleModificar:Modificador): ParqueEvent()

    class AddPlatos(val detalleplatos:Plato): ParqueEvent()
}