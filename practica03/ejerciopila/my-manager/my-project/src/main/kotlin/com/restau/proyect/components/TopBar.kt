package com.restau.proyect.components

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.text.font.FontFamily
import com.restau.proyect.theme.POPPINS
import com.restau.proyect.views.dialogs.DialogAddMesa
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Composable
fun TopBar(
    scope: CoroutineScope,
    scaffoldState: ScaffoldState,
    title: String
) {
    var visible by remember { mutableStateOf(false) }
    TopAppBar(
        title = { Text(title,
            fontFamily = FontFamily.POPPINS(),) },
        navigationIcon = {
            IconButton(onClick = {
                scope.launch {
                    scaffoldState.drawerState.open()
                }
            }) {
                Icon(imageVector = Icons.Filled.Menu, contentDescription = "Menu Icon")
            }
        },
        actions = {

            IconButton(onClick = {
                visible = true
            }) {
                Icon(imageVector = Icons.Filled.AddCircle, contentDescription = "Search Icon")
            }
        }
    )
    if (visible) {
        DialogAddMesa(visible, {
            visible = !visible
        })
    }

}

