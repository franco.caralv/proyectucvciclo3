package com.restau.proyect.util


object Constans {

    // logo
    const val LOGOSINFONDO = "drawables/screen-remove.png"
    const val LOGOSINFONDODESICRIPCION = "drawables/screen-remove.png"

    // Icono de restarurant
    const val ICON_RESTAURAN = "drawables/food-and-restaurant.png"
    const val ICON_RESTAURANDESCRIPCION = ""

    // es el logo sin titulo
    const val LOGOSINFONDOYTITULO = "drawables/logosinfondo.png"
    const val LOGOSINFONDOYTITULODESCRIPCION = "drawables/logosinfondo.png"

    // respuesta general
    const val DESCRIPCION = "Es una imagen sin descripcion"

    //tiempo de espera en scrren
    const val TIMEDELAY = 5000L

    const val TABLERESTAURANT = "drawables/iconos/table_restaurant.png"

}