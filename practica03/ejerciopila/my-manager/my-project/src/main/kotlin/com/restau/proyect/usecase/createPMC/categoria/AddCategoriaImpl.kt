package com.restau.proyect.usecase.createPMC.categoria

import com.restau.proyect.base.UseCaseBase
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Categoria
import com.restau.proyect.dominan.remote.CategoriaDB
import com.restau.proyect.dominan.remote.RestaurantDB
import com.restau.proyect.usecase.createPMC.event.ResponseCategoria
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class AddCategoriaImpl(
    @Autowired private val liscategoriaPreferent: CategoriaDB,
    @Autowired private val listPreferentRestaurante: RestaurantDB
) : UseCaseBase<Categoria, ResponseCategoria> {



    override suspend fun invoke(data: Categoria): ResponseCategoria {
        return when (val it= liscategoriaPreferent.add(data)) {
            is Customer.onSuccess -> {
                ResponseCategoria.Sucess()
            }

            is Customer.onError -> {
                ResponseCategoria.Error
            }
        }
    }
}