package com.restau.proyect.dominan.remote

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.data.Pedido
import io.ebean.Database
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct

@Repository
@Lazy
class PedidoDB(
    @Autowired val db: Database
) {




    @PostConstruct
    fun init() {


    }


    suspend fun countPedido(): Customer<Int> = withContext(Dispatchers.IO) {
        return@withContext try {
            val idpedido = db.find(Pedido::class.java).findList().size
            Customer.onSuccess(idpedido)
        } catch (e: Exception) {
            Customer.onError(Error(500, e.message))
        }
    }





}