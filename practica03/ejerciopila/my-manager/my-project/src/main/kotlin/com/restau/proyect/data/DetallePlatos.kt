package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "DetallePlatos")
data class DetallePlatos(
    @field:SerializedName("id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    val id: Int? = null,
    @field:SerializedName("idpedido")
    @ManyToOne
    @JoinColumn(name = "id_pedido", referencedColumnName = "id")
    var idpedido: Pedido? = null,
    @field:SerializedName("idplato")
    @ManyToOne
    @JoinColumn(name = "id_platos", referencedColumnName = "id")
    var idplato: Plato? = null,
) : Model() {
    override fun toString(): String {
        return toStringPrimitivo()
    }
}