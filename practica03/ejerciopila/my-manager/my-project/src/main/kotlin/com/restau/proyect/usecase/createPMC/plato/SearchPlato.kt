package com.restau.proyect.usecase.createPMC.plato

import com.restau.proyect.data.Plato
import com.restau.proyect.dominan.remote.PlatosDB
import com.restau.proyect.usecase.createPMC.base.SearchPlato
import kotlinx.coroutines.flow.Flow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
 class SearchPlatoImpl(
    @Autowired private val listPreferentPlato: PlatosDB
 ):SearchPlato {


    override fun invoke(): Flow<List<Plato>> {
      return  listPreferentPlato.listCategoria
    }

}
