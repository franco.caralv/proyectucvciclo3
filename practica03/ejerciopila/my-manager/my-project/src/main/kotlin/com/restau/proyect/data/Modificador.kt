package com.restau.proyect.data

import com.google.gson.annotations.SerializedName
import com.restau.proyect.data.extenciones.toStringPrimitivo
import io.ebean.Model
import javax.persistence.*

@Entity
@Table(name = "Modificador")
data class Modificador(
    @field:SerializedName("id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null,
    @field:SerializedName("precio")
    var precio: Double? = null,
    @field:SerializedName("nombre")
    var nombre: String? = null,
    @SerializedName("tiempo_estimado")
    var tiempo_estimado: Int? = null,
    @field:SerializedName("descripcion")
    var descripcion: String? = null,
    @ManyToOne
    @field:SerializedName("idRestaurant")
    @JoinColumn(name = "id_restaurant", referencedColumnName = "id")
    var idRestaurant: Restaurant? = null,
    @field:SerializedName("listmodificador")
    @OneToMany( mappedBy = "idModificador")
    val listmodificador: List<DetalleModificador>? = null,
): Model(){
    override fun toString(): String {
        return toStringPrimitivo()
    }
}