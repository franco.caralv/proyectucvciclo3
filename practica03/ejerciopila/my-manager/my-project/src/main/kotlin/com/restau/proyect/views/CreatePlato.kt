package com.restau.proyect.views

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.restau.proyect.IOD.viewmodels
import com.restau.proyect.components.AutoCompleteTextField
import com.restau.proyect.components.ParamsData
import com.restau.proyect.components.h.H3
import com.restau.proyect.components.h.H4
import com.restau.proyect.components.p.P
import com.restau.proyect.components.table.StyledTable
import com.restau.proyect.data.Categoria
import com.restau.proyect.events.createPlato.CreatePlatoEventos
import com.restau.proyect.theme.POPPINS
import com.restau.proyect.theme.Tumbleweed
import com.restau.proyect.theme.greend
import com.restau.proyect.viewmodel.CrearPlatoModel
import com.restau.proyect.views.dialogs.DialogoCreateCategoria

@Composable
@Preview
fun CreatePlato() {

    val crearPlatoModel: CrearPlatoModel by viewmodels()
    val modificador by crearPlatoModel.modificador.collectAsState()
    val plato by crearPlatoModel.plato.collectAsState()
    val tablamodificador by crearPlatoModel.tablemodificador.collectAsState()
    val tablecategoria by crearPlatoModel.tablecategoria.collectAsState()
    val tableplato by crearPlatoModel.tableplato.collectAsState()
    val listcategoria by crearPlatoModel.listCategoria.collectAsState()
    var crearCategoria by remember {
        mutableStateOf(false)
    }
    val stateVertical = rememberLazyListState(0)
    DialogoCreateCategoria(crearPlatoModel, crearCategoria, {
        crearCategoria = false
    })

    Box(modifier = Modifier.fillMaxWidth().fillMaxHeight()) {
        LazyColumn(Modifier.fillMaxHeight().fillMaxWidth().padding(end = 10.dp), state = stateVertical) {
            item {
                Card(
                    modifier = Modifier
                        .fillMaxWidth().fillMaxHeight().heightIn(100.dp, 200.dp).padding(20.dp),
                    border = BorderStroke(
                        5.dp,
                        brush = SolidColor(greend)
                    ),
                    shape = RoundedCornerShape(15.dp),
                    elevation = 25.dp,
                ) {
                    Text(
                        buildAnnotatedString {
                            append(
                                "Este apartado podras crear tus "
                            )
                            withStyle(SpanStyle(Tumbleweed)) {
                                append(" PLATOS ")
                            }
                            append("agregarle una categoria en caso necesario  y crear los ")
                            withStyle(style = SpanStyle(Color.Magenta)) {
                                append("MODIFICADORES ")
                            }
                            append(
                                "que son aquellos que modifican el pedido : Ejemplo (Carne, Salsas , etc  )"
                            )
                        },
                        modifier = Modifier.padding(5.dp),
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp,
                        fontFamily = FontFamily.POPPINS(),
                        fontWeight = FontWeight.Bold
                    )

                }
            }
            item {
                Row(modifier = Modifier.fillMaxWidth().fillMaxHeight()) {
                    Card(
                        shape = RoundedCornerShape(15.dp),
                        elevation = 25.dp,
                        modifier = Modifier.padding(15.dp).weight(1f).fillMaxHeight()
                    ) {
                        Column(
                            modifier = Modifier.padding(10.dp)
                                .fillMaxWidth()
                                .fillMaxHeight()
                        ) {
                            Text(
                                "AGREGA TUS PLATOS",
                                fontSize = 25.sp,
                                color = Tumbleweed,
                                fontFamily = FontFamily.POPPINS(),
                                fontWeight = FontWeight.Bold
                            )
                            OutlinedTextField(plato.nombre ?: "", {
                                crearPlatoModel.emit(CreatePlatoEventos.OyenteDeTexPlato(plato.copy(nombre = it)))
                            }, label = {
                                P("Nombre")
                            }, modifier = Modifier.fillMaxWidth(),
                                singleLine = true
                            )
                            OutlinedTextField((plato.precio ?: 0.0).toString(), { newValue ->
                                if (newValue.isEmpty() || newValue.all { it.isDigit() || it == '.' }) {
                                    val newPrice = newValue.toDoubleOrNull()
                                    crearPlatoModel.emit(CreatePlatoEventos.OyenteDeTexPlato(plato.copy(precio = if (newPrice != 0.0) newPrice else null)))
                                }
                            }, singleLine = true, label = {
                                P("Precio")
                            }, modifier = Modifier.fillMaxWidth()
                            )
                            OutlinedTextField(
                                (plato.tiempo_estimado ?: 0).toString(), { newValue ->
                                    if (newValue.isEmpty() || newValue.all { it.isDigit()  }) {
                                        val newPrice = newValue.toIntOrNull()
                                        crearPlatoModel.emit(
                                            CreatePlatoEventos.OyenteDeTexPlato(
                                                plato.copy(
                                                    tiempo_estimado = if (newPrice != 0) newPrice else null
                                                )
                                            )
                                        )
                                    }

                                },
                                singleLine = true, label = {
                                    P("Tiempo estimado")
                                }, placeholder = {
                                    P("Tiempo estimado va estar medido en minutos")
                                }, modifier = Modifier.fillMaxWidth()
                            )
                            OutlinedTextField(plato.descripcion ?: "", {
                                crearPlatoModel.emit(CreatePlatoEventos.OyenteDeTexPlato(plato.copy(descripcion = it)))
                            }, singleLine = true, label = {
                                P("Descripcion")
                            }, modifier = Modifier.fillMaxWidth()
                            )
                            Spacer(Modifier.fillMaxWidth().padding(top = 10.dp))
                            Divider(Modifier.fillMaxWidth().height(5.dp))
                            H4("Agrega una Categoria")
                            OutlinedButton({
                                crearCategoria = true
                            }, modifier = Modifier.fillMaxWidth()) {
                                P("Crear Categoria")
                            }
                            AutoCompleteTextField(
                                ParamsData(
                                    listcategoria,
                                    modifier = Modifier.fillMaxWidth(),
                                    label = "Busca tu Categoria",
                                    onSearch = {
                                         if (it.isBlank()){
                                             crearPlatoModel.emit(CreatePlatoEventos.OyenteDeTexPlato(plato.copy(categoria = null)))
                                         }
                                    },
                                    onItemSelected = {
                                        crearPlatoModel.emit(CreatePlatoEventos.OyenteDeTexPlato(plato.copy(categoria = it)))
                                    },
                                    atributo = Categoria::nombre
                                )
                            )
                            Button({
                                crearPlatoModel.emit(CreatePlatoEventos.GuardarPlato)
                            }, modifier = Modifier.fillMaxWidth()) {
                                P("Guardar")
                            }
                            Button({
                                crearPlatoModel.emit(CreatePlatoEventos.BorrarPlato)
                            }, modifier = Modifier.fillMaxWidth()) {
                                P("Borrar Datos")
                            }


                        }
                    }
                    Card(
                        shape = RoundedCornerShape(15.dp),
                        elevation = 25.dp,
                        modifier = Modifier.padding(15.dp).weight(1f).fillMaxHeight()
                    ) {
                        Column(
                            modifier = Modifier.padding(10.dp).fillMaxWidth()
                                .fillMaxHeight()
                        ) {
                            Text(
                                "AGREGA TUS MODIFICADORES",
                                fontSize = 25.sp,
                                color = Color.Magenta,
                                fontFamily = FontFamily.POPPINS(),
                                fontWeight = FontWeight.Bold
                            )
                            OutlinedTextField(
                                modificador.nombre ?: "", {
                                    crearPlatoModel.emit(
                                        CreatePlatoEventos.OyenteDeTexModificador(
                                            modificador.copy(
                                                nombre = it
                                            )
                                        )
                                    )
                                },
                                singleLine = true, label = {
                                    P("Nombre")
                                }, modifier = Modifier.fillMaxWidth()
                            )
                            OutlinedTextField(
                                (modificador.precio ?: 0).toString(), { newValue ->
                                    if (newValue.isEmpty() || newValue.all { it.isDigit() || it == '.' }) {
                                        val newPrice = newValue.toDoubleOrNull()
                                        crearPlatoModel.emit(
                                            CreatePlatoEventos.OyenteDeTexModificador(
                                                modificador.copy(
                                                    precio = if (newPrice != 0.0) newPrice else null
                                                )
                                            )
                                        )
                                    }

                                },
                                singleLine = true, label = {
                                    P("Precio")
                                }, modifier = Modifier.fillMaxWidth()
                            )
                            OutlinedTextField(
                                (modificador.tiempo_estimado ?: 0).toString(), { newValue ->
                                    if (newValue.isEmpty() || newValue.all { it.isDigit()  }) {
                                        val newPrice = newValue.toIntOrNull()
                                        crearPlatoModel.emit(
                                            CreatePlatoEventos.OyenteDeTexModificador(
                                                modificador.copy(
                                                    tiempo_estimado = if (newPrice != 0) newPrice else null
                                                )
                                            )
                                        )
                                    }

                                },
                                singleLine = true, label = {
                                    P("Tiempo estimado")
                                }, placeholder = {
                                    P("Tiempo estimado va estar medido en minutos")
                                }, modifier = Modifier.fillMaxWidth()
                            )
                            OutlinedTextField(
                                modificador.descripcion ?: "", {
                                    crearPlatoModel.emit(
                                        CreatePlatoEventos.OyenteDeTexModificador(
                                            modificador.copy(
                                                descripcion = it
                                            )
                                        )
                                    )
                                },
                                singleLine = true, label = {
                                    P("Descripcion")
                                }, modifier = Modifier.fillMaxWidth()
                            )
                            Button({
                                crearPlatoModel.emit(CreatePlatoEventos.GuardarModifcador)
                            }, modifier = Modifier.fillMaxWidth()) {
                                P("Guardar")
                            }
                            Button({
                                crearPlatoModel.emit(CreatePlatoEventos.BorrarModificador)
                            }, modifier = Modifier.fillMaxWidth()) {
                                P("Borrar Datos")
                            }

                        }
                    }

                }
            }
            item {
                Card(
                    shape = RoundedCornerShape(15.dp),
                    elevation = 25.dp,
                    modifier = Modifier.padding(15.dp)
                ) {
                    Column {
                        H3(
                            "Lista de platos",
                            modifier = Modifier.fillParentMaxWidth().padding(10.dp),
                            textAlign = TextAlign.Center
                        )
                        tableplato?.let {
                            StyledTable(it)
                        } ?: run {
                            Box(modifier = Modifier.fillParentMaxWidth().background(Color.LightGray).padding(20.dp)){
                                H4("Se encuentra vacio")
                            }
                        }

                    }
                }
            }
            item {
                Card(
                    shape = RoundedCornerShape(15.dp),
                    elevation = 25.dp,
                    modifier = Modifier.padding(15.dp)
                ) {
                    Column {
                        H3(
                            "Lista de Categorias",
                            modifier = Modifier.fillParentMaxWidth().padding(10.dp),
                            textAlign = TextAlign.Center
                        )
                        tablecategoria?.let {
                            StyledTable(it)
                        }?: run {
                            Box(modifier = Modifier.fillParentMaxWidth().background(Color.LightGray).padding(20.dp)){
                                H4("Se encuentra vacio")
                            }
                        }

                    }
                }
            }
            item {

                Card(
                    shape = RoundedCornerShape(15.dp),
                    elevation = 25.dp,
                    modifier = Modifier.padding(15.dp)
                ) {
                    Column {
                        H3(
                            "Lista de Modificadores",
                            modifier = Modifier.fillParentMaxWidth().padding(10.dp),
                            textAlign = TextAlign.Center
                        )

                        tablamodificador?.let {
                            StyledTable(it)
                        }?: run {
                            Box(modifier = Modifier.fillParentMaxWidth().background(Color.LightGray).padding(20.dp)){
                                H4("Se encuentra vacio")
                            }
                        }

                    }

                }
            }


        }
        VerticalScrollbar(
            modifier = Modifier.align(Alignment.CenterEnd).fillMaxHeight(),
            adapter = rememberScrollbarAdapter(stateVertical)
        )
    }


}



