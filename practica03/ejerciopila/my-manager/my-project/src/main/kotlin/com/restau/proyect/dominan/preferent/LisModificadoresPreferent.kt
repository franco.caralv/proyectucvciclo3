package com.restau.proyect.dominan.preferent

import com.google.gson.reflect.TypeToken
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.base.http.Respnse.Error
import com.restau.proyect.base.localstorage.StorageService
import com.restau.proyect.data.Modificador
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct


@Repository
@Lazy
@Primary
class LisModificadoresPreferent(
    @Autowired private val usuariopreferent: PersonLogin
) {


    @Value("\${app.secretKey}")
    private lateinit var key: String

    lateinit var listmodificadoresPreferent: StorageService<List<Modificador>>

    private val nameDocumento = "modificadores"

    private var fileocumento = "listmodificadores"

    private val _listModificador = MutableStateFlow<List<Modificador>>(listOf())

    val listModificador = _listModificador.asStateFlow()


    @PostConstruct
    fun cargandoModificadores() {
        fileocumento = "$fileocumento${usuariopreferent.person?.id ?: ""}"
        listmodificadoresPreferent = StorageService(object : TypeToken<List<Modificador>>() {}, nameDocumento, key)
        listmodificadoresPreferent.load(fileocumento)?.let {
            _listModificador.value = it
        }
    }


    fun add(modificador: Modificador): Customer<List<Modificador>> {
        return try {
            _listModificador.value = _listModificador.value.toMutableList().apply {
                add(modificador.apply {
                    id = if (_listModificador.value.isEmpty()) 1 else _listModificador.value.last().id
                        ?.let { (it + 1) } ?: 1
                })
            }
            listmodificadoresPreferent.save(_listModificador.value, fileocumento)
            return Customer.onSuccess(_listModificador.value)
        } catch (e: Exception) {
            Customer.onError(Error(500, "No se logro guardar"))
        }

    }


    fun delete(modificador: Modificador): Customer<List<Modificador>> {

        return try {
            _listModificador.value = _listModificador.value.toMutableList().filter {
                it != modificador
            }.toList()
            listmodificadoresPreferent.save(_listModificador.value, fileocumento)
            return Customer.onSuccess(_listModificador.value)
        } catch (e: Exception) {
            Customer.onError(Error(500, "No se logro guardar"))
        }
    }


}