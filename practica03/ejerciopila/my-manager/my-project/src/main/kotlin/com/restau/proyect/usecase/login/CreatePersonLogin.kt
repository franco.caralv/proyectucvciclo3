package com.restau.proyect.usecase.login

import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Person
import com.restau.proyect.dominan.preferent.ListmesasPreferent
import com.restau.proyect.dominan.preferent.PersonLogin
import com.restau.proyect.dominan.remote.PersonaDB
import com.restau.proyect.dominan.remote.RestaurantDB
import org.jetbrains.skia.impl.Log
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service


@Service
@Lazy
class CreatePersonLogin(
    @Autowired private val personLogin: PersonLogin,
    @Autowired private val restaurante: RestaurantDB,
    @Autowired private val listmesasPreferent: ListmesasPreferent,
    @Autowired private val pesonarm: PersonaDB
) {

    suspend operator fun invoke(person: Person) {
        personLogin + person
        listmesasPreferent.cargandolistas()
        personLogin.person?.id?.let {
            val result = pesonarm.searchpersonawithRestaurante(it)
            when (result) {
                is Customer.onSuccess -> {
                    restaurante.restaurant = result.info.restaurant?.first() ?: restaurante.newsaveRestaurant(person)
                }

                is Customer.onError -> {
                    Log.debug(result.error.toString())
                    restaurante.newsaveRestaurant(person)
                }
            }
        } ?: restaurante.newsaveRestaurant(person)

    }

}