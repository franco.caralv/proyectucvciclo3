package com.restau.proyect.usecase.parque

import com.restau.proyect.data.Mozo
import com.restau.proyect.dominan.remote.MozosDB
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class SearchMozoUseCase(
    @Autowired private val mozosDB: MozosDB,
) {

    operator fun invoke(nombre: String): Flow<List<Mozo>> {
        return mozosDB.listMozos.map {
            it.filter {
                it.estado == "ACTIVO" && it.nombre?.uppercase()?.contains(nombre.uppercase()) == true
            }
        }
    }


}