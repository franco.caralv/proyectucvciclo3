package com.restau.proyect.viewmodel

import com.restau.proyect.base.ViewModelbase
import com.restau.proyect.data.Mozo
import com.restau.proyect.entity.ParamsStyledTable
import com.restau.proyect.mappers.toParamsStyleMozo
import com.restau.proyect.usecase.createUser.Mozos.AddMozos
import com.restau.proyect.usecase.createUser.Mozos.DeleteMozo
import com.restau.proyect.usecase.createUser.Mozos.SearchMozos
import com.restau.proyect.usecase.createUser.event.EventCreateUser
import kotlinx.coroutines.flow.*
import org.springframework.beans.factory.annotation.Autowired
import javax.annotation.PostConstruct

@org.springframework.context.annotation.Scope("prototype")
@org.springframework.stereotype.Controller
@org.springframework.context.annotation.Lazy
class CreateUserModel(
    @Autowired private val addMozos: AddMozos,
    @Autowired private val deleteMozo: DeleteMozo,
    @Autowired private val searchMozos: SearchMozos,
) : ViewModelbase() {

    private val _mozo = MutableStateFlow(Mozo(estado = "ACTIVO", totalpuntaje = "0"))
    val mozo = _mozo.asStateFlow()
    val tableMozo = MutableStateFlow<ParamsStyledTable?>(null)

    @PostConstruct
    fun init() {
        searchMozos.invoke().onEach {
            tableMozo.value = it.toParamsStyleMozo { indexfila, data ->
                val element = it.getOrNull(indexfila)
                element?.let { mozo ->
                    events(EventCreateUser.deleteMozo(mozo))
                }
            }
            if (it.isEmpty())
                tableMozo.value = null
        }.launchIn(lyceviewmodel)
    }


    fun events(value: EventCreateUser) = launch {
        evaluarEvents(value)
    }

    private suspend fun evaluarEvents(value: EventCreateUser) {
        when (value) {
            is EventCreateUser.listenerField -> {
                _mozo.value = value.mozo
            }

            is EventCreateUser.saveMozo -> {
                addMozos.invoke(_mozo.value)
                events(EventCreateUser.clearField)
            }

            is EventCreateUser.deleteMozo -> {
                deleteMozo.invoke(value.mozo)
                events(EventCreateUser.clearField)
            }

            is EventCreateUser.clearField -> {
                _mozo.value = Mozo(estado = "ACTIVO", totalpuntaje = "0")
            }
        }
    }


}