package com.restau.proyect.theme

import androidx.compose.ui.graphics.Color

val greend = Color(196,210,178)
val greendvariante = Color(0xff95b41c)
val azul = Color(0xff4ec4f7)
val verdeclaro = Color(0xffb9d835)
val oro = Color(0xffffd700)
val naranja = Color(0xffff9f3a)
val rosapastel = Color(0xffffb6c1)
val rojointenso = Color(0xffe60000)
val Copper_rust=Color(0xffa44c4c)
val Tumbleweed=Color(218,169,114)
val Bali_hai=Color(137,154,174)
val Tundora=Color(77,77,77)
