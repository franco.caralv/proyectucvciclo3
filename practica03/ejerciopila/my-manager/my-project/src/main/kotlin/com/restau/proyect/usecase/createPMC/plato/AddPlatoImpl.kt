package com.restau.proyect.usecase.createPMC.plato

import com.restau.proyect.base.UseCaseBase
import com.restau.proyect.base.http.Respnse.Customer
import com.restau.proyect.data.Plato
import com.restau.proyect.dominan.remote.PlatosDB
import com.restau.proyect.usecase.createPMC.event.ReponsePLato
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service

@Service
@Lazy
class AddPlatoImpl(
    @Autowired private val listPreferentPlato: PlatosDB
) : UseCaseBase<Plato, ReponsePLato> {

    override suspend fun invoke(data: Plato): ReponsePLato {
        validateFields(data)?.let{
            return ReponsePLato.ErrorCampos(it)
        }
        data.categoria?.let {
            val response = listPreferentPlato.guardarPlato(data)
            if (response is Customer.onSuccess) {
                return ReponsePLato.Sucess(response.info)
            }
            if (response is Customer.onError) {
                return ReponsePLato.ErrordeFile
            }
        } ?: run {
           return ReponsePLato.ErrorCampos("Primero Vinculalo a una categoria")
        }
        return ReponsePLato.Error
    }

    private fun validateFields(data: Plato): String? {
        if (data.descripcion.isNullOrBlank())
            return "Falta una descripcion"
        if (data.nombre.isNullOrBlank())
            return "Debe tener un nombre "
        if (data.precio==0.0 || data.precio==null)
            return "Ingresa un precio "

        return null
    }


}