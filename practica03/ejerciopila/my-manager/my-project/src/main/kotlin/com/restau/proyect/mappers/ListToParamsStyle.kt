package com.restau.proyect.mappers

import com.restau.proyect.data.Categoria
import com.restau.proyect.data.Modificador
import com.restau.proyect.data.Plato
import com.restau.proyect.entity.ParamsStyledTable

fun List<Modificador>.toParamsStyle(
    onClickIcon: (indexfila: Int, data: Any) -> Unit = { index, data ->

    }
): ParamsStyledTable {
    val data = this.map {
        listOf<Any>(it.id ?: "", it.nombre ?: "", it.descripcion ?: "", it.precio ?: 0)
    }

    val columnHeaders = listOf("ID", "NOMBRE", "DESCRIPCION", "PRECIO")

    val columnSortFunctions = listOf<(List<Any>) -> Int>(
        { row -> (row[0] as Int?) ?: 0 },
        { row -> (row[1] as String).singleOrNull()?.toInt() ?: 0 },
        { row -> (row[2] as String).firstOrNull()?.toInt() ?: 0 }, { row -> (row[3] as Double?)?.toInt() ?: 0 }
    )
    return ParamsStyledTable(data, columnHeaders, columnSortFunctions) { indexfila, data ->
        onClickIcon(indexfila, data)
    }
}




fun List<Categoria>.toCateParamsStyle(
    onClickIcon: (indexfila: Int, data: Any) -> Unit = { index, data ->

    }
): ParamsStyledTable {
    val data = this.map {
        listOf<Any>(it.id ?: "", it.nombre ?: "", it.descripcion ?: 0)
    }

    val columnHeaders = listOf("ID", "NOMBRE", "DESCRIPCION")

    val columnSortFunctions = listOf<(List<Any>) -> Int>(
        { row -> (row[0] as Int?) ?: 0 },
        { row -> (row[1] as String).singleOrNull()?.toInt() ?: 0 },
        { row -> (row[2] as String).singleOrNull()?.toInt() ?: 0 },
    )
    return ParamsStyledTable(data, columnHeaders, columnSortFunctions) { indexfila, data ->
        onClickIcon(indexfila, data)
    }
}





fun List<Plato>.toPlatoParamsStyle(
    onClickIcon: (indexfila: Int, data: Any) -> Unit = { index, data ->

    }
): ParamsStyledTable {
    val data = this.map {
        listOf<Any>(it.id ?: "", it.nombre ?: "", it.precio ?: 0,it.categoria?.nombre?:"")
    }

    val columnHeaders = listOf("ID", "NOMBRE", "PRECIO","CATEGORIA")

    val columnSortFunctions = listOf<(List<Any>) -> Int>(
        { row -> (row[0] as Int?) ?: 0 },
        { row -> (row[1] as String).singleOrNull()?.toInt() ?: 0 },
        { row -> (row[2] as Double?)?.toInt() ?: 0 },
        { row -> (row[3] as String).singleOrNull()?.toInt() ?: 0 },
    )
    return ParamsStyledTable(data, columnHeaders, columnSortFunctions) { indexfila, data ->
        onClickIcon(indexfila, data)
    }
}