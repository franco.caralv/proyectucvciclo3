package com.restau.proyect.usecase.createUser.event

sealed class EventDeleteMozo {
    object SuccessDeleteMozo:EventDeleteMozo()
    object FaildMozo:EventDeleteMozo()
}