package com.restau.proyect.usecase.screen

import com.restau.proyect.dominan.preferent.PersonLogin
import com.restau.proyect.router.Controller
import com.restau.proyect.router.Router
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class ValidadSreenRouter(
    @Autowired private val personLogin: PersonLogin
) {


    suspend operator fun invoke(): Router {
        if (personLogin.checkperson()) {
            Controller.clear(Router.Parque)
            return Router.Parque
        }
        Controller.clear(Router.Login)
        return Router.Login
    }


}