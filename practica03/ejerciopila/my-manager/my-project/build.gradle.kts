
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.10"
    kotlin("kapt") version "1.7.10"
    id("org.jetbrains.compose")
    id("dev.hydraulic.conveyor") version "1.5"
    id("io.ebean") version "13.6.5"
}

group = "com.example"
version = "4.0"



repositories {
    mavenCentral()
    google()
    mavenCentral()
    maven { url = uri("https://jitpack.io") }
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}
dependencies {
    // Use the configurations created by the Conveyor plugin to tell Gradle/Conveyor where to find the artifacts for each platform.
    linuxAmd64(compose.desktop.linux_x64)
    macAmd64(compose.desktop.macos_x64)
    macAarch64(compose.desktop.macos_arm64)
    windowsAmd64(compose.desktop.windows_x64)
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.7.10")
    //iconos
    implementation("org.jetbrains.compose.material:material-icons-extended-desktop:1.1.1")
    implementation("org.apache.tomcat.embed:tomcat-embed-core:9.0.56")
    //sping framework
    implementation("org.springframework:spring-context:5.3.14")
    implementation("org.springframework:spring-webmvc:5.3.14")
    implementation("javax.annotation:javax.annotation-api:1.3.2")
    // http
    implementation("com.google.code.gson:gson:2.9.0")

    //test
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.0")
    testImplementation("org.mockito:mockito-core:5.2.0")
    testImplementation("org.mockito.kotlin:mockito-kotlin:4.0.0")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.0")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.8.0")

    // reordenable
    implementation("org.burnoutcrew.composereorderable:reorderable:0.9.6")

    //hash
    implementation("org.mindrot:jbcrypt:0.4")

    //agregando librerias externas
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    //sqllite
    implementation("org.xerial:sqlite-jdbc:3.34.0")

    // ebean
    implementation("io.ebean:ebean:13.6.5") // Asegúrate de usar la versión más reciente
    implementation("io.ebean:ebean-ddl-generator:13.6.5")
    kapt("io.ebean:kotlin-querybean-generator:13.6.5")
    testImplementation("io.ebean:ebean-test:13.6.5")
    //log
    implementation("org.slf4j:slf4j-api:1.7.32") // Revisa si es la última versión
    implementation("org.slf4j:slf4j-simple:1.7.32")
}


compose.desktop {
    application {
        mainClass = "com.restau.proyect.MainKt"

        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageVersion = "1.0.0"
            licenseFile.set(project.file("LICENSE.txt"))
            description = "Compose Example App"
            appResourcesRootDir.set(project.layout.projectDirectory.dir("resources"))
        }
        buildTypes.release.proguard {
            obfuscate.set(true)
        }
    }

}

// region Work around temporary Compose bugs.
configurations.all {
    attributes {
        // https://github.com/JetBrains/compose-jb/issues/1404#issuecomment-1146894731
        attribute(Attribute.of("ui", String::class.java), "awt")
    }
}


dependencies {
    // Force override the Kotlin stdlib version used by Compose to 1.7 in the machine specific configurations, as otherwise we can end up
    // with a mix of 1.6 and 1.7 on our classpath. This is the same logic as is applied to the regular Compose configurations normally.

    val v = "1.7.10"

    for (m in setOf("linuxAmd64", "macAmd64", "macAarch64", "windowsAmd64")) {
        m("org.jetbrains.kotlin:kotlin-stdlib:$v")
        m("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$v")
        m("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$v")
    }


}
// endregion


