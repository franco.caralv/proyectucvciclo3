package vehiculos;
public class Sedan extends  Vehiculo{

    private  double longitud;

    public Sedan(double velocidad, double precioFabricacion, String color, double precioVenta) {
        super(velocidad, precioFabricacion, color, precioVenta);
    }

    public Sedan(double velocidad, double precioFabricacion, String color, double precioVenta, double longitud) {
        super(velocidad, precioFabricacion, color, precioVenta);
        this.longitud = longitud;
    }

    public  double getPrecioVenta(){
        if(longitud>20){
            return   this.PrecioVenta- this.PrecioVenta*0.05;
        }
        return   this.PrecioVenta- this.PrecioVenta*0.1;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Sedan{" +
                "longitud=" + longitud +
                ", velocidad=" + velocidad +
                ", PrecioFabricacion=" + PrecioFabricacion +
                ", Color='" + Color + '\'' +
                ", PrecioVenta=" + PrecioVenta +
                '}';
    }
}