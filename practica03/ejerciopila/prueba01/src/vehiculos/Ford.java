package vehiculos;

public class Ford extends Vehiculo{

    private int anio;

    private double DescuentoFabricacion;


    public Ford(double velocidad, double precioFabricacion, String color, double precioVenta, int anio, double descuentoFabricacion) {
        super(velocidad, precioFabricacion, color, precioVenta);
        this.anio = anio;
        DescuentoFabricacion = descuentoFabricacion;
    }

    public  double getPrecioVenta(){
        return this.PrecioVenta- this.DescuentoFabricacion;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public double getDescuentoFabricacion() {
        return DescuentoFabricacion;
    }

    public void setDescuentoFabricacion(double descuentoFabricacion) {
        DescuentoFabricacion = descuentoFabricacion;
    }

    @Override
    public String toString() {
        return "Ford{" +
                "anio=" + anio +
                ", DescuentoFabricacion=" + DescuentoFabricacion +
                ", velocidad=" + velocidad +
                ", PrecioFabricacion=" + PrecioFabricacion +
                ", Color='" + Color + '\'' +
                ", PrecioVenta=" + PrecioVenta +
                '}';
    }
}

