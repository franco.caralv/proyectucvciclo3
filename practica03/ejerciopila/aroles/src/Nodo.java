public class Nodo<T> {

    private Nodo left;
    private T value;
    private Nodo right;
    private  Nodo parent;

    public Nodo(Nodo left, T value, Nodo right, Nodo parent) {
        this.left = left;
        this.value = value;
        this.right = right;
        this.parent = parent;
    }

    public Nodo(Nodo left) {
        this.left = left;
    }

    public Nodo(T value) {
        this.value = value;
    }

    public Nodo getLeft() {
        return left;
    }

    public void setLeft(Nodo left) {
        this.left = left;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Nodo getRight() {
        return right;
    }

    public void setRight(Nodo right) {
        this.right = right;
    }

    public Nodo getParent() {
        return parent;
    }

    public void setParent(Nodo parent) {
        this.parent = parent;
    }
}
