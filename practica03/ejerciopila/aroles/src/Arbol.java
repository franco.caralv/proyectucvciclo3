public class Arbol<T extends Comparable<T>> {


    private static final short ONE_NODE_LEFT = 1;
    private static final short ONE_NODE_RIGHT = 2;
    private static final short TWO_NODES = 3;

    private Nodo<T> raiz;

    public boolean isEmty() {
        return raiz == null;
    }

    public Nodo<T> getRaiz() {
        return raiz;

    }

    public boolean isRaiz(Nodo<T> t) {
        return raiz == t;
    }

    public boolean isLeaf(Nodo<T> t) {
        return t.getLeft() == null && t.getRight() == null;
    }

    public boolean isInternal(Nodo<T> t) {
        return !isLeaf(t);
    }

    public Nodo add(Nodo<T> origen, T elemento) {
        Nodo nodo = new Nodo(elemento);
        if (isEmty()) {
            raiz = new Nodo<>(elemento);
        } else if (origen == null) {
            System.out.println("El origen es nulo");
        } else {
            if (origen.getValue().compareTo(elemento) > 0) {
                if (origen.getLeft() != null) {
                    add(origen.getLeft(), elemento);
                } else {

                    nodo.setParent(origen);
                    nodo.setLeft(nodo);
                }
            } else {
                if (origen.getRight() != null) {
                    add(origen.getRight(), elemento);
                } else {

                    nodo.setParent(origen);
                    nodo.setRight(nodo);
                }
            }
        }
        return nodo;
    }

    public Nodo<T> add(T elemento) {

        Nodo<T> nodo = null;
        //Si el root es nulo, lo añade el primero
        if (raiz == null) {
            nodo = new Nodo<>(elemento);
            raiz = nodo;
        } else {

            //Creo un nodo auxuliar
            Nodo<T> aux = raiz;
            boolean insertado = false;
            //No salgo hasta que este insertado
            while (!insertado) {

                //Comparamos los elementos
                //Si el nodo del origen es mayor que el elemento pasado, pasa a la izquierda
                if (aux.getValue().compareTo(elemento) > 0) {

                    //Si tiene nodo izquierdo, actualizo el aux
                    if (aux.getLeft() != null) {
                        aux = aux.getLeft();
                    } else {
                        //Creo el nodo
                        nodo = new Nodo<>(elemento);
                        //Indico que el padre del nodo creado
                        nodo.setParent(aux);
                        aux.setLeft(nodo);
                        //indico que lo he insertado
                        insertado = true;
                    }

                } else {

                    if (aux.getRight() != null) {
                        aux = aux.getRight();
                    } else {
                        //Creo el nodo
                        nodo = new Nodo<>(elemento);
                        //Indico que el padre del nodo creado
                        nodo.setParent(aux);
                        aux.setRight(nodo);
                        //indico que lo he insertado
                        insertado = true;
                    }

                }

            }

        }

        return nodo;

    }

    public void preorder(Nodo<T> nodo) {
        System.out.println(String.valueOf(nodo.getValue()));
        if (nodo.getLeft() != null) {
            preorder(nodo.getLeft());
        }
        if (nodo.getRight() != null) {
            preorder(nodo.getRight());
        }
    }


    public void inorder(Nodo<T> nodo) {

        if (nodo.getLeft() != null) {
            inorder(nodo.getLeft());
        }
        System.out.println(String.valueOf(nodo.getValue()));
        if (nodo.getRight() != null) {
            inorder(nodo.getRight());
        }
    }

    public void postorder(Nodo<T> nodo) {
        if (nodo.getLeft() != null) {
            postorder(nodo.getLeft());
        }

        if (nodo.getRight() != null) {
            postorder(nodo.getRight());
        }
        System.out.println(String.valueOf(nodo.getValue()));
    }


    public int height(Nodo<T> nodo) {
        int height = 0;
        if (isInternal(nodo)) {
            if (nodo.getLeft() != null) {
                height = Math.max(height, height(nodo.getLeft()));
            }
            if (nodo.getRight() != null) {
                height = Math.max(height, height(nodo.getRight()));
            }
            height++;
        }
        return height;
    }

    public int depth(Nodo<T> nodo) {
        int depth = 0;
        if (nodo != getRaiz()) {
            depth = 1 + depth(nodo.getParent());
        }
        return depth;
    }





    public void remove(Nodo<T> nodo) {

        if (raiz == null) {
            System.out.println("No hay nodos que borrar");
        } else if (isLeaf(nodo)) {
            removeLeaf(nodo);
        } else if (nodo.getRight() != null && nodo.getLeft() == null) {
            removeWithChlid(nodo, ONE_NODE_RIGHT);
        } else if (nodo.getRight() == null && nodo.getLeft() != null) {
            removeWithChlid(nodo, ONE_NODE_LEFT);
        } else {
            removeWithChlid(nodo, TWO_NODES);
        }
    }

    private void removeLeaf(Nodo<T> nodo) {

        if (isRaiz(nodo)) {
            raiz = null;
        } else {

            Nodo<T> parent = nodo.getParent();

            if (parent.getLeft() == nodo) {
                parent.setLeft(null);
            }

            if (parent.getRight() == nodo) {
                parent.setRight(null);
            }

            nodo = null;

        }

    }


    private void removeWithChlid(Nodo<T> nodo, short type_node) {

        Nodo<T> siguiente = null;

        switch (type_node) {
            case ONE_NODE_LEFT:
                siguiente = nodo.getLeft();
                break;
            case ONE_NODE_RIGHT:
                siguiente = minSubTree(nodo.getRight());
                break;
            case TWO_NODES:

                siguiente = minSubTree(nodo.getRight());

                if (!isRaiz(siguiente.getParent())) {

                    nodo.getLeft().setParent(siguiente);
                    nodo.getRight().setParent(siguiente);

                    if (siguiente.getParent().getLeft() == siguiente) {
                        siguiente.getParent().setLeft(null);
                    } else if (siguiente.getParent().getRight() == siguiente) {
                        siguiente.getParent().setRight(null);
                    }

                }

                break;
        }

        siguiente.setParent(nodo.getParent());

        if (!isRaiz(nodo)) {

            if (nodo.getParent().getLeft() == nodo) {
                nodo.getParent().setLeft(siguiente);
            } else if (nodo.getParent().getRight() == nodo) {
                nodo.getParent().setRight(siguiente);
            }

        } else {
            raiz = siguiente;
        }

        if (nodo.getRight() != null && nodo.getRight() != siguiente) {
            siguiente.setRight(nodo.getRight());
        }

        if (nodo.getLeft() != null && nodo.getLeft() != siguiente) {
            siguiente.setLeft(nodo.getLeft());
        }

        nodo = null;

    }
    private Nodo<T> minSubTree(Nodo<T> nodo) {

        if (nodo != null && nodo.getLeft() != null) {
            while (!isLeaf(nodo)) {
                nodo = minSubTree(nodo.getLeft());
            }

        }

        return nodo;
    }


}
