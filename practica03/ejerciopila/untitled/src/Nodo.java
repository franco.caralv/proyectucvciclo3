public class Nodo {

    private Nodo siguiente;
    private String aux;

    public Nodo(String valor) {
        this.siguiente = null;
        this.aux = valor;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    public String getAux() {
        return aux;
    }

    public void setAux(String aux) {
        this.aux = aux;
    }

}
