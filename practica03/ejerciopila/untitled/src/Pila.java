public class Pila {

    private Nodo inicio;
    private int size;

    public Pila() {
        this.inicio = null;
        this.size = 0;
    }

    public void push(String elemento) {
        Nodo aux = inicio;
        if (inicio == null) {
            inicio = new Nodo(elemento);
            size++;
            return;
        }
        while (aux.getSiguiente() != null) {
            aux = aux.getSiguiente();
        }
        aux.setSiguiente(new Nodo(elemento));
        size++;
    }

    public String pop() {
        if (size == 0) {
            return "0";
        }
        if (size == 1) {
            String valor = inicio.getAux();
            inicio = null;
            size--;
            return valor;
        }
        Nodo aux = inicio;
        for (int i = 1; i < size - 1; i++) {
            aux = aux.getSiguiente();
        }
        String valor = aux.getSiguiente().getAux() + "";
        aux.setSiguiente(null);
        size--;
        return valor;
    }

    public void runpila() {
        Nodo aux = inicio;
        while (aux != null) {
            System.out.println("el elemeneto es: " + aux.getAux());
            aux = aux.getSiguiente();
        }
    }

}
