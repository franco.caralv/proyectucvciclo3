import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String entrada;
        Pila p = new Pila();
        System.out.println("Ingrese su valor prefijos");
        entrada = sc.next();
        String operadores = "+-*/%";
        for (int i = entrada.length() - 1; i >= 0; i--) {
            char letra = entrada.charAt(i);
            if (operadores.contains(letra + "")) {
                String num1 = p.pop();
                String num2 = p.pop();
                String valoe = evaluar(num1, num2, letra + "") + "";
                p.push(valoe);
            } else {
                p.push(letra + "");
            }
        }
        p.runpila();
    }


    public static int evaluar(String n1, String n2, String op) {
        int num1 = Integer.parseInt(n1);
        int num2 = Integer.parseInt(n2);
        if (op.contains("+")) {
            return num1 + num2;
        }
        if (op.contains("-")) {
            return num1 - num2;
        }
        if (op.contains("*")) {
            return num1 * num2;
        }
        if (op.contains("/") && num2 != 0) {
            return num1 / num2;
        }
        if (op.contains("%")) {
            return num1 % num2;
        }
        return 0;
    }
}