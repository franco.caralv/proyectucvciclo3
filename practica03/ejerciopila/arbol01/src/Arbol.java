public class Arbol {

   private Nodo raiz;
   private int tamaño=0;

   public Arbol(){
       raiz=null;
   }

   public boolean isvacio(){
       return  raiz==null;
   }

   public void insert(int i,Object fruta){
        Nodo n=new Nodo(i);
        n.setFruta(fruta);
        if(isvacio()){
            raiz=n;
        }else{
            Nodo aux=raiz;
            while (aux!=null){
                n.setPadre(aux);
                if(n.getValor()>=aux.getValor()){
                    aux=aux.getDerecha();
                }else {
                    aux= aux.getIzquierda();
                }
            }
            if(n.getValor()<n.getPadre().getValor()){
                n.getPadre().setIzquierda(n);
            }else{
                n.getPadre().setDerecha(n);
            }
        }
       tamaño++;
   }

   public void runar(Nodo n){
       if(n!=null){
           runar(n.getIzquierda());
           System.out.println("indece "+n.getValor()+" valor :"+ n.getFruta());
           runar(n.getDerecha());
       }
   }

    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    public int getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }
}
