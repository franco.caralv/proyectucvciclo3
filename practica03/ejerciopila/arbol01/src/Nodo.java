public class Nodo {

    private Nodo derecha;
    private int valor;
    private Nodo izquierda;

    private Object fruta;


    private Nodo padre;

    public Nodo(int val){
        derecha=null;
        izquierda=null;
        valor=val;
    }

    public Nodo getDerecha() {
        return derecha;
    }

    public void setDerecha(Nodo derecha) {
        this.derecha = derecha;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Nodo getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Nodo izquierda) {
        this.izquierda = izquierda;
    }

    public Object getFruta() {
        return fruta;
    }

    public void setFruta(Object fruta) {
        this.fruta = fruta;
    }

    public Nodo getPadre() {
        return padre;
    }

    public void setPadre(Nodo padre) {
        this.padre = padre;
    }
}
