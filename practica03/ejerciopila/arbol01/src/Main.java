


      /*Confeccionar un programa clase que permita insertar un entero en un árbol binario ordenado verificando que no se encuentre previamente dicho número.
        Desarrollar los siguientes métodos:
        1 - Retornar la cantidad de nodos del árbol.
        2 - Retornar la cantidad de nodos hoja del árbol.
        3 - Imprimir en entre orden.
        4 - Imprimir en entre orden junto al nivel donde se encuentra dicho nodo.
        5 - Retornar la altura del árbol.
        6 - Imprimir el mayor valor del árbol.
        7 - Borrar el nodo menor del árbol.
        */

public class Main {
    public static void main(String[] args) {
    Arbol ar=new Arbol();
    ar.insert(2,"hola45");
        ar.insert(4,"hola45");
        ar.insert(1,"hola 34");
        ar.insert(3,"hola34");
        ar.insert(36,"hola34");
        ar.runar(ar.getRaiz());
    }
}