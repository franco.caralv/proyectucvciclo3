package vehiculos;

public final class Truck extends  Vehiculo{



    private double ancho;


    public Truck(double velocidad, double precioFabricacion, String color, double precioVenta, double ancho) {
        super(velocidad, precioFabricacion, color, precioVenta);
        this.ancho = ancho;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getPrecioVenta(){
        if(this.PrecioVenta >2000){
            return  this.PrecioVenta-this.PrecioVenta*0.10;
        }
        return this.PrecioVenta-this.PrecioVenta*0.20;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "ancho=" + ancho +
                ", velocidad=" + velocidad +
                ", PrecioFabricacion=" + PrecioFabricacion +
                ", Color='" + Color + '\'' +
                ", PrecioVenta=" + PrecioVenta +
                '}';
    }
}
