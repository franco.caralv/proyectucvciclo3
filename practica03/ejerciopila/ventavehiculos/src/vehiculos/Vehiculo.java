package vehiculos;

public class Vehiculo {

    protected double velocidad;

    protected double PrecioFabricacion;

    protected String Color;

    public double PrecioVenta;

    public Vehiculo(double velocidad, double precioFabricacion, String color, double precioVenta) {
        this.velocidad = velocidad;
        PrecioFabricacion = precioFabricacion;
        Color = color;
        PrecioVenta = precioVenta;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getPrecioFabricacion() {
        return PrecioFabricacion;
    }

    public void setPrecioFabricacion(double precioFabricacion) {
        PrecioFabricacion = precioFabricacion;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public double getPrecioVenta() {
        return PrecioVenta;
    }

    @Override
    public String toString() {
        return "Vehiculo{" +
                "velocidad=" + velocidad +
                ", PrecioFabricacion=" + PrecioFabricacion +
                ", Color='" + Color + '\'' +
                ", PrecioVenta=" + PrecioVenta +
                '}';
    }

    public void setPrecioVenta(double precioVenta) {
        PrecioVenta = precioVenta;
    }
}
