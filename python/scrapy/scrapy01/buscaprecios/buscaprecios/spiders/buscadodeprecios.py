from typing import List, Any

import scrapy
from scrapy.item import Item
from scrapy.item import Field
from scrapy.spiders import  CrawlSpider


class productos(Item):
    titulo=Field()
    precio=Field()
    descripcion=Field()


class buscadoprecios(CrawlSpider):
    name = "realprecios"

    start_urls = [
        "https://www.realplaza.com/"
    ]
    download_delay = 2

    custom_settings = {
        'USER_AGENT':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
        'FEEDS': {
            'quotes.json': {
                'format': 'json',
                'encoding': 'utf8',
                'store_empty': False,
                'fields': None,
                'indent': 4,
                'item_export_kwargs': {
                    'export_empty_fields': True,
                },
            },
        },
    }

    def parse(self, response, **kwargs):
        print("\n\n\n\n\n")
        print("\n\n\n\n\n holas")
        print("\n\n\n\n\n")
        print(response.xpath("/html/body/div[2]/div/div[1]/div/div[2]/div/div/div/div[13]/div[3]/div/div[3]/div/div/div/div[1]/div/div/section/div[1]/div/div[2]/div/div/div/div/div/img"))
        print("\n\n\n\n\n")
        yield {
            ""
        }
