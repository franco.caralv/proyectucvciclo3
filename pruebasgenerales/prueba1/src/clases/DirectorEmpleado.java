package clases;

public class DirectorEmpleado implements Empleados {

    private Creacioninformes infrome;

    public DirectorEmpleado(Creacioninformes infrome) {
        this.infrome = infrome;
    }

    @Override
    public String getempeleado() {
        return "Director empleado";
    }

    @Override
    public String getinforme() {
        return "Informe de director empleado" + infrome.getinforme();
    }

    public void ejercercargo() {

    }

    public void ondestroy() {

    }

}
