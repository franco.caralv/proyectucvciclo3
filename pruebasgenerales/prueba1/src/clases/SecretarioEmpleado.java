package clases;

public class SecretarioEmpleado implements Empleados {

    private Creacioninformes infrome;
    private String nombreempresa;

    private String correoempresa;

    @Override
    public String getempeleado() {
        return "Secretario empleado";
    }

    public void setinformenuevo(Creacioninformes infrome) {
        this.infrome = infrome;
    }

    public String getinformepleado() {
        return infrome.getinforme();
    }

    @Override
    public String getinforme() {
        return "Informe de secretario empleado" + infrome.getinforme();
    }

    public void setnombreempresa(String nombreempresa) {
        this.nombreempresa = nombreempresa;
    }

    public String getnombreempresa() {
        return nombreempresa;
    }

    public void setcorreoempresa(String correoempresa) {
        this.correoempresa = correoempresa;
    }

    public String getcorreoempresa() {
        return correoempresa;
    }

}
