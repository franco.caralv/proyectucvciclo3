package com.example.demo2;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/persona")
public class Personacontroller {

    private final AtomicLong counter = new AtomicLong();

    private static final String template = "Hola, %s!";

    @GetMapping("/name={name}")
    public Persona getpersona(@RequestParam(value = "name", defaultValue = "World") String name1) {
        return new Persona(counter.incrementAndGet(), String.format(template, "name"));
    }


}
