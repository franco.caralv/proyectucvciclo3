package com.franco.prubeas12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Prubeas12Application {

	public static void main(String[] args) {
		SpringApplication.run(Prubeas12Application.class, args);
	}

}
