package clases;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Comercialexperi implements Empleados {


    //solo si el setter
    @Autowired
    @Qualifier("informefinaiero3")
    private Creacioninforme informe;

    /*
     * @Autowired
     * public Comercialexperi(Creacioninforme informe) {
     * this.informe = informe;
     * }
     */
    /* @PostConstruct    
    public void ejecucionini(){
        System.out.println("Comercial experi ejecucion ini");
    }    
    @PreDestroy
    public void ejecucionfin(){
        System.out.println("Comercial experi ejecucion fin");
    }
 */

    public String gettareas() {
        return "Vender , Vender y Vender mas";
    }

    public Creacioninforme getInforme() {
        return informe;
    }

    public String getinforme() {
        /* return "Este es un informe de comercial experto"; */
        return informe.getinformefinanciore();
    }
    // Con el set
    /*
     * @Autowired
     * public void setInforme(Creacioninforme informe) {
     * this.informe = informe;
     * }
     */


}
