package clases;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {

        // ClassPathXmlApplicationContext context = new
        // ClassPathXmlApplicationContext("applicationContext.xml");

        // leer la clase
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(configuracion.class);

        // pedir el bean

        Empleados empleado = context.getBean("directorfinaciero", Empleados.class);

        System.out.println(empleado.gettareas());

        System.out.println(empleado.getinforme());

        Doctorfinanciero empleado3 = (Doctorfinanciero) empleado;

        System.out.println(empleado3.getEmail());

        System.out.println(empleado3.getNombreempresa());

        context.close();

    }

}