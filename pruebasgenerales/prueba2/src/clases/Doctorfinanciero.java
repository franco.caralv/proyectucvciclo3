package clases;

import org.springframework.beans.factory.annotation.Value;

public class Doctorfinanciero implements Empleados {

    private Creacioninforme informefinanciero;

    public Doctorfinanciero(Creacioninforme informefinanciero) {
        this.informefinanciero = informefinanciero;
    }

    @Override
    public String gettareas() {
        // TODO Auto-generated method stub
        return "Tareas de doctor financiero";
    }

    @Override
    public String getinforme() {
        // TODO Auto-generated method stub
        return "" + informefinanciero.getinformefinanciore();
    }


    @Value("${email}")
    private String email;

    @Value("${nombreemopresa}")
    private String nombreempresa;

    public String getEmail() {
        return email;
    }

    public String getNombreempresa() {
        return nombreempresa;
    }

    


}
