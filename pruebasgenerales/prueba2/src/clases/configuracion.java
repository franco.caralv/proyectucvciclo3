package clases;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "clases")
@PropertySource("classpath:Datos.propiedades")
public class configuracion {

    @Bean
    public Creacioninforme informedp() {
        return new Informedtodoctor();
    }

    @Bean
    public Empleados directorfinaciero() {
        return new Doctorfinanciero(informedp());
    }

    

}
