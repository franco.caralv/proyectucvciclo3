package clases;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Usoanotacion2 {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        Empleados empleado = context.getBean("comercialexperi", Empleados.class);

        Empleados empleado2 = context.getBean("comercialexperi", Empleados.class);

        if (empleado == empleado2) {
            System.out.println("son iguales " + empleado.toString() + " " + empleado2.hashCode());
        }else{
            System.out.println("son diferentes " + empleado.toString() + " " + empleado2.toString());

        }

        System.out.println(empleado.gettareas());

        System.out.println(empleado.getinforme());

        context.close();

    }

}
