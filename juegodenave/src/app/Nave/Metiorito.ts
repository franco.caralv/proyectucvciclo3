import Enemigo from "./Enemigo";


export default class Metiorito {

  public live = Math.floor(Math.random() * (this.ctx.canvas.height - this.height));

  public puntaje = 50;

  constructor(
    public x: number,
    public y: number,
    public width: number,
    public height: number,
    public speed: number,
    public img: string,
    public ctx: CanvasRenderingContext2D
  ) {

  }

  public draw() {
    const metiorito = new Image();
    metiorito.src = this.img;
    metiorito.onload = () => {
      this.ctx.drawImage(metiorito, this.x, this.y, this.width, this.height);
    }

  }

  public move() {

    this.y += this.speed;
    if (this.y > this.ctx.canvas.height) {
      this.y = 0;
      this.x = Math.floor(Math.random() * this.ctx.canvas.width);
    }

  }


  public isdead() {
    return this.live <= 0;
  }


}


