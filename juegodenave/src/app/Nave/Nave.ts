// import {Injectable} from "@angular/core";
//
// @Injectable({providedIn: 'root'})
import Bala from "./Bala";
import Metiorito from "./Metiorito";

export default class Nave {

  public bala="assets/download-removebg-preview.png";

  public listbalas:Bala[]=[];

  public limitedebalas=10;

  public estavivo=true;

  public puntaje = 0;

  public isCollision(meteorito:Metiorito){
    return this.x < meteorito.x + meteorito.width &&
      this.x + this.width > meteorito.x &&
      this.y < meteorito.y + meteorito.height-20 &&
      this.y + this.height-20 > meteorito.y;
  }

  public isCollisionene(meteorito:Metiorito){
    return this.x < meteorito.x + meteorito.width &&
      this.x + this.width > meteorito.x &&
      this.y < meteorito.y + meteorito.height-20 &&
      this.y + this.height-20 > meteorito.y;
  }

  constructor(
    public x: number,
    public y: number,
    public width: number,
    public height: number,
    public speed: number,
    public img: string,
    public ctx: CanvasRenderingContext2D
  ) {

  }

  public draw() {
    const nave = new Image();
    nave.src = this.img;
    nave.onload = () => {
      this.ctx.drawImage(nave, this.x, this.y, this.width, this.height);
    }
    this.listbalas.forEach(bala => {
      bala.draw();
      bala.move();
    });
    this.listbalas = this.listbalas.filter(bala => !bala.isOutOfBounds() && bala.isdead());
  }

  public move(lugar:number) {
    this.x += this.speed*lugar;
    if (this.x > this.ctx.canvas.width) {
      this.x = 0;
    }
    if (this.x < 0) {
      this.x = this.ctx.canvas.width;
    }


  }

  public disparar() {
    if (this.listbalas.length < this.limitedebalas) {
      this.listbalas.push(new Bala(this.x, this.y, 10, 10, 10, "assets/7-removebg-preview.png", this.ctx));
    }

  }




}
