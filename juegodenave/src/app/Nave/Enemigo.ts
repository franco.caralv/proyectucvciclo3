import Nave from "./Nave";

export default class Enemigo {

  public live = Math.floor(Math.random() * (this.ctx.canvas.height - this.height) + 500);

  public puntaje=80;

  constructor(
    public x: number,
    public y: number,
    public width: number,
    public height: number,
    public speed: number,
    public img: string,
    public ctx: CanvasRenderingContext2D
  ) {
  }


  public draw() {
    const enemigo = new Image();
    enemigo.src = this.img;
    enemigo.onload = () => {
      this.ctx.drawImage(enemigo, this.x, this.y, this.width, this.height);
    }
  }

  public move(nave: Nave) {
    let muitliplicador = 1;
    if (nave.x-Math.floor(Math.random()*100) <this.x) {
      muitliplicador = -1;
    }
    this.y += this.speed;
    this.x += Math.floor(Math.random() *5)*muitliplicador;
    if (this.y > this.ctx.canvas.height) {
      this.y = 0;
      this.x = Math.floor(Math.random() * this.ctx.canvas.width);
    }
    if (this.x > this.ctx.canvas.width) {
      this.x = 0;
    }
    if (this.x < 0) {
      this.x = this.ctx.canvas.width;
    }
  }


}
