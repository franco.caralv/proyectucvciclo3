import Metiorito from "./Metiorito";
import Enemigo from "./Enemigo";

export default class Bala {

  public daño(metiorito: Metiorito) {
    metiorito.live = metiorito.live - 50;
  }

  public dañoene(metiorito: Enemigo) {
    metiorito.live = metiorito.live - 150;
  }


  public estavivo = true;

  public isdead() {
    return this.estavivo;
  }

  constructor(
    public x: number,
    public y: number,
    public width: number,
    public height: number,
    public speed: number,
    public img: string,
    public ctx: CanvasRenderingContext2D
  ) {

  }

  public draw() {
    const bala = new Image();
    bala.src = this.img;
    bala.onload = () => {
      this.ctx.drawImage(bala, this.x, this.y, this.width, this.height);
    }

  }

  public isCollisionene(metiorito: Enemigo) {
    return (
      this.x < metiorito.x + metiorito.width &&
      this.x + this.width > metiorito.x &&
      this.y < metiorito.y + metiorito.height &&
      this.y + this.height > metiorito.y
    );
  }

  public isCollision(metiorito: Metiorito) {
    return (
      this.x < metiorito.x + metiorito.width &&
      this.x + this.width > metiorito.x &&
      this.y < metiorito.y + metiorito.height &&
      this.y + this.height > metiorito.y
    );
  }

  public isOutOfBounds() {
    return (
      this.x > this.ctx.canvas.width ||
      this.x < 0 ||
      this.y > this.ctx.canvas.height ||
      this.y < 0
    );
  }


  public move() {
    this.y -= this.speed;
  }

}

