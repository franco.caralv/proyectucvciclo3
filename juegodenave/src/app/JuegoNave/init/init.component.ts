import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import Nave from "../../Nave/Nave";
import {fromEvent, interval, Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import Metiorito from "../../Nave/Metiorito";
import Bala from "../../Nave/Bala";
import Enemigo from "../../Nave/Enemigo";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styleUrls: ['./init.component.scss']
})
export class InitComponent implements OnInit {

  @ViewChild('canvas') canvas!: ElementRef;

  public unsubscribe = new Subject<void>();

  public key$ = fromEvent(document, 'keydown');

  public nave: Nave | any;

  public isactivo = true;

  public bakcground: any;

  public ctx: CanvasRenderingContext2D | any;

  public cantidademeteoritos = 3;

  public contadodemeteoritos = 0;

  public listadodeimagenes = [
    "assets/1-removebg-preview.png",
  ]
  public metiorito: Metiorito[] = [];

  public enemigos: Enemigo[] = [];

  public cantidadenemigos = 2;

  public listadodeimagenesenemigos = [
    "assets/3-removebg-preview.png"
  ];
  public audio:any;

  public nivel = 1;

  constructor() {
  }

  ngOnInit(): void {
    this.key$
      .pipe(
        takeUntil(this.unsubscribe),
      ).subscribe((data: any) => {
      if (!this.isactivo)
        this.start();
      switch (data.key) {
        case "ArrowUp":
          this.nave.disparar();
          break;
        case "ArrowDown":
          break;
        case "ArrowLeft":
          this.nave.move(-1);
          break;
        case "ArrowRight":
          this.nave.move(1);
          break;
        default:
          break;
      }
    })
  }

  ngAfterViewInit() {
    this.ctx = this.canvas.nativeElement.getContext("2d");
    this.bakcground = new Image();
    this.bakcground.src = "assets/Capture.png";
    this.nave = new Nave(500, 500, 50, 50, 10, "assets/2-removebg-preview.png", this.ctx);
    this.audio = new Audio();
    this.audio.src = "assets/【FULL】Nanbaka-_ナンバカ_-Opening-Rin_-Rin_-Hi_-Hi_-_Piano-Synthesia-Sheet_.wav";
    this.audio.play();

    this.bakcground.onload = () => {
      this.ctx.drawImage(this.bakcground, 0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }
    interval(1500 / 60).pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(() => {
      this.myloop();
    })
  }

  public myloop() {
    setTimeout(
      () => {
        if (this.isactivo) {
          this.pintar();
          this.logicadelJuego();
          this.clear();
        }
      }
    )
  }

  public reset() {
    this.stop();
    this.isactivo = false;
    this.clear();
    this.nivel = 1;
    this.metiorito = [];
    this.contadodemeteoritos = 0;
    this.cantidadenemigos = 2;
    this.cantidademeteoritos = 3;
    this.audio.pause();
    this.audio.currentTime = 0;
    this.audio.play();
    this.enemigos = [];
    this.nave = new Nave(500, 500, 50, 50, 10, "assets/2-removebg-preview.png", this.ctx);
    this.pintar();
  }

  public logicadelJuego() {
    if (this.contadodemeteoritos < this.cantidademeteoritos) {
      this.contadodemeteoritos++;
      this.crearMeteorito();
    }
    if (this.cantidadenemigos > this.enemigos.length) {
      this.enemigos.push(new Enemigo(
        Math.floor(Math.random() * this.ctx.canvas.width),
        Math.floor(Math.random() * 50),
        Math.floor(Math.random() * 100) + 30,
        Math.floor(Math.random() * 100) + 30,
        Math.floor(Math.random() * 5) + 1,
        this.listadodeimagenesenemigos[Math.floor(Math.random() * this.listadodeimagenesenemigos.length)],
        this.ctx
      ));

    }

    this.metiorito.forEach((meteorito: Metiorito) => {
      this.nave.listbalas.forEach((bala: Bala) => {
        if (bala.isCollision(meteorito)) {
          bala.daño(meteorito);
          bala.estavivo = false;
        }
      });
      if (meteorito.live < 0) {
        this.nave.puntaje += meteorito.puntaje;
      }
      if (this.nave.isCollision(meteorito)) {
        this.stop();
        Swal.fire({
          title: 'Perdiste',
          text: '¿Quieres volver a intentarlo?',
          icon: 'warning',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          showCancelButton: true,
          showConfirmButton: true,
        }).then((result) => {
          if (result.isConfirmed) {
            this.reset();
          } else {
            this.stop();
          }
        })
      }
      meteorito.draw();
      meteorito.move();
    })
    this.enemigos.forEach((enemigo: Enemigo) => {
      this.nave.listbalas.forEach((bala: Bala) => {
        if (bala.isCollisionene(enemigo)) {
          bala.dañoene(enemigo);
          bala.estavivo = false;
        }

      })
      if (enemigo.live < 0) {
        this.nave.puntaje += enemigo.puntaje;
      }
      if (this.nave.isCollisionene(enemigo)) {
        this.stop();
        Swal.fire({
          title: 'Perdiste',
          text: '¿Quieres volver a intentarlo?',
          icon: 'warning',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          showCancelButton: true,
          showConfirmButton: true,
        }).then((result) => {
          if (result.isConfirmed) {
            this.reset();
          } else {
            this.stop();
          }
        })
      }
      enemigo.draw();
      enemigo.move(this.nave);
    })
    this.metiorito = this.metiorito.filter((meteorito: Metiorito) => meteorito.live > 0);
    this.contadodemeteoritos = this.metiorito.length;
    this.enemigos = this.enemigos.filter((enemigo: Enemigo) => enemigo.live > 0);
    if (this.nave.puntaje >= this.nivel * 300) {
      this.nivel++;
      this.cantidademeteoritos++;
      this.cantidadenemigos++;
    }
  }


  public crearMeteorito() {
    this.metiorito.push(new Metiorito(
      Math.floor(Math.random() * this.canvas.nativeElement.width),
      0,
      Math.floor(Math.random() * 100) + 50,
      Math.floor(Math.random() * 100) + 50,
      Math.floor(Math.random() * 5) + 1,
      this.listadodeimagenes[Math.floor(Math.random() * this.listadodeimagenes.length)],
      this.ctx
    ));

  }

  public pintar() {
    this.bakcground = new Image();
    this.bakcground.src = "assets/Capture.png";
    this.bakcground.onload = () => {
      this.ctx.drawImage(this.bakcground, 0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }
    this.nave.draw();
  }

  public clear() {
    this.ctx.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
  }

  public start() {
    this.isactivo = true;
    this.myloop()
  }

  public stop() {
    this.isactivo = false;
    this.audio.pause();
  }


}
