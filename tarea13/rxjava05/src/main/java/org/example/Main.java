package org.example;


import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import java.util.concurrent.atomic.AtomicReference;

public class Main {
    public static void main(String[] args) {
        AtomicReference<ObservableEmitter<Object>> objs = new AtomicReference<>();
        Subject<String> valor= PublishSubject.create();

        valor.subscribe(next->{
            System.out.println("que hola "+next);
        });
        valor.onNext("1");
        valor.onNext("2");
        valor.onNext("3");
    }
}