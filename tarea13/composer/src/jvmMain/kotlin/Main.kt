// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.material.MaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.*

@Composable
@Preview
fun App() {
    var text by remember { mutableStateOf("Hello, World!") }

    MaterialTheme {
        Column (modifier = Modifier.fillMaxWidth()) {
            Button(onClick = {
                text = "Hello, Desktop!"
            }) {
                Text(text)
            }
            Text("hola dios hola", modifier = Modifier.padding(15.dp).fillMaxSize(),
                textAlign = TextAlign.Center
            )
        }


    }
}

fun main() = application {
    Window(onCloseRequest = ::exitApplication,
    state = WindowState(WindowPlacement.Maximized,false, WindowPosition(Alignment.Center)),
        title = "App de franco",
    ) {
        App()
    }
}
