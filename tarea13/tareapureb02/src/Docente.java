public class Docente {
    //Constantes
    private static final float PAGO_HORA_Principal = 25;

    private static final float PAGO_HORA_Asociado = 18;

    private static final float PAGO_HORA_Auxiliar = 15;

    //Atributos
    private String nombre;
    private Categorias categoria;
    private String codigo;
    private Estudios estudios;
    private int antiguedad;
    private float horas;


    public Docente(final String nombre, final Categorias categoria,
                   final String codigo, final Estudios estudios,
                   final int antiguedad, final float horas) {
        this.nombre = nombre;
        this.categoria = categoria;
        this.codigo = codigo;
        this.estudios = estudios;
        this.antiguedad = antiguedad;
        this.horas = horas;
    }

    public float pagoPorHora() {
        float pago = 0;
        switch (categoria) {
            case Principal:
                pago =horas* PAGO_HORA_Principal;
                break;
            case Asociado:
                pago =horas* PAGO_HORA_Asociado;
                break;
            case Auxiliar:
                pago =horas* PAGO_HORA_Auxiliar;
                break;
        }
        return pago;
    }

    public float bonificacion(){
        float bonificacion = 0;
        switch (categoria) {
            case Principal:
                switch (estudios) {
                    case Ninguno:
                        bonificacion = 0f;
                        break;
                    case Maestria:
                        bonificacion = 0.17f;
                        break;
                    case Doctorado:
                        bonificacion = 0.2f;
                        break;
                    case Ambas:
                        bonificacion = 0.25f;
                        break;
                }
                break;
            case Asociado:
                switch (estudios) {
                    case Ninguno:
                        bonificacion = 0f;
                        break;
                    case Maestria:
                        bonificacion = 0.15f;
                        break;
                    case Doctorado:
                        bonificacion = 0.1f;
                        break;
                    case Ambas:
                        bonificacion = 0.2f;
                        break;
                }
                break;
            case Auxiliar:
                switch (estudios) {
                    case Ninguno:
                        bonificacion = 0f;
                        break;
                    case Maestria:
                        bonificacion = 0.12f;
                        break;
                    case Doctorado:
                        bonificacion = 0.08f;
                        break;
                    case Ambas:
                        bonificacion = 0.17f;
                        break;
                }
                break;
        }
        return bonificacion*pagoPorHora();
    }

    public float sueldoBruto(){
        return pagoPorHora()+bonificacion();
    }

    public float descuento(){
    if(antiguedad<7){
        return sueldoBruto()*0.05f;
    }

    if (antiguedad>=8){
        return sueldoBruto()*0.04f;
    }
    return 0;
    }

    public float sueldoNeto(){
        return sueldoBruto()-descuento();
    }




    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Categorias getCategoria() {
        return categoria;
    }

    public void setCategoria(Categorias categoria) {
        this.categoria = categoria;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Estudios getEstudios() {
        return estudios;
    }

    public void setEstudios(Estudios estudios) {
        this.estudios = estudios;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }


}
