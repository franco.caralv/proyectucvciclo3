public class Nodo<T> {
    private T dato;
    private Nodo<T> siguiente;

    private  Nodo<T> anterior;

    public Nodo(final T dato) {
        this.dato = dato;
        this.siguiente = null;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(final T dato) {
        this.dato = dato;
    }

    public Nodo<T> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(final Nodo<T> siguiente) {
        this.siguiente = siguiente;
    }

    public Nodo<T> getAnterior() {
        return anterior;
    }

    public void setAnterior(final Nodo<T> anterior) {
        this.anterior = anterior;
    }
}
