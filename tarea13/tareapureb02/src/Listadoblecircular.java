public class Listadoblecircular {

    private Nodo<Docente> inicio;
    private Nodo<Docente> fin;
    private int tamanio;

    public Listadoblecircular() {
        this.inicio = null;
        this.fin = null;
        this.tamanio = 0;
    }

    public void insertarInicio(final Docente docente) {
        Nodo<Docente> nuevo = new Nodo<Docente>(docente);
        if (this.inicio == null) {
            this.inicio = nuevo;
            this.fin = nuevo;
            this.inicio.setSiguiente(this.fin);
            this.fin.setAnterior(this.inicio);
        } else {
            nuevo.setSiguiente(this.inicio);
            this.inicio.setAnterior(nuevo);
            this.inicio = nuevo;
        }
        this.tamanio++;
    }

    public void insertarFin(final Docente docente) {
        Nodo<Docente> nuevo = new Nodo<Docente>(docente);
        if (this.inicio == null) {
            this.inicio = nuevo;
            this.fin = nuevo;
            this.inicio.setSiguiente(this.fin);
            this.fin.setAnterior(this.inicio);
        } else {
            nuevo.setAnterior(this.fin);
            this.fin.setSiguiente(nuevo);
            this.fin = nuevo;
        }
        this.tamanio++;
    }

    public void insertar(final Docente docente, final int posicion) {
        if (posicion < 0 || posicion > this.tamanio) {
            throw new IllegalArgumentException("Posicion invalida");
        }
        if (posicion == 0) {
            this.insertarInicio(docente);
        } else if (posicion == this.tamanio) {
            this.insertarFin(docente);
        } else {
            Nodo<Docente> actual = this.inicio;
            for (int i = 0; i < posicion; i++) {
                actual = actual.getSiguiente();
            }
            Nodo<Docente> nuevo = new Nodo<Docente>(docente);
            nuevo.setSiguiente(actual);
            nuevo.setAnterior(actual.getAnterior());
            actual.getAnterior().setSiguiente(nuevo);
            actual.setAnterior(nuevo);
            this.tamanio++;
        }
    }

    public void eliminarInicio() {
        if (this.inicio == null) {
            throw new IllegalArgumentException("Lista vacia");
        }
        if (this.inicio == this.fin) {
            this.inicio = null;
            this.fin = null;
        } else {
            this.inicio = this.inicio.getSiguiente();
            this.inicio.setAnterior(null);
        }
        this.tamanio--;
    }

    public void eliminarFin() {
        if (this.inicio == null) {
            throw new IllegalArgumentException("Lista vacia");
        }
        if (this.inicio == this.fin) {
            this.inicio = null;
            this.fin = null;
        } else {
            this.fin = this.fin.getAnterior();
            this.fin.setSiguiente(null);
        }
        this.tamanio--;
    }

    public void eliminar(final int posicion) {
        if (posicion < 0 || posicion >= this.tamanio) {
            throw new IllegalArgumentException("Posicion invalida");
        }
        if (posicion == 0) {
            this.eliminarInicio();
        } else if (posicion == this.tamanio - 1) {
            this.eliminarFin();
        } else {
            Nodo<Docente> actual = this.inicio;
            for (int i = 0; i < posicion; i++) {
                actual = actual.getSiguiente();
            }
            actual.getAnterior().setSiguiente(actual.getSiguiente());
            actual.getSiguiente().setAnterior(actual.getAnterior());
            this.tamanio--;
        }
    }

    public Docente obtener(final int posicion) {
        if (posicion < 0 || posicion >= this.tamanio) {
            throw new IllegalArgumentException("Posicion invalida");
        }
        Nodo<Docente> actual = this.inicio;
        for (int i = 0; i < posicion; i++) {
            actual = actual.getSiguiente();
        }
        return actual.getDato();
    }

    public int tamanio() {
        return this.tamanio;
    }

    public boolean esVacia() {
        return this.tamanio == 0;
    }

    public void imprimir() {
        Nodo<Docente> actual = this.inicio;
        for (int i = 0; i < this.tamanio; i++) {
            Docente docente = actual.getDato();
            System.out.println("---------------------------------");
            System.out.println("Docente: " + docente.getNombre());
            System.out.println("Categoria: " + docente.getCategoria());
            System.out.println("Estudios: " + docente.getEstudios());
            System.out.println("Antiguedad: " + docente.getAntiguedad());
            System.out.println("Horas: " + docente.getHoras());
            System.out.println("Salario por hora: " + docente.pagoPorHora());
            System.out.println("Salario bruto: " + docente.sueldoBruto());
            System.out.println("Salario neto: " + docente.sueldoNeto());
            System.out.println("Bonificacion: " + docente.bonificacion());
            System.out.println("Descuento por antiguedad: " + docente.descuento());
            System.out.println("---------------------------------");
            actual = actual.getSiguiente();
        }
    }


    public Docente buscarMenorSueldoNeto() {
        Nodo<Docente> actual = this.inicio;
        Docente menor = actual.getDato();
        for (int i = 0; i < this.tamanio; i++) {
            if (actual.getDato().sueldoNeto() < menor.sueldoNeto()) {
                menor = actual.getDato();
            }
            actual = actual.getSiguiente();
        }
        return menor;
    }

    //Promedio de Descuentos de docentes auxiliares con más 10 años de antigüedad.
    public double promedioDescuentos() {
        double promedio = 0;
        int contador = 0;
        Nodo<Docente> actual = this.inicio;
        for (int i = 0; i < this.tamanio; i++) {
            if (actual.getDato().getAntiguedad() > 10) {
                promedio += actual.getDato().descuento();
                contador++;
            }
            actual = actual.getSiguiente();
        }
        return promedio / contador;
    }

    //Número de Empleados cuyo Bonificación superen los 100 soles con más 20 horas de clase
    public int numeroEmpleadosBonificacion() {
        int contador = 0;
        Nodo<Docente> actual = this.inicio;
        for (int i = 0; i < this.tamanio; i++) {
            if (actual.getDato().bonificacion() > 100 && actual.getDato().getHoras() > 20) {
                contador++;
            }
            actual = actual.getSiguiente();
        }
        return contador;
    }

    //Número de docentes con pago parcial entre  3000 y 5000 que sean Principales y
    //    que tengan el grado de Maestría.
    public int numeroDocentesPagoParcial() {
        int contador = 0;
        Nodo<Docente> actual = this.inicio;
        for (int i = 0; i < this.tamanio; i++) {
            if (actual.getDato().getCategoria() == Categorias.Principal && actual.getDato().sueldoBruto() > 3000 && actual.getDato().sueldoBruto() < 5000 && actual.getDato().getEstudios() == Estudios.Maestria) {
                contador++;
            }
            actual = actual.getSiguiente();
        }
        return contador;
    }


}
