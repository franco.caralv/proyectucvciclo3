import java.util.Scanner;

public class main {

    /*Construya un aplicativo que manipule un arreglo de objetos que muestre la siguiente información:
    a)	Nombre del docente asociado tenga el menor sueldo neto
    b)	Promedio de Descuentos de docentes auxiliares con más 10 años de antigüedad.
    c)	Número de Empleados cuyo Bonificación superen los 100 soles con más 20 horas de clase
    d)	Número de docentes con pago parcial entre  3000 y 5000 que sean Principales y
    que tengan el grado de Maestría.*/
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Bienvenido al sistema de gestión de docentes");
        Listadoblecircular lista = new Listadoblecircular();
        int posicion;
        Docente docente;
        int opcion = 0;
        do {
            System.out.println("1. Insertar docente");
            System.out.println("2. Insertar docente inicio");
            System.out.println("3. Insertar docente fin");
            System.out.println("4. Mostrar docentes");
            System.out.println("5. Eliminar docente");
            System.out.println("6. Eleminar Final");
            System.out.println("7. Eliminar Inicio");
            System.out.println("8. Salir");
            System.out.println("Ingrese una opcion");
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    docente = crearNuevoDocente(sc);
                    System.out.println("Ingrese la posicion del docente");
                    posicion = sc.nextInt();
                    lista.insertar(docente, posicion);
                    break;
                case 2:
                    docente = crearNuevoDocente(sc);
                    lista.insertarInicio(docente);
                    break;
                case 3:
                    docente = crearNuevoDocente(sc);
                    lista.insertarFin(docente);
                    break;
                case 4:
                    lista.imprimir();
                    break;
                case 5:
                    System.out.println("Ingrese la posicion del docente");
                    posicion = sc.nextInt();
                    lista.eliminar(posicion);
                    break;
                case 6:
                    lista.eliminarFin();
                    break;
                case 7:
                    lista.eliminarInicio();
                    break;
                case 8:
                    System.out.println("Gracias por usar el sistema");
                    break;
                default:
                    System.out.println("Opcion no valida");
            }
        } while (opcion != 8);

        System.out.println("El docente con menor sueldo neto es: " + lista.buscarMenorSueldoNeto().sueldoNeto());
        System.out.println("El promedio de descuentos de docentes auxiliares con más 10 años de antigüedad es: " + lista.promedioDescuentos());
        System.out.println("El numero de empleados cuyo bonificacion superen los 100 soles con mas 20 horas de clase es: " + lista.numeroEmpleadosBonificacion());
        System.out.println("El numero de docentes con pago parcial entre 3000 y 5000 que sean Principales y que tengan el grado de Maestria es: " + lista.numeroDocentesPagoParcial());
        System.out.println("Gracias por usar el sistema");

    }

    public static Docente crearNuevoDocente(Scanner sc) {
        String nombre, codigo;
        Categorias categoria;
        Estudios estudios;
        int antiguedad;
        float horas;
        System.out.println("Ingrese el nombre del docente");
        nombre = sc.next();
        categoria = createNewCategoria(sc);
        System.out.println("Ingrese el codigo del docente");
        codigo = sc.next();
        estudios =createNewEstudio(sc);
        System.out.println("Ingrese la antiguedad del docente");
        antiguedad = sc.nextInt();
        System.out.println("Ingrese las horas del docente");
        horas = sc.nextFloat();
        return new Docente(nombre, categoria, codigo, estudios, antiguedad, horas);
    }

    public static Categorias createNewCategoria(Scanner sc) {
        System.out.println("Ingrese la categoria del docente");
        System.out.println("1. Principal");
        System.out.println("2. Asociado");
        System.out.println("3. Auxiliar");
        int opcion = sc.nextInt();
        switch (opcion) {
            case 1:
                return Categorias.Principal;
            case 2:
                return Categorias.Asociado;
            case 3:
                return Categorias.Auxiliar;
            default:
                return Categorias.Principal;
        }

    }


    public static Estudios createNewEstudio(Scanner sc){
        System.out.println("Ingrese el estudio del docente");
        System.out.println("1. Ninguno");
        System.out.println("2. Maestria");
        System.out.println("3. Doctorado");
        System.out.println("4. Ambas");
        int opcion = sc.nextInt();
        switch (opcion) {
            case 1:
                return Estudios.Ninguno;
            case 2:
                return Estudios.Maestria;
            case 3:
                return Estudios.Doctorado;
            case 4:
                return Estudios.Ambas;
            default:
                return Estudios.Ninguno;
        }
    }


}
