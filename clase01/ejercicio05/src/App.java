import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Las marcas son: ");
        System.out.println("1.Nike");
        System.out.println("2.Adidas");
        System.out.println("3.Fila");
        System.out.println("Las tallas son: ");
        System.out.println("1) 38");
        System.out.println("2) 40");
        System.out.println("3) 42");
        System.out.println("Pares de zapatillas :");
        System.out.println("2 a 5 5% de venta");
        System.out.println("6 a 10 8% de venta");
        System.out.println("11 a 20 10% de venta");
        System.out.println("21 a mas 15% de venta");
        System.out.println("Ingrese la marca de la zapatillas");
        String marca = sc.nextLine().toLowerCase();
        System.out.println("Ingrese la talla de la zapatillas");
        int talla = sc.nextInt();
        System.out.println("Ingrese el par de zapatillas que vas a comprar");
        int par = sc.nextInt();
        Zapatilla zapatilla = new Zapatilla(marca, talla, par);
        System.out.println("El precio neto de la zapatilla es: " + zapatilla.precioneto());
        System.out.println("El descuento de la zapatilla es: " + zapatilla.getDescuento());
        System.out.println("El precio neto de la zapatilla es: " + zapatilla.getNeto());
        System.out.println(zapatilla);

    }
}
