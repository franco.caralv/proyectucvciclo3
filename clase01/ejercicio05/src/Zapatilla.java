public class Zapatilla {

    private String marca;
    private int talla;
    private int par;
    private double precio;
    private String descuento;
    private double neto;

    public Zapatilla(String marca, int talla, int par) {
        this.marca = marca;
        this.talla = talla >= 38 && talla <= 42 ? talla : 38;
        this.par = par;
        this.poner_precio();
    }

    private void poner_precio() {
        if (talla == 38) {
            if (marca.equals("nike")) {
                precio = 150;
            } else if (marca.equals("adidas")) {
                precio = 140;
            } else if (marca.equals("fila")) {
                precio = 80;
            }
        } else if (talla == 40) {
            if (marca.equals("nike")) {
                precio = 160;
            } else if (marca.equals("adidas")) {
                precio = 150;
            } else if (marca.equals("fila")) {
                precio = 85;
            }
        } else if (talla == 42) {
            if (marca.equals("nike")) {
                precio = 160;
            } else if (marca.equals("adidas")) {
                precio = 150;
            } else if (marca.equals("fila")) {
                precio = 90;
            }
        }
    }

    public double precioneto() {
        double precio_neto = precio;
        if (par >= 2 && par <= 5) {
            neto = (precio_neto * 0.05);
            precio_neto = precio_neto - (precio_neto * 0.05);
        } else if (par >= 6 && par <= 10) {
            neto = (precio_neto * 0.08);
            precio_neto = precio_neto - (precio_neto * 0.08);
        } else if (par >= 11 && par <= 20) {
            neto = (precio_neto * 0.10);
            precio_neto = precio_neto - (precio_neto * 0.10);
        } else if (par >= 21) {
            neto = (precio_neto * 0.15);
            precio_neto = precio_neto - (precio_neto * 0.15);
        }
        return precio_neto;
    }

    public String getDescuento() {
        if (par >= 2 && par <= 5) {
            descuento = "5% de descuento";
        } else if (par >= 6 && par <= 10) {
            descuento = "8% de descuento";
        } else if (par >= 11 && par <= 20) {
            descuento = "10% de descuento";
        } else if (par >= 21) {
            descuento = "15% de descuento";
        }
        return descuento;
    }

    public double getNeto() {
        return neto;
    }

    public double getPrecio() {
        return precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public int getPar() {
        return par;
    }

    public void setPar(int par) {
        this.par = par;
    }

    @Override
    public String toString() {
        return "Zapatilla{" + "marca=" + marca + ", talla=" + talla + ", par=" + par + '}';
    }

}
