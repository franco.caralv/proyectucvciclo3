package presentacion;


import Clase.empleados;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.table.DefaultTableModel;
import javax.swing.*;
public class Panel extends javax.swing.JFrame {

  DefaultTableModel modelo=new DefaultTableModel();
  ArrayList<empleados>lista = new ArrayList();
   int indiceBusqueda;
    public Panel() {
        initComponents();
        setLocationRelativeTo(null);
        mostrar(modelo);

    }
    
  
    public void limpiarEntradas(){
        txt_dni.setText("");
        txt_apellidos.setText("");
        txt_nombre.setText("");
        cbo_sexo.setSelectedIndex(0);
        txt_edad.setText("");
        txt_telefono.setText("");
        txt_dni.setText("");
        
        
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        txt_dni = new javax.swing.JTextField();
        txt_apellidos = new javax.swing.JTextField();
        txt_nombre = new javax.swing.JTextField();
        cbo_sexo = new javax.swing.JComboBox<>();
        txt_telefono = new javax.swing.JTextField();
        txt_edad = new javax.swing.JTextField();
        btnguardar = new javax.swing.JButton();
        btnconsultar = new javax.swing.JButton();
        btnrestaurar = new javax.swing.JButton();
        btnordenar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnactualizar = new javax.swing.JButton();
        btneliminar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_dni.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(txt_dni, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 40, 160, 30));

        txt_apellidos.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(txt_apellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 90, 160, 30));

        txt_nombre.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(txt_nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, 160, 30));

        cbo_sexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MASCULINO", "FEMENINO", " " }));
        cbo_sexo.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        cbo_sexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbo_sexoActionPerformed(evt);
            }
        });
        getContentPane().add(cbo_sexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 340, 140, 30));

        txt_telefono.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txt_telefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_telefonoActionPerformed(evt);
            }
        });
        getContentPane().add(txt_telefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 270, 160, 30));

        txt_edad.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txt_edad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_edadActionPerformed(evt);
            }
        });
        getContentPane().add(txt_edad, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 210, 160, 30));

        btnguardar.setText("GUARDAR");
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnguardar, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 40, -1, -1));

        btnconsultar.setText("CONSULTAR");
        btnconsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnconsultarActionPerformed(evt);
            }
        });
        getContentPane().add(btnconsultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 40, -1, -1));

        btnrestaurar.setText("RESTAURAR");
        btnrestaurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrestaurarActionPerformed(evt);
            }
        });
        getContentPane().add(btnrestaurar, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 80, -1, -1));

        btnordenar.setText("ORDENAR");
        btnordenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnordenarActionPerformed(evt);
            }
        });
        getContentPane().add(btnordenar, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 80, -1, -1));

        jTable1.setModel(modelo);
        jScrollPane1.setViewportView(jTable1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 130, 539, 274));

        btnactualizar.setText("ACTUALIZAR");
        btnactualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnactualizarActionPerformed(evt);
            }
        });
        getContentPane().add(btnactualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 80, -1, -1));

        btneliminar.setText("ELIMINAR");
        btneliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneliminarActionPerformed(evt);
            }
        });
        getContentPane().add(btneliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, -1, -1));

        jLabel1.setText("APELLIDOS :");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, -1, -1));

        jLabel2.setText("NOMBRE :");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, -1, -1));

        jLabel3.setText("EDAD :");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, -1, -1));

        jLabel4.setText("TELÉFONO :");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 280, -1, -1));

        jLabel5.setText("GENERO :");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 340, -1, -1));

        jLabel6.setText("DNI :");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
     String dni=txt_dni.getText();
     String apellidos=txt_apellidos.getText();
     String nombres=txt_nombre.getText();
     String sexo=cbo_sexo.getSelectedItem().toString();
     int edad=Integer.parseInt(txt_edad.getText());
     String numero=(txt_telefono.getText());
     empleados x=new empleados(dni,apellidos,nombres,sexo,edad,numero);
     lista.add(x);

     mostrar(modelo);
     limpiarEntradas();
     
    }//GEN-LAST:event_btnguardarActionPerformed

    private void btnconsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnconsultarActionPerformed
   String dni=txt_dni.getText();
   if(dni.equals(""))
       JOptionPane.showMessageDialog(null,"Ingrese DNI por favor");
   else{
       indiceBusqueda=buscar(dni);
       if(indiceBusqueda!=-1){
           empleados x=getEmpleados(indiceBusqueda);
           txt_dni.setText(x.getDni());
           txt_apellidos.setText(x.getApellidos());
           txt_nombre.setText(x.getNombres());
           if(x.getSexo().equalsIgnoreCase("MASCULINO"))
               cbo_sexo.setSelectedIndex(0);
           else
               cbo_sexo.setSelectedIndex(0);
           txt_edad.setText(String.valueOf(x.getEdad()));
           txt_telefono.setText(String.valueOf(x.getTelefono()));
       }
       else 
           JOptionPane.showMessageDialog(null,"¡NO EXISTE EL DNI!");
   }
    }//GEN-LAST:event_btnconsultarActionPerformed

    private void btnrestaurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrestaurarActionPerformed

     limpiarEntradas();
    }//GEN-LAST:event_btnrestaurarActionPerformed

    private void btnordenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnordenarActionPerformed
   String opciones[]={"APELLIDOS","EDAD"};
   int election=JOptionPane.showOptionDialog(null,"Escoger opcion","opciones",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,opciones,opciones[0]);
   switch(election){
       case 0:OrdenarPorApellidos();
              mostrar(modelo);
              break;
       case 1: OrdenarPorEdad();
               mostrar(modelo);
   }
   
    }//GEN-LAST:event_btnordenarActionPerformed

    private void txt_edadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_edadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_edadActionPerformed

    private void btnactualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnactualizarActionPerformed
       String dni=txt_dni.getText();
     String apellidos=txt_apellidos.getText();
     String nombres=txt_nombre.getText();
     String sexo=cbo_sexo.getSelectedItem().toString();
     int edad=Integer.parseInt(txt_edad.getText());
     String numero=(txt_telefono.getText());
     empleados x=new empleados(dni,apellidos,nombres,sexo,edad,numero);
     lista.set(indiceBusqueda,x);
     mostrar(modelo);
     
     limpiarEntradas();
    }//GEN-LAST:event_btnactualizarActionPerformed

    private void btneliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneliminarActionPerformed
       lista.remove(indiceBusqueda);
       mostrar(modelo);
    
       limpiarEntradas();
    }//GEN-LAST:event_btneliminarActionPerformed

    private void txt_telefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_telefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_telefonoActionPerformed

    private void cbo_sexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbo_sexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbo_sexoActionPerformed
public void mostrar(DefaultTableModel modelo){
      Object datos[][]=new Object [lista.size()][6];
      String titulos[]={"DNI","APELLIDOS","NOMBRES","EDAD","SEXO","TÉLEFONO"};
      for(int i=0;i<lista.size();i++){
          empleados x=lista.get(i);
          datos[i][0]=x.getDni();
          datos[i][1]=x.getApellidos();
          datos[i][2]=x.getNombres();
          datos[i][3]=x.getEdad();
          datos[i][4]=x.getSexo();
          datos[i][5]=x.getTelefono();
     }
      modelo.setDataVector(datos,titulos);
}
public void OrdenarPorApellidos(){
        Collections.sort(lista,(empleados persona1, empleados persona2)->(persona1.getApellidos()).compareToIgnoreCase(persona2.getApellidos()));
    }
    
    public void OrdenarPorEdad(){
        Collections.sort(lista,(empleados persona1,empleados persona2)-> new Integer(persona1.getEdad()).compareTo(persona2.getEdad()));
    
    }
   
    public int buscar(String dni){
        empleados  empleado =new empleados(dni);
        return lista.indexOf(empleado);
        
    }
    public empleados getEmpleados(int indice){
        return lista.get(indice);
        
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Panel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnactualizar;
    private javax.swing.JButton btnconsultar;
    private javax.swing.JButton btneliminar;
    private javax.swing.JButton btnguardar;
    private javax.swing.JButton btnordenar;
    private javax.swing.JButton btnrestaurar;
    private javax.swing.JComboBox<String> cbo_sexo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JList<String> jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txt_apellidos;
    private javax.swing.JTextField txt_dni;
    private javax.swing.JTextField txt_edad;
    private javax.swing.JTextField txt_nombre;
    private javax.swing.JTextField txt_telefono;
    // End of variables declaration//GEN-END:variables

}
