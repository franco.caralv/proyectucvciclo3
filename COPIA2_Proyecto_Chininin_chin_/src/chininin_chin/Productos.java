
package chininin_chin;
public class Productos implements Comparable<Productos>
{
    
     // variables 
    String nombre;
    double precio;
    int cantidad;
    double total;
    double descuento;
    double igv;

    // constructor
    public Productos(String nombre, double precio, int cantidad) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;

    }

    public Productos(double total) { //constructor
        this.total = total;
        
    }

    public String getNombre() { 
        return nombre;
    }

    public void setNombre(String nombre) { 
        this.nombre = nombre;
    }

    public double getPrecio() {  
        return precio;
    }

    public void setPrecio(double precio) {   
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;    
    }

    // métodos
    
    public double CalcularDescuento() {

        this.descuento = 0;

        if (this.total > 1000 && this.total <= 2000) {
            this.descuento = (this.total * 0.08);

        } else {
            if (this.total > 2000 && this.total <= 3000) {
                this.descuento = (this.total * 0.12);

            } else {
                if (this.total > 3000) {
                    this.descuento = (this.total * 0.15);

                }
            }
        }

        return this.descuento;
    }

    public double CalculartotalDes() {

        return this.total - this.descuento;

    }

    public double calcularMontoU() {
        return cantidad * precio;

    }

    public double CalcularIgv() {
         igv = 0.18;
        return this.total * this.igv;
    }

    @Override
    public int compareTo(Productos o) {
        Double num1 =o.calcularMontoU();
        Double num2=calcularMontoU();
        return num2.compareTo(num1);
    }

    @Override
    public String toString() {
        return "Productos{" + "nombre=" + nombre + ", precio=" + precio + ", cantidad=" + cantidad + ", total=" + total + ", descuento=" + descuento + ", igv=" + igv + '}';
    }
    
    
    
}
