/*
    Mas o menos Pasable fal signo y porcentaje
    subString = una parte de la cadena como decir (conjunto = string, subconjunto = substring)
 */
package chininin_chin;

/**
 *
 * @author ACER
 */
public class calculadora extends javax.swing.JFrame {

    public double[] numeros = new double[2]; 

    public boolean decimal = true;

    public boolean divicion = false;
    public boolean multiplicacion = false;
    public boolean resta = false;
    public boolean suma = false;

    public boolean porcentaje = false;
    public boolean signo = false;

    public calculadora() {
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
        setSize(315, 550); //modificar tamaño de la ventana de la calculadora
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btn_borrar = new javax.swing.JButton();
        btn_signo = new javax.swing.JButton();
        btn_porcentaje = new javax.swing.JButton();
        btn_divi = new javax.swing.JButton();
        btn_7 = new javax.swing.JButton();
        btn_8 = new javax.swing.JButton();
        btn_9 = new javax.swing.JButton();
        btn_mult = new javax.swing.JButton();
        btn_4 = new javax.swing.JButton();
        btn_5 = new javax.swing.JButton();
        btn_6 = new javax.swing.JButton();
        btn_resta = new javax.swing.JButton();
        btn_1 = new javax.swing.JButton();
        btn_2 = new javax.swing.JButton();
        btn_suma = new javax.swing.JButton();
        btn_0 = new javax.swing.JButton();
        btn_punto = new javax.swing.JButton();
        btn_igual = new javax.swing.JButton();
        btn_3 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        txt_resultado = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_borrar.setBackground(new java.awt.Color(255, 255, 255));
        btn_borrar.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_borrar.setText("AC");
        btn_borrar.setFocusable(false);
        btn_borrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_borrarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_borrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 64));

        btn_signo.setBackground(new java.awt.Color(255, 255, 255));
        btn_signo.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_signo.setText("+/-");
        btn_signo.setFocusable(false);
        btn_signo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_signoActionPerformed(evt);
            }
        });
        jPanel2.add(btn_signo, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 70, 64));

        btn_porcentaje.setBackground(new java.awt.Color(255, 255, 255));
        btn_porcentaje.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_porcentaje.setText("%");
        btn_porcentaje.setFocusable(false);
        btn_porcentaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_porcentajeActionPerformed(evt);
            }
        });
        jPanel2.add(btn_porcentaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, 70, 64));

        btn_divi.setBackground(new java.awt.Color(255, 255, 255));
        btn_divi.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_divi.setText("/");
        btn_divi.setFocusable(false);
        btn_divi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_diviActionPerformed(evt);
            }
        });
        jPanel2.add(btn_divi, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, 70, 64));

        btn_7.setBackground(new java.awt.Color(255, 255, 255));
        btn_7.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_7.setText("7");
        btn_7.setFocusable(false);
        btn_7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_7ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 70, 64));

        btn_8.setBackground(new java.awt.Color(255, 255, 255));
        btn_8.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_8.setText("8");
        btn_8.setFocusable(false);
        btn_8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_8ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_8, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, 70, 64));

        btn_9.setBackground(new java.awt.Color(255, 255, 255));
        btn_9.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_9.setText("9");
        btn_9.setFocusable(false);
        btn_9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_9ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_9, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, 70, 64));

        btn_mult.setBackground(new java.awt.Color(255, 255, 255));
        btn_mult.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_mult.setText("x");
        btn_mult.setFocusable(false);
        btn_mult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_multActionPerformed(evt);
            }
        });
        jPanel2.add(btn_mult, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, 70, 64));

        btn_4.setBackground(new java.awt.Color(255, 255, 255));
        btn_4.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_4.setText("4");
        btn_4.setFocusable(false);
        btn_4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_4ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 70, 64));

        btn_5.setBackground(new java.awt.Color(255, 255, 255));
        btn_5.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_5.setText("5");
        btn_5.setFocusable(false);
        btn_5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_5ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 150, 70, 64));

        btn_6.setBackground(new java.awt.Color(255, 255, 255));
        btn_6.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_6.setText("6");
        btn_6.setFocusable(false);
        btn_6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_6ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_6, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, 70, 64));

        btn_resta.setBackground(new java.awt.Color(255, 255, 255));
        btn_resta.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_resta.setText("-");
        btn_resta.setFocusable(false);
        btn_resta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_restaActionPerformed(evt);
            }
        });
        jPanel2.add(btn_resta, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 150, 70, 64));

        btn_1.setBackground(new java.awt.Color(255, 255, 255));
        btn_1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_1.setText("1");
        btn_1.setFocusable(false);
        btn_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_1ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 70, 64));

        btn_2.setBackground(new java.awt.Color(255, 255, 255));
        btn_2.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_2.setText("2");
        btn_2.setFocusable(false);
        btn_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_2ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 220, 70, 64));

        btn_suma.setBackground(new java.awt.Color(255, 255, 255));
        btn_suma.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_suma.setText("+");
        btn_suma.setFocusable(false);
        btn_suma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_sumaActionPerformed(evt);
            }
        });
        jPanel2.add(btn_suma, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 220, 70, 64));

        btn_0.setBackground(new java.awt.Color(255, 255, 255));
        btn_0.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_0.setText("0");
        btn_0.setFocusable(false);
        btn_0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_0ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_0, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 140, 64));

        btn_punto.setBackground(new java.awt.Color(255, 255, 255));
        btn_punto.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_punto.setText(".");
        btn_punto.setFocusable(false);
        btn_punto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_puntoActionPerformed(evt);
            }
        });
        jPanel2.add(btn_punto, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 290, 70, 64));

        btn_igual.setBackground(new java.awt.Color(255, 255, 255));
        btn_igual.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_igual.setText("=");
        btn_igual.setFocusable(false);
        btn_igual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_igualActionPerformed(evt);
            }
        });
        jPanel2.add(btn_igual, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 290, 70, 64));

        btn_3.setBackground(new java.awt.Color(255, 255, 255));
        btn_3.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        btn_3.setText("3");
        btn_3.setFocusable(false);
        btn_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_3ActionPerformed(evt);
            }
        });
        jPanel2.add(btn_3, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 220, 70, 64));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 300, 380));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_resultado.setBackground(new java.awt.Color(17, 62, 142));
        txt_resultado.setFont(new java.awt.Font("Arial", 1, 50)); // NOI18N
        txt_resultado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txt_resultado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel3.add(txt_resultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 280, 53));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 300, 100));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 310, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_borrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_borrarActionPerformed
        txt_resultado.setText("");

        decimal = true;
        divicion = false;
        multiplicacion = false;
        suma = false;
        resta = false;
        porcentaje = false;

        numeros[0] = 0;
        numeros[1] = 0;
    }//GEN-LAST:event_btn_borrarActionPerformed

    private void btn_signoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_signoActionPerformed
        try {
            double cambio;
            String entero;
            cambio = Double.parseDouble(txt_resultado.getText());
            cambio = cambio * -1;
            if (cambio % 1 == 0) { //123.1 % 1 = 0,1 de resto  % hace diviciones exactas para sacar el resto
                entero = Double.toString(cambio);  //1234,0
                txt_resultado.setText(entero.substring(0, entero.length() - 2)); //1234.0 - 2 = 1234
            } else {
                txt_resultado.setText(Double.toString(cambio));
            }
        } catch (Exception e) {

        }
    }//GEN-LAST:event_btn_signoActionPerformed

    private void btn_porcentajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_porcentajeActionPerformed
        try {
            numeros[0] = Double.parseDouble(txt_resultado.getText());
            txt_resultado.setText("");
            porcentaje = true;
            decimal = true;
        } catch (Exception e) {

        }
    }//GEN-LAST:event_btn_porcentajeActionPerformed


    private void btn_7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_7ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "7");
    }//GEN-LAST:event_btn_7ActionPerformed

    private void btn_8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_8ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "8");
    }//GEN-LAST:event_btn_8ActionPerformed

    private void btn_9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_9ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "9");
    }//GEN-LAST:event_btn_9ActionPerformed

    private void btn_4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_4ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "4");
    }//GEN-LAST:event_btn_4ActionPerformed

    private void btn_5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_5ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "5");
    }//GEN-LAST:event_btn_5ActionPerformed

    private void btn_6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_6ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "6");
    }//GEN-LAST:event_btn_6ActionPerformed

    private void btn_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_1ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "1");
    }//GEN-LAST:event_btn_1ActionPerformed

    private void btn_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_2ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "2");
    }//GEN-LAST:event_btn_2ActionPerformed

    private void btn_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_3ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "3");
    }//GEN-LAST:event_btn_3ActionPerformed

    private void btn_0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_0ActionPerformed
        txt_resultado.setText(txt_resultado.getText() + "0");
    }//GEN-LAST:event_btn_0ActionPerformed

    private void btn_puntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_puntoActionPerformed
        if (decimal == true) {
            txt_resultado.setText(txt_resultado.getText() + ".");
            decimal = false;
        }
    }//GEN-LAST:event_btn_puntoActionPerformed


    private void btn_diviActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_diviActionPerformed
        try {
            numeros[0] = Double.parseDouble(txt_resultado.getText());
            txt_resultado.setText("");
            decimal = true;
            divicion = true;
        } catch (Exception e) {

        }

    }//GEN-LAST:event_btn_diviActionPerformed

    private void btn_multActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_multActionPerformed
        try {
            numeros[0] = Double.parseDouble(txt_resultado.getText());
            txt_resultado.setText("");
            decimal = true;
            multiplicacion = true;
        } catch (Exception e) {

        }

    }//GEN-LAST:event_btn_multActionPerformed

    private void btn_restaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_restaActionPerformed
        try {
            numeros[0] = Double.parseDouble(txt_resultado.getText());
            txt_resultado.setText("");
            decimal = true;
            resta = true;
        } catch (Exception e) {

        }

    }//GEN-LAST:event_btn_restaActionPerformed

    private void btn_sumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_sumaActionPerformed

        try {
            numeros[0] = Double.parseDouble(txt_resultado.getText());
            txt_resultado.setText("");
            suma = true;
            decimal = true;
        } catch (Exception e) {

        }

    }//GEN-LAST:event_btn_sumaActionPerformed

    private void btn_igualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_igualActionPerformed
        double resultado = 0;
        String entero;
        try {
            numeros[1] = Double.parseDouble(txt_resultado.getText());

            if (divicion == true) {
                if (numeros[1] == 0.0) {
                    txt_resultado.setText("Error");
                } else {
                    resultado = numeros[0] / numeros[1];
                    if (resultado % 1 == 0) {
                        entero = Double.toString(resultado);
                        txt_resultado.setText(entero.substring(0, entero.length() - 2)); // length
                    } else {
                        txt_resultado.setText(Double.toString(resultado));
                    }
                }
            } else if (multiplicacion == true) {
                resultado = numeros[0] * numeros[1];
                if (resultado % 1 == 0) {
                    entero = Double.toString(resultado);
                    txt_resultado.setText(entero.substring(0, entero.length() - 2));
                } else {
                    txt_resultado.setText(Double.toString(resultado));
                }
            } else if (resta == true) {
                resultado = numeros[0] - numeros[1];
                if (resultado % 1 == 0) {
                    entero = Double.toString(resultado);
                    txt_resultado.setText(entero.substring(0, entero.length() - 2));
                } else {
                    txt_resultado.setText(Double.toString(resultado));
                }
            } else if (suma == true) {
                resultado = numeros[0] + numeros[1];
                if (resultado % 1 == 0) {
                    entero = Double.toString(resultado);
                    txt_resultado.setText(entero.substring(0, entero.length() - 2));
                } else {
                    txt_resultado.setText(Double.toString(resultado));
                }
            } else if (porcentaje == true) {
                resultado = numeros[1] * (numeros[0] / 100.0);
                if (resultado % 1 == 0) {
                    entero = Double.toString(resultado);
                    txt_resultado.setText(entero.substring(0, entero.length() - 2));
                } else {
                    txt_resultado.setText(Double.toString(resultado));
                }
            }

        } catch (Exception e) {

        }

        decimal = true;
        divicion = false;
        multiplicacion = false;
        suma = false;
        resta = false;
        porcentaje = false;

        numeros[0] = 0;
        numeros[1] = 0;
    }//GEN-LAST:event_btn_igualActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(calculadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new calculadora().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_0;
    public javax.swing.JButton btn_1;
    public javax.swing.JButton btn_2;
    public javax.swing.JButton btn_3;
    public javax.swing.JButton btn_4;
    public javax.swing.JButton btn_5;
    public javax.swing.JButton btn_6;
    public javax.swing.JButton btn_7;
    public javax.swing.JButton btn_8;
    public javax.swing.JButton btn_9;
    public javax.swing.JButton btn_borrar;
    public javax.swing.JButton btn_divi;
    public javax.swing.JButton btn_igual;
    public javax.swing.JButton btn_mult;
    public javax.swing.JButton btn_porcentaje;
    public javax.swing.JButton btn_punto;
    public javax.swing.JButton btn_resta;
    public javax.swing.JButton btn_signo;
    public javax.swing.JButton btn_suma;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanel3;
    public javax.swing.JLabel txt_resultado;
    // End of variables declaration//GEN-END:variables

    public boolean setVisible() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
