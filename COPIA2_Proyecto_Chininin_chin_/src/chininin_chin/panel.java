package chininin_chin;

import presentacion.Panel;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import structu.AddContend;
import structu.Arbol;
import structu.Nodo;

public class panel extends javax.swing.JFrame {

    Arbol<Productos> listpro = new Arbol<>();

    DefaultTableModel modelo;
    int indice;

    private Object matriz[][];
    private int contador = 0;

    public panel() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        minimizar = new javax.swing.JButton();
        ampliar = new javax.swing.JButton();
        cerrar = new javax.swing.JButton();
        Productos = new javax.swing.JTabbedPane();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel11 = new javax.swing.JPanel();
        txt_precio_pan3 = new javax.swing.JTextField();
        txt_aceituna = new javax.swing.JTextField();
        imagen_aceituna = new javax.swing.JLabel();
        spiner_vega = new javax.swing.JSpinner();
        txt_precio_vega = new javax.swing.JTextField();
        txt_atun_vega = new javax.swing.JTextField();
        imagen_atun_cartinal = new javax.swing.JLabel();
        spiner_cardinal = new javax.swing.JSpinner();
        txt_precio_cardinal = new javax.swing.JTextField();
        txt_cardinal = new javax.swing.JTextField();
        spiner_campiñones = new javax.swing.JSpinner();
        txt_precio_champiñones = new javax.swing.JTextField();
        txt_champiñones = new javax.swing.JTextField();
        imagen_sardinas = new javax.swing.JLabel();
        spiner_aceituna = new javax.swing.JSpinner();
        imagen_atun_uno = new javax.swing.JLabel();
        spiner_sardinas = new javax.swing.JSpinner();
        txt_precio_sardinas = new javax.swing.JTextField();
        txt_sardinas_hatch = new javax.swing.JTextField();
        imagen_sardinas_hatch = new javax.swing.JLabel();
        txt_precio_chinelos = new javax.swing.JTextField();
        txt_chilenos = new javax.swing.JTextField();
        imagen_chilenos = new javax.swing.JLabel();
        spiner_chilenos = new javax.swing.JSpinner();
        spiner_timonel = new javax.swing.JSpinner();
        txt_precio_timonel = new javax.swing.JTextField();
        txt_timonel = new javax.swing.JTextField();
        imagen_durazno_timonel = new javax.swing.JLabel();
        spiner_africa = new javax.swing.JSpinner();
        txt_precio_africa = new javax.swing.JTextField();
        txt_africa = new javax.swing.JTextField();
        imagen_durazno_arica = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txt_donofrio = new javax.swing.JTextField();
        txt_precio_donofrio = new javax.swing.JTextField();
        spiner_donofrio = new javax.swing.JSpinner();
        jLabel20 = new javax.swing.JLabel();
        txt_chotox = new javax.swing.JTextField();
        txt_precio_chotox = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_flor = new javax.swing.JTextField();
        txt_precio_flor = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txt_winters = new javax.swing.JTextField();
        txt_precio_winters = new javax.swing.JTextField();
        spiner_chotox = new javax.swing.JSpinner();
        spiner_flor = new javax.swing.JSpinner();
        spiner_winters = new javax.swing.JSpinner();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        txt_gloria = new javax.swing.JTextField();
        txt_santiago = new javax.swing.JTextField();
        txt_bimbo = new javax.swing.JTextField();
        txt_precio_bimbo = new javax.swing.JTextField();
        txt_precio_santiago = new javax.swing.JTextField();
        txt_precio_gloria = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txt_natale = new javax.swing.JTextField();
        txt_precio_natale = new javax.swing.JTextField();
        spiner_bimbo = new javax.swing.JSpinner();
        spiner_santiago = new javax.swing.JSpinner();
        spiner_gloria = new javax.swing.JSpinner();
        spiner_buon_natale = new javax.swing.JSpinner();
        jScrollPane1 = new javax.swing.JScrollPane();
        jScrollPane7 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        txt_casserita = new javax.swing.JTextField();
        txt_precio_casserita = new javax.swing.JTextField();
        spiner_casserita = new javax.swing.JSpinner();
        jLabel31 = new javax.swing.JLabel();
        txt_aro = new javax.swing.JTextField();
        txt_precio_aro = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        txt_aro2 = new javax.swing.JTextField();
        txt_precio_aro2 = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        txt_menestra = new javax.swing.JTextField();
        txt_precio_menestra = new javax.swing.JTextField();
        spiner_aro = new javax.swing.JSpinner();
        spiner_aro2 = new javax.swing.JSpinner();
        spiner_menestra = new javax.swing.JSpinner();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txt_menestra2 = new javax.swing.JTextField();
        txt_azucar_casa = new javax.swing.JTextField();
        txt_azucar = new javax.swing.JTextField();
        txt_precio_azucar = new javax.swing.JTextField();
        txt_precio_azucar_casa = new javax.swing.JTextField();
        txt_precio_menestra2 = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        txt_cartavio = new javax.swing.JTextField();
        txt_precio_cartavio = new javax.swing.JTextField();
        spiner_azucar = new javax.swing.JSpinner();
        spiner_azucar_casa = new javax.swing.JSpinner();
        spiner_menestra2 = new javax.swing.JSpinner();
        spiner_cartavio = new javax.swing.JSpinner();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        txt_leche_celeste = new javax.swing.JTextField();
        txt_precio_leche_celeste = new javax.swing.JTextField();
        spiner_leche_celeste = new javax.swing.JSpinner();
        jLabel39 = new javax.swing.JLabel();
        txt_leche_amarilla = new javax.swing.JTextField();
        txt_precio_leche_amarilla = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        txt_sin_lactosa = new javax.swing.JTextField();
        txt_precio_sin_lactosa = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        txt_leche_roja = new javax.swing.JTextField();
        txt_precio_leche_roja = new javax.swing.JTextField();
        spiner_leche_amarilla = new javax.swing.JSpinner();
        spiner_sin_lactosa = new javax.swing.JSpinner();
        spiner_leche_roja = new javax.swing.JSpinner();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        txt_leche_live = new javax.swing.JTextField();
        txt_leche_vita = new javax.swing.JTextField();
        txt_leche_azul = new javax.swing.JTextField();
        txt_precio_leche_azul = new javax.swing.JTextField();
        txt_precio_leche_vita = new javax.swing.JTextField();
        txt_precio_leche_live = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        txt_leche_nestle = new javax.swing.JTextField();
        txt_precio_leche_nestle = new javax.swing.JTextField();
        spiner_leche_azul = new javax.swing.JSpinner();
        spiner_leche_vita = new javax.swing.JSpinner();
        spiner_leche_live = new javax.swing.JSpinner();
        spiner_leche_nestle = new javax.swing.JSpinner();
        jScrollPane8 = new javax.swing.JScrollPane();
        jPanel15 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        txt_colgate = new javax.swing.JTextField();
        txt_precio_colgate = new javax.swing.JTextField();
        spiner_colgate = new javax.swing.JSpinner();
        jLabel47 = new javax.swing.JLabel();
        txt_pack_colinos = new javax.swing.JTextField();
        txt_precio_pak_colinos = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        txt_jabon_fresa = new javax.swing.JTextField();
        txt_precio_jabon_fresa = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        txt_jabon_platano = new javax.swing.JTextField();
        txt_precio_jabon_platano = new javax.swing.JTextField();
        spiner_pack_colinos = new javax.swing.JSpinner();
        spiner_jabon_fresa = new javax.swing.JSpinner();
        spiner_jabon_platano = new javax.swing.JSpinner();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        txt_dove_mujer = new javax.swing.JTextField();
        txt_cotex = new javax.swing.JTextField();
        txt_nosotras = new javax.swing.JTextField();
        txt_precio_nosotras = new javax.swing.JTextField();
        txt_precio_cotex = new javax.swing.JTextField();
        txt_precio_dove_mujer = new javax.swing.JTextField();
        jLabel53 = new javax.swing.JLabel();
        txt_rexona_mujer = new javax.swing.JTextField();
        txt_precio_rexona_hombre = new javax.swing.JTextField();
        spiner_nosotras = new javax.swing.JSpinner();
        spiner_cotex = new javax.swing.JSpinner();
        spiner_dove_mujer = new javax.swing.JSpinner();
        espiner_rexona_hombre = new javax.swing.JSpinner();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel12 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        txt_laive = new javax.swing.JTextField();
        txt_precio_laive = new javax.swing.JTextField();
        spiner_laive = new javax.swing.JSpinner();
        jLabel55 = new javax.swing.JLabel();
        txt_tinto = new javax.swing.JTextField();
        txt_precio_tinto = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        txt_pack_gaseosas = new javax.swing.JTextField();
        txt_precio_pack_gaseosas = new javax.swing.JTextField();
        jLabel57 = new javax.swing.JLabel();
        txt_kr = new javax.swing.JTextField();
        txt_precio_kr = new javax.swing.JTextField();
        spiner_tinto = new javax.swing.JSpinner();
        spiner_pack_gaseosas = new javax.swing.JSpinner();
        spiner_kr = new javax.swing.JSpinner();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        txt_pulp_2 = new javax.swing.JTextField();
        txt_yogurt_fresa = new javax.swing.JTextField();
        txt_yogurt_durazno = new javax.swing.JTextField();
        txt_precio_yogurt_durazno = new javax.swing.JTextField();
        txt_precio_yogurt_fresa = new javax.swing.JTextField();
        txt_precio_pulp_2 = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        txt_pulp_litro_medio = new javax.swing.JTextField();
        txt_precio_pulp_litro_medio = new javax.swing.JTextField();
        spiner_yogurt_durazno = new javax.swing.JSpinner();
        spiner_yogurt_fresa = new javax.swing.JSpinner();
        spiner_pulp_2 = new javax.swing.JSpinner();
        spiner_pul_litro_medio = new javax.swing.JSpinner();
        jPanel16 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btn_desc = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tabla2 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        txt_subtotal = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_total = new javax.swing.JTextField();
        txt_igv = new javax.swing.JTextField();
        txt_descuento = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jToggleButton1 = new javax.swing.JToggleButton();
        btn_desc1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(204, 204, 255));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBackground(new java.awt.Color(155, 156, 160));
        jPanel3.setPreferredSize(new java.awt.Dimension(88, 700));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel3.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, -1, -1));
        jPanel3.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 90, 0));
        jPanel3.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 90, 0));
        jPanel3.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 260, 90, 10));

        jSeparator5.setForeground(new java.awt.Color(255, 255, 255));
        jPanel3.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 170, 90, 30));
        jPanel3.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 350, 90, 10));
        jPanel3.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, -1, -1));

        jSeparator8.setForeground(new java.awt.Color(255, 255, 255));
        jPanel3.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 90, 10));

        jButton7.setBackground(new java.awt.Color(155, 156, 160));
        jButton7.setForeground(new java.awt.Color(155, 156, 160));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/logo.png"))); // NOI18N
        jButton7.setBorderPainted(false);
        jButton7.setContentAreaFilled(false);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 71, 52));

        jButton8.setBackground(new java.awt.Color(155, 156, 160));
        jButton8.setForeground(new java.awt.Color(155, 156, 160));
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/empleado.png"))); // NOI18N
        jButton8.setBorderPainted(false);
        jButton8.setContentAreaFilled(false);
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton8MouseClicked(evt);
            }
        });
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 71, 52));

        jButton9.setBackground(new java.awt.Color(155, 156, 160));
        jButton9.setForeground(new java.awt.Color(155, 156, 160));
        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/calculadora.png"))); // NOI18N
        jButton9.setBorderPainted(false);
        jButton9.setContentAreaFilled(false);
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton9MouseClicked(evt);
            }
        });
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 71, 52));

        jButton10.setBackground(new java.awt.Color(155, 156, 160));
        jButton10.setForeground(new java.awt.Color(155, 156, 160));
        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/carrito.png"))); // NOI18N
        jButton10.setBorderPainted(false);
        jButton10.setContentAreaFilled(false);
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 71, 52));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, 700));

        minimizar.setBackground(new java.awt.Color(155, 156, 160));
        minimizar.setForeground(new java.awt.Color(155, 156, 160));
        minimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/minimizar.png"))); // NOI18N
        minimizar.setContentAreaFilled(false);
        minimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizarActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 0, 40, 30));

        ampliar.setBackground(new java.awt.Color(155, 156, 160));
        ampliar.setForeground(new java.awt.Color(155, 156, 160));
        ampliar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/ampliar.png"))); // NOI18N
        ampliar.setContentAreaFilled(false);
        ampliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ampliarActionPerformed(evt);
            }
        });
        getContentPane().add(ampliar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1290, 0, 40, 30));

        cerrar.setBackground(new java.awt.Color(155, 156, 160));
        cerrar.setForeground(new java.awt.Color(155, 156, 160));
        cerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/chininin_chin/imagenes/cerrar.png"))); // NOI18N
        cerrar.setContentAreaFilled(false);
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });
        getContentPane().add(cerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1330, 0, 40, 30));

        jPanel11.setBackground(new java.awt.Color(210, 211, 215));
        jPanel11.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_precio_pan3.setEditable(false);
        txt_precio_pan3.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_pan3.setText("3.00");
        txt_precio_pan3.setBorder(null);
        txt_precio_pan3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_pan3ActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_pan3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 120, -1));

        txt_aceituna.setEditable(false);
        txt_aceituna.setBackground(new java.awt.Color(210, 211, 215));
        txt_aceituna.setText("ACEITUNA");
        txt_aceituna.setBorder(null);
        txt_aceituna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_aceitunaActionPerformed(evt);
            }
        });
        jPanel11.add(txt_aceituna, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 100, 20));

        imagen_aceituna.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/aceitunas_adobespark_adobespark.png"))); // NOI18N
        imagen_aceituna.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_aceitunaMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_aceituna, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 100, 110));

        spiner_vega.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_vegaStateChanged(evt);
            }
        });
        jPanel11.add(spiner_vega, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 180, 120, -1));

        txt_precio_vega.setEditable(false);
        txt_precio_vega.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_vega.setText("3.00");
        txt_precio_vega.setBorder(null);
        txt_precio_vega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_vegaActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_vega, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 140, 120, -1));

        txt_atun_vega.setEditable(false);
        txt_atun_vega.setBackground(new java.awt.Color(210, 211, 215));
        txt_atun_vega.setText("ATÚN VEGA");
        txt_atun_vega.setBorder(null);
        txt_atun_vega.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_atun_vegaActionPerformed(evt);
            }
        });
        jPanel11.add(txt_atun_vega, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 110, 120, 20));

        imagen_atun_cartinal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/atun_adobespark_adobespark.png"))); // NOI18N
        imagen_atun_cartinal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_atun_cartinalMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_atun_cartinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 20, 100, 80));

        spiner_cardinal.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_cardinalStateChanged(evt);
            }
        });
        jPanel11.add(spiner_cardinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 180, 120, -1));

        txt_precio_cardinal.setEditable(false);
        txt_precio_cardinal.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_cardinal.setText("3.50");
        txt_precio_cardinal.setBorder(null);
        txt_precio_cardinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_cardinalActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_cardinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 140, 120, -1));

        txt_cardinal.setEditable(false);
        txt_cardinal.setBackground(new java.awt.Color(210, 211, 215));
        txt_cardinal.setText("ATÚN CARDINAL");
        txt_cardinal.setBorder(null);
        txt_cardinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cardinalActionPerformed(evt);
            }
        });
        jPanel11.add(txt_cardinal, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 110, 120, 20));

        spiner_campiñones.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_campiñonesStateChanged(evt);
            }
        });
        jPanel11.add(spiner_campiñones, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 180, 120, -1));

        txt_precio_champiñones.setEditable(false);
        txt_precio_champiñones.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_champiñones.setText("3.00");
        txt_precio_champiñones.setBorder(null);
        txt_precio_champiñones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_champiñonesActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_champiñones, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 140, 120, -1));

        txt_champiñones.setEditable(false);
        txt_champiñones.setBackground(new java.awt.Color(210, 211, 215));
        txt_champiñones.setText("CHAMPIÑONES");
        txt_champiñones.setBorder(null);
        txt_champiñones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_champiñonesActionPerformed(evt);
            }
        });
        jPanel11.add(txt_champiñones, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 110, 120, 20));

        imagen_sardinas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/champiñones_adobespark_adobespark.png"))); // NOI18N
        imagen_sardinas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_sardinasMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_sardinas, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 10, 110, 110));

        spiner_aceituna.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_aceitunaStateChanged(evt);
            }
        });
        jPanel11.add(spiner_aceituna, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 120, -1));

        imagen_atun_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/sardinas_adobespark_adobespark.png"))); // NOI18N
        imagen_atun_uno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_atun_unoMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_atun_uno, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, 100, 110));

        spiner_sardinas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_sardinasStateChanged(evt);
            }
        });
        jPanel11.add(spiner_sardinas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 470, 120, -1));

        txt_precio_sardinas.setEditable(false);
        txt_precio_sardinas.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_sardinas.setText("10.50");
        txt_precio_sardinas.setBorder(null);
        txt_precio_sardinas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_sardinasActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_sardinas, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 440, 120, -1));

        txt_sardinas_hatch.setEditable(false);
        txt_sardinas_hatch.setBackground(new java.awt.Color(210, 211, 215));
        txt_sardinas_hatch.setText("SARDINAS HATCH");
        txt_sardinas_hatch.setBorder(null);
        txt_sardinas_hatch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_sardinas_hatchActionPerformed(evt);
            }
        });
        jPanel11.add(txt_sardinas_hatch, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, 120, -1));

        imagen_sardinas_hatch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/chilees_adobespark(1)_adobespark.png"))); // NOI18N
        imagen_sardinas_hatch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_sardinas_hatchMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_sardinas_hatch, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, 110, 110));

        txt_precio_chinelos.setEditable(false);
        txt_precio_chinelos.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_chinelos.setText("4.50");
        txt_precio_chinelos.setBorder(null);
        txt_precio_chinelos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_chinelosActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_chinelos, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 440, 120, -1));

        txt_chilenos.setEditable(false);
        txt_chilenos.setBackground(new java.awt.Color(210, 211, 215));
        txt_chilenos.setText("AJÍES CHILENOS");
        txt_chilenos.setBorder(null);
        txt_chilenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_chilenosActionPerformed(evt);
            }
        });
        jPanel11.add(txt_chilenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 410, 120, -1));

        imagen_chilenos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/chiles 1_adobespark_adobespark.png"))); // NOI18N
        imagen_chilenos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_chilenosMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_chilenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 300, 100, 100));

        spiner_chilenos.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_chilenosStateChanged(evt);
            }
        });
        jPanel11.add(spiner_chilenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 470, 120, -1));

        spiner_timonel.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_timonelStateChanged(evt);
            }
        });
        jPanel11.add(spiner_timonel, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 470, 120, -1));

        txt_precio_timonel.setEditable(false);
        txt_precio_timonel.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_timonel.setText("12.00");
        txt_precio_timonel.setBorder(null);
        txt_precio_timonel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_timonelActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_timonel, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 440, 120, -1));

        txt_timonel.setEditable(false);
        txt_timonel.setBackground(new java.awt.Color(210, 211, 215));
        txt_timonel.setText("DURAZNO TIMONEL");
        txt_timonel.setBorder(null);
        txt_timonel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_timonelActionPerformed(evt);
            }
        });
        jPanel11.add(txt_timonel, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 410, 120, -1));

        imagen_durazno_timonel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/durazno4_adobespark(1)_adobespark.png"))); // NOI18N
        imagen_durazno_timonel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_durazno_timonelMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_durazno_timonel, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 290, 80, 110));

        spiner_africa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_africaStateChanged(evt);
            }
        });
        jPanel11.add(spiner_africa, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 470, 120, -1));

        txt_precio_africa.setEditable(false);
        txt_precio_africa.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_africa.setText("11.00");
        txt_precio_africa.setBorder(null);
        txt_precio_africa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_africaActionPerformed(evt);
            }
        });
        jPanel11.add(txt_precio_africa, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 440, 120, -1));

        txt_africa.setEditable(false);
        txt_africa.setBackground(new java.awt.Color(210, 211, 215));
        txt_africa.setText("DURZANO ARICA");
        txt_africa.setBorder(null);
        txt_africa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_africaActionPerformed(evt);
            }
        });
        jPanel11.add(txt_africa, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 410, 120, -1));

        imagen_durazno_arica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/durazno_adobespark_adobespark.png"))); // NOI18N
        imagen_durazno_arica.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imagen_durazno_aricaMouseClicked(evt);
            }
        });
        jPanel11.add(imagen_durazno_arica, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 290, 80, 110));

        jScrollPane5.setViewportView(jPanel11);

        Productos.addTab("ENLATADOS", jScrollPane5);

        jPanel2.setBackground(new java.awt.Color(210, 211, 215));
        jPanel2.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton1_adobespark_adobespark.png"))); // NOI18N
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, 100, 80));

        txt_donofrio.setEditable(false);
        txt_donofrio.setBackground(new java.awt.Color(210, 211, 215));
        txt_donofrio.setText("PANETON DONOFRIO");
        txt_donofrio.setBorder(null);
        txt_donofrio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_donofrioActionPerformed(evt);
            }
        });
        jPanel2.add(txt_donofrio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 130, -1));

        txt_precio_donofrio.setEditable(false);
        txt_precio_donofrio.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_donofrio.setText("30.00");
        txt_precio_donofrio.setBorder(null);
        txt_precio_donofrio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_donofrioActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_donofrio, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 120, -1));

        spiner_donofrio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_donofrioStateChanged(evt);
            }
        });
        jPanel2.add(spiner_donofrio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 120, -1));

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton2_adobespark_adobespark.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 30, 80, 100));

        txt_chotox.setEditable(false);
        txt_chotox.setBackground(new java.awt.Color(210, 211, 215));
        txt_chotox.setText("PANETON CHOCOTOX");
        txt_chotox.setBorder(null);
        txt_chotox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_chotoxActionPerformed(evt);
            }
        });
        jPanel2.add(txt_chotox, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 120, 130, -1));

        txt_precio_chotox.setEditable(false);
        txt_precio_chotox.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_chotox.setText("50.00");
        txt_precio_chotox.setBorder(null);
        txt_precio_chotox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_chotoxActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_chotox, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, 120, -1));

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton3_adobespark_adobespark.png"))); // NOI18N
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 30, 100, 90));

        txt_flor.setEditable(false);
        txt_flor.setBackground(new java.awt.Color(210, 211, 215));
        txt_flor.setText("PANETON FLOR");
        txt_flor.setBorder(null);
        txt_flor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_florActionPerformed(evt);
            }
        });
        jPanel2.add(txt_flor, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 120, 120, -1));

        txt_precio_flor.setEditable(false);
        txt_precio_flor.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_flor.setText("20.00");
        txt_precio_flor.setBorder(null);
        txt_precio_flor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_florActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_flor, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, 120, -1));

        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton 6_adobespark_adobespark.png"))); // NOI18N
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 10, 90, 110));

        txt_winters.setEditable(false);
        txt_winters.setBackground(new java.awt.Color(210, 211, 215));
        txt_winters.setText("PANETON WINTERS");
        txt_winters.setBorder(null);
        txt_winters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_wintersActionPerformed(evt);
            }
        });
        jPanel2.add(txt_winters, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 120, 120, -1));

        txt_precio_winters.setEditable(false);
        txt_precio_winters.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_winters.setText("28.00");
        txt_precio_winters.setBorder(null);
        txt_precio_winters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_wintersActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_winters, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 150, 120, -1));

        spiner_chotox.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_chotoxStateChanged(evt);
            }
        });
        jPanel2.add(spiner_chotox, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 190, 120, -1));

        spiner_flor.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_florStateChanged(evt);
            }
        });
        jPanel2.add(spiner_flor, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 190, 120, -1));

        spiner_winters.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_wintersStateChanged(evt);
            }
        });
        jPanel2.add(spiner_winters, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 190, 120, -1));

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/panetones4_adobespark_adobespark.png"))); // NOI18N
        jLabel26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel26MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 290, 80, 110));

        jLabel27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton 11_adobespark_adobespark.png"))); // NOI18N
        jLabel27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel27MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 300, 80, 110));

        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton 9_adobespark_adobespark.png"))); // NOI18N
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 310, 100, 100));

        txt_gloria.setEditable(false);
        txt_gloria.setBackground(new java.awt.Color(210, 211, 215));
        txt_gloria.setText("PANETON GLORIA");
        txt_gloria.setBorder(null);
        txt_gloria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_gloriaActionPerformed(evt);
            }
        });
        jPanel2.add(txt_gloria, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 410, 120, -1));

        txt_santiago.setEditable(false);
        txt_santiago.setBackground(new java.awt.Color(210, 211, 215));
        txt_santiago.setText("PANETON SANTIAGO");
        txt_santiago.setBorder(null);
        txt_santiago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_santiagoActionPerformed(evt);
            }
        });
        jPanel2.add(txt_santiago, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 410, 120, -1));

        txt_bimbo.setEditable(false);
        txt_bimbo.setBackground(new java.awt.Color(210, 211, 215));
        txt_bimbo.setText("PANETON BIMBO");
        txt_bimbo.setBorder(null);
        txt_bimbo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_bimboActionPerformed(evt);
            }
        });
        jPanel2.add(txt_bimbo, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 410, 120, -1));

        txt_precio_bimbo.setEditable(false);
        txt_precio_bimbo.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_bimbo.setText("30.00");
        txt_precio_bimbo.setBorder(null);
        txt_precio_bimbo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_bimboActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_bimbo, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 440, 120, -1));

        txt_precio_santiago.setEditable(false);
        txt_precio_santiago.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_santiago.setText("35.00");
        txt_precio_santiago.setBorder(null);
        txt_precio_santiago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_santiagoActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_santiago, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 440, 120, -1));

        txt_precio_gloria.setEditable(false);
        txt_precio_gloria.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_gloria.setText("40.00");
        txt_precio_gloria.setBorder(null);
        txt_precio_gloria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_gloriaActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_gloria, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 440, 120, -1));

        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/paneton 8_adobespark_adobespark.png"))); // NOI18N
        jLabel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel29MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 300, 110, 110));

        txt_natale.setEditable(false);
        txt_natale.setBackground(new java.awt.Color(210, 211, 215));
        txt_natale.setText("PANETON BUON NATALE");
        txt_natale.setBorder(null);
        txt_natale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nataleActionPerformed(evt);
            }
        });
        jPanel2.add(txt_natale, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, 140, -1));

        txt_precio_natale.setEditable(false);
        txt_precio_natale.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_natale.setText("30.00");
        txt_precio_natale.setBorder(null);
        txt_precio_natale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_nataleActionPerformed(evt);
            }
        });
        jPanel2.add(txt_precio_natale, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 440, 120, -1));

        spiner_bimbo.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_bimboStateChanged(evt);
            }
        });
        jPanel2.add(spiner_bimbo, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 480, 120, -1));

        spiner_santiago.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_santiagoStateChanged(evt);
            }
        });
        jPanel2.add(spiner_santiago, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 480, 120, -1));

        spiner_gloria.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_gloriaStateChanged(evt);
            }
        });
        jPanel2.add(spiner_gloria, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 480, 120, -1));

        spiner_buon_natale.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_buon_nataleStateChanged(evt);
            }
        });
        jPanel2.add(spiner_buon_natale, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 480, 120, -1));

        jScrollPane2.setViewportView(jPanel2);

        Productos.addTab("PANETONES", jScrollPane2);

        jPanel4.setBackground(new java.awt.Color(210, 211, 215));
        jPanel4.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/menestra 1_adobespark_adobespark.png"))); // NOI18N
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel30MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 100, 110));

        txt_casserita.setEditable(false);
        txt_casserita.setBackground(new java.awt.Color(210, 211, 215));
        txt_casserita.setText("ARROZ CASSERITA");
        txt_casserita.setBorder(null);
        txt_casserita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_casseritaActionPerformed(evt);
            }
        });
        jPanel4.add(txt_casserita, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 120, -1));

        txt_precio_casserita.setEditable(false);
        txt_precio_casserita.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_casserita.setText("120.00");
        txt_precio_casserita.setBorder(null);
        txt_precio_casserita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_casseritaActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_casserita, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 150, 120, -1));

        spiner_casserita.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_casseritaStateChanged(evt);
            }
        });
        jPanel4.add(spiner_casserita, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 120, -1));

        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/aro arroz - 15 soles_adobespark.png"))); // NOI18N
        jLabel31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel31MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 80, 110));

        txt_aro.setEditable(false);
        txt_aro.setBackground(new java.awt.Color(210, 211, 215));
        txt_aro.setText("ARROZ ARO DE 20 KG");
        txt_aro.setBorder(null);
        txt_aro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_aroActionPerformed(evt);
            }
        });
        jPanel4.add(txt_aro, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 120, 120, -1));

        txt_precio_aro.setEditable(false);
        txt_precio_aro.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_aro.setText("90.00");
        txt_precio_aro.setBorder(null);
        txt_precio_aro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_aroActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_aro, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 150, 120, -1));

        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/azucar 2_adobespark_adobespark.png"))); // NOI18N
        jLabel32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel32MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 300, 100, 100));

        txt_aro2.setEditable(false);
        txt_aro2.setBackground(new java.awt.Color(210, 211, 215));
        txt_aro2.setText("ARROZ ARO");
        txt_aro2.setBorder(null);
        txt_aro2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_aro2ActionPerformed(evt);
            }
        });
        jPanel4.add(txt_aro2, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 120, 120, -1));

        txt_precio_aro2.setEditable(false);
        txt_precio_aro2.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_aro2.setText("110.00");
        txt_precio_aro2.setBorder(null);
        txt_precio_aro2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_aro2ActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_aro2, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 150, 120, -1));

        jLabel33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/aro arroz- 120 _adobespark.png"))); // NOI18N
        jLabel33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel33MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 10, 110, 110));

        txt_menestra.setEditable(false);
        txt_menestra.setBackground(new java.awt.Color(210, 211, 215));
        txt_menestra.setText("MENESTRA 4 KG");
        txt_menestra.setBorder(null);
        txt_menestra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_menestraActionPerformed(evt);
            }
        });
        jPanel4.add(txt_menestra, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 120, 120, -1));

        txt_precio_menestra.setEditable(false);
        txt_precio_menestra.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_menestra.setText("70.00");
        txt_precio_menestra.setBorder(null);
        txt_precio_menestra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_menestraActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_menestra, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 150, 120, -1));

        spiner_aro.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_aroStateChanged(evt);
            }
        });
        jPanel4.add(spiner_aro, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 180, 120, -1));

        spiner_aro2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_aro2StateChanged(evt);
            }
        });
        jPanel4.add(spiner_aro2, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 180, 120, -1));

        spiner_menestra.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_menestraStateChanged(evt);
            }
        });
        jPanel4.add(spiner_menestra, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 180, 120, -1));

        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/azucar 1_adobespark_adobespark.png"))); // NOI18N
        jLabel34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel34MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 300, 80, 110));

        jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/menestra 5_adobespark_adobespark.png"))); // NOI18N
        jLabel35.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel35MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 10, 80, 100));

        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/arroz caserita 25 kg- 67 soles_adobespark.png"))); // NOI18N
        jLabel36.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel36MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 0, 100, 160));

        txt_menestra2.setEditable(false);
        txt_menestra2.setBackground(new java.awt.Color(210, 211, 215));
        txt_menestra2.setText("MENESTRA ");
        txt_menestra2.setBorder(null);
        txt_menestra2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_menestra2ActionPerformed(evt);
            }
        });
        jPanel4.add(txt_menestra2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 410, 120, -1));

        txt_azucar_casa.setEditable(false);
        txt_azucar_casa.setBackground(new java.awt.Color(210, 211, 215));
        txt_azucar_casa.setText("AZUCAR CASAGRANDE");
        txt_azucar_casa.setBorder(null);
        txt_azucar_casa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_azucar_casaActionPerformed(evt);
            }
        });
        jPanel4.add(txt_azucar_casa, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 410, 120, -1));

        txt_azucar.setEditable(false);
        txt_azucar.setBackground(new java.awt.Color(210, 211, 215));
        txt_azucar.setText("AZUCAR");
        txt_azucar.setBorder(null);
        txt_azucar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_azucarActionPerformed(evt);
            }
        });
        jPanel4.add(txt_azucar, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 410, 120, -1));

        txt_precio_azucar.setEditable(false);
        txt_precio_azucar.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_azucar.setText("120.00");
        txt_precio_azucar.setBorder(null);
        txt_precio_azucar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_azucarActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_azucar, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 440, 120, -1));

        txt_precio_azucar_casa.setEditable(false);
        txt_precio_azucar_casa.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_azucar_casa.setText("130.00");
        txt_precio_azucar_casa.setBorder(null);
        txt_precio_azucar_casa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_azucar_casaActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_azucar_casa, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 440, 120, -1));

        txt_precio_menestra2.setEditable(false);
        txt_precio_menestra2.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_menestra2.setText("80.00");
        txt_precio_menestra2.setBorder(null);
        txt_precio_menestra2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_menestra2ActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_menestra2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 440, 120, -1));

        jLabel37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/menestra 3_adobespark_adobespark.png"))); // NOI18N
        jLabel37.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel37MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 310, 110, 110));

        txt_cartavio.setEditable(false);
        txt_cartavio.setBackground(new java.awt.Color(210, 211, 215));
        txt_cartavio.setText("MENESTRA CARTAVIO");
        txt_cartavio.setBorder(null);
        txt_cartavio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cartavioActionPerformed(evt);
            }
        });
        jPanel4.add(txt_cartavio, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 410, 120, -1));

        txt_precio_cartavio.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_cartavio.setText("90.00");
        txt_precio_cartavio.setBorder(null);
        txt_precio_cartavio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_cartavioActionPerformed(evt);
            }
        });
        jPanel4.add(txt_precio_cartavio, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 440, 120, -1));

        spiner_azucar.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_azucarStateChanged(evt);
            }
        });
        jPanel4.add(spiner_azucar, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 470, 120, -1));

        spiner_azucar_casa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_azucar_casaStateChanged(evt);
            }
        });
        jPanel4.add(spiner_azucar_casa, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 470, 120, -1));

        spiner_menestra2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_menestra2StateChanged(evt);
            }
        });
        jPanel4.add(spiner_menestra2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 470, 120, -1));

        spiner_cartavio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_cartavioStateChanged(evt);
            }
        });
        jPanel4.add(spiner_cartavio, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, 120, -1));

        jScrollPane7.setViewportView(jPanel4);

        jScrollPane1.setViewportView(jScrollPane7);

        Productos.addTab("ABARROTES", jScrollPane1);

        jPanel9.setBackground(new java.awt.Color(210, 211, 215));
        jPanel9.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/259806-1_adobespark_adobespark.png"))); // NOI18N
        jLabel38.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel38MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 100, 110));

        txt_leche_celeste.setEditable(false);
        txt_leche_celeste.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_celeste.setText("LECHE GLORIA CELESTE");
        txt_leche_celeste.setBorder(null);
        txt_leche_celeste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_celesteActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_celeste, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 150, -1));

        txt_precio_leche_celeste.setEditable(false);
        txt_precio_leche_celeste.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_celeste.setText("2.80");
        txt_precio_leche_celeste.setBorder(null);
        txt_precio_leche_celeste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_celesteActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_celeste, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 120, -1));

        spiner_leche_celeste.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_celesteStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_celeste, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 120, -1));

        jLabel39.setIcon(new javax.swing.ImageIcon("C:\\Users\\Imanol\\Desktop\\chininin chin\\productos con precios\\prodcutos sin fondo\\productos con tamaño\\leche_gloria_amarilla.png")); // NOI18N
        jLabel39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel39MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 10, 80, 110));

        txt_leche_amarilla.setEditable(false);
        txt_leche_amarilla.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_amarilla.setText("LECHE GLORIA AMARILLA");
        txt_leche_amarilla.setBorder(null);
        txt_leche_amarilla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_amarillaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_amarilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 120, 140, -1));

        txt_precio_leche_amarilla.setEditable(false);
        txt_precio_leche_amarilla.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_amarilla.setText("3.20");
        txt_precio_leche_amarilla.setBorder(null);
        txt_precio_leche_amarilla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_amarillaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_amarilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 150, 120, -1));

        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/Leche pasteurizada_adobespark_adobespark.png"))); // NOI18N
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 20, 100, 90));

        txt_sin_lactosa.setEditable(false);
        txt_sin_lactosa.setBackground(new java.awt.Color(210, 211, 215));
        txt_sin_lactosa.setText("LECHE SIN LACTOSA");
        txt_sin_lactosa.setBorder(null);
        txt_sin_lactosa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_sin_lactosaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_sin_lactosa, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 120, 130, 20));

        txt_precio_sin_lactosa.setEditable(false);
        txt_precio_sin_lactosa.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_sin_lactosa.setText("3.40");
        txt_precio_sin_lactosa.setBorder(null);
        txt_precio_sin_lactosa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_sin_lactosaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_sin_lactosa, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, 120, -1));

        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/descarga (7)_adobespark_adobespark (1).png"))); // NOI18N
        jLabel41.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel41MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 10, 110, 110));

        txt_leche_roja.setEditable(false);
        txt_leche_roja.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_roja.setText("LECHE GLORIA ROJA");
        txt_leche_roja.setBorder(null);
        txt_leche_roja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_rojaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_roja, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 120, 120, -1));

        txt_precio_leche_roja.setEditable(false);
        txt_precio_leche_roja.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_roja.setText("2.50");
        txt_precio_leche_roja.setBorder(null);
        txt_precio_leche_roja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_rojaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_roja, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 150, 120, -1));

        spiner_leche_amarilla.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_amarillaStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_amarilla, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 120, -1));

        spiner_sin_lactosa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_sin_lactosaStateChanged(evt);
            }
        });
        jPanel9.add(spiner_sin_lactosa, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 180, 120, -1));

        spiner_leche_roja.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_rojaStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_roja, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 180, 120, -1));

        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/Leche evaporada_adobespark_adobespark.png"))); // NOI18N
        jLabel42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel42MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 290, 100, 110));

        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/images (3)_adobespark_adobespark.png"))); // NOI18N
        jLabel43.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel43MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 290, 80, 110));

        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/Crema_adobespark_adobespark.png"))); // NOI18N
        jLabel44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel44MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 100, 100));

        txt_leche_live.setEditable(false);
        txt_leche_live.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_live.setText("LECHE LIVE");
        txt_leche_live.setBorder(null);
        txt_leche_live.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_liveActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_live, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 400, 120, -1));

        txt_leche_vita.setEditable(false);
        txt_leche_vita.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_vita.setText("LECHE VITA");
        txt_leche_vita.setBorder(null);
        txt_leche_vita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_vitaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_vita, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 400, 120, -1));

        txt_leche_azul.setEditable(false);
        txt_leche_azul.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_azul.setText("LECHE GLORIA AZUL");
        txt_leche_azul.setBorder(null);
        txt_leche_azul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_azulActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_azul, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 400, 120, -1));

        txt_precio_leche_azul.setEditable(false);
        txt_precio_leche_azul.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_azul.setText("3.00");
        txt_precio_leche_azul.setBorder(null);
        txt_precio_leche_azul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_azulActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_azul, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 430, 120, -1));

        txt_precio_leche_vita.setEditable(false);
        txt_precio_leche_vita.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_vita.setText("1.50");
        txt_precio_leche_vita.setBorder(null);
        txt_precio_leche_vita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_vitaActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_vita, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 430, 120, -1));

        txt_precio_leche_live.setEditable(false);
        txt_precio_leche_live.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_live.setText("1.80");
        txt_precio_leche_live.setBorder(null);
        txt_precio_leche_live.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_liveActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_live, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 430, 120, -1));

        jLabel45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/descarga (7)_adobespark_adobespark.png"))); // NOI18N
        jLabel45.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel45MouseClicked(evt);
            }
        });
        jPanel9.add(jLabel45, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 110, 100));

        txt_leche_nestle.setEditable(false);
        txt_leche_nestle.setBackground(new java.awt.Color(210, 211, 215));
        txt_leche_nestle.setText("LECHE NESTLE");
        txt_leche_nestle.setBorder(null);
        txt_leche_nestle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_leche_nestleActionPerformed(evt);
            }
        });
        jPanel9.add(txt_leche_nestle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, 120, -1));

        txt_precio_leche_nestle.setEditable(false);
        txt_precio_leche_nestle.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_leche_nestle.setText("2.30");
        txt_precio_leche_nestle.setBorder(null);
        txt_precio_leche_nestle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_leche_nestleActionPerformed(evt);
            }
        });
        jPanel9.add(txt_precio_leche_nestle, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 430, 120, -1));

        spiner_leche_azul.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_azulStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_azul, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 470, 120, -1));

        spiner_leche_vita.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_vitaStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_vita, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 470, 120, -1));

        spiner_leche_live.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_liveStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_live, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 470, 120, -1));

        spiner_leche_nestle.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_leche_nestleStateChanged(evt);
            }
        });
        jPanel9.add(spiner_leche_nestle, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 120, -1));

        jScrollPane3.setViewportView(jPanel9);

        Productos.addTab("LACTEOS", jScrollPane3);

        jPanel15.setBackground(new java.awt.Color(210, 211, 215));
        jPanel15.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel15.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/colgate 1_adobespark_adobespark.png"))); // NOI18N
        jLabel46.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel46MouseClicked(evt);
            }
        });
        jPanel15.add(jLabel46, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 100, 80));

        txt_colgate.setEditable(false);
        txt_colgate.setBackground(new java.awt.Color(210, 211, 215));
        txt_colgate.setText("COLGATE TRIPLE ACCIÓN");
        txt_colgate.setBorder(null);
        txt_colgate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_colgateActionPerformed(evt);
            }
        });
        jPanel15.add(txt_colgate, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 160, -1));

        txt_precio_colgate.setEditable(false);
        txt_precio_colgate.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_colgate.setText("2.00");
        txt_precio_colgate.setBorder(null);
        txt_precio_colgate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_colgateActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_colgate, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 120, -1));

        spiner_colgate.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_colgateStateChanged(evt);
            }
        });
        jPanel15.add(spiner_colgate, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 120, -1));

        jLabel47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/colgate 2_adobespark_adobespark.png"))); // NOI18N
        jLabel47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel47MouseClicked(evt);
            }
        });
        jPanel15.add(jLabel47, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 10, 80, 110));

        txt_pack_colinos.setEditable(false);
        txt_pack_colinos.setBackground(new java.awt.Color(210, 211, 215));
        txt_pack_colinos.setText("LISTERINE + CEPILLO");
        txt_pack_colinos.setBorder(null);
        txt_pack_colinos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_pack_colinosActionPerformed(evt);
            }
        });
        jPanel15.add(txt_pack_colinos, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 120, 140, -1));

        txt_precio_pak_colinos.setEditable(false);
        txt_precio_pak_colinos.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_pak_colinos.setText("4.00");
        txt_precio_pak_colinos.setBorder(null);
        txt_precio_pak_colinos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_pak_colinosActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_pak_colinos, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 150, 120, -1));

        jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/jabon en liquido_adobespark_adobespark.png"))); // NOI18N
        jLabel48.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel48MouseClicked(evt);
            }
        });
        jPanel15.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 20, 100, 100));

        txt_jabon_fresa.setEditable(false);
        txt_jabon_fresa.setBackground(new java.awt.Color(210, 211, 215));
        txt_jabon_fresa.setText("JABÓN LIQUIDO FRESA");
        txt_jabon_fresa.setBorder(null);
        txt_jabon_fresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_jabon_fresaActionPerformed(evt);
            }
        });
        jPanel15.add(txt_jabon_fresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 120, 150, -1));

        txt_precio_jabon_fresa.setEditable(false);
        txt_precio_jabon_fresa.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_jabon_fresa.setText("12.00");
        txt_precio_jabon_fresa.setBorder(null);
        txt_precio_jabon_fresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_jabon_fresaActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_jabon_fresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 150, 120, -1));

        jLabel49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/jabon liquido_adobespark_adobespark.png"))); // NOI18N
        jLabel49.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel49MouseClicked(evt);
            }
        });
        jPanel15.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 0, 90, 110));

        txt_jabon_platano.setEditable(false);
        txt_jabon_platano.setBackground(new java.awt.Color(210, 211, 215));
        txt_jabon_platano.setText("JABÓN LIQUIDO PLATANO");
        txt_jabon_platano.setBorder(null);
        txt_jabon_platano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_jabon_platanoActionPerformed(evt);
            }
        });
        jPanel15.add(txt_jabon_platano, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 120, 160, -1));

        txt_precio_jabon_platano.setEditable(false);
        txt_precio_jabon_platano.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_jabon_platano.setText("12.50");
        txt_precio_jabon_platano.setBorder(null);
        txt_precio_jabon_platano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_jabon_platanoActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_jabon_platano, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 150, 120, -1));

        spiner_pack_colinos.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_pack_colinosStateChanged(evt);
            }
        });
        jPanel15.add(spiner_pack_colinos, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 180, 120, -1));

        spiner_jabon_fresa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_jabon_fresaStateChanged(evt);
            }
        });
        jPanel15.add(spiner_jabon_fresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 180, 120, -1));

        spiner_jabon_platano.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_jabon_platanoStateChanged(evt);
            }
        });
        jPanel15.add(spiner_jabon_platano, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 180, 120, -1));

        jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/toallas 2_adobespark_adobespark.png"))); // NOI18N
        jLabel50.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel50MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel50MouseEntered(evt);
            }
        });
        jPanel15.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 300, 80, 110));

        jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/toallas 1_adobespark_adobespark.png"))); // NOI18N
        jLabel51.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel51MouseClicked(evt);
            }
        });
        jPanel15.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 300, 80, 110));

        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/desodorante 2_adobespark_adobespark.png"))); // NOI18N
        jLabel52.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel52MouseClicked(evt);
            }
        });
        jPanel15.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 300, 70, 100));

        txt_dove_mujer.setEditable(false);
        txt_dove_mujer.setBackground(new java.awt.Color(210, 211, 215));
        txt_dove_mujer.setText("DESODORANTE DOVE MUJER");
        txt_dove_mujer.setBorder(null);
        txt_dove_mujer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_dove_mujerActionPerformed(evt);
            }
        });
        jPanel15.add(txt_dove_mujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 410, 180, -1));

        txt_cotex.setEditable(false);
        txt_cotex.setBackground(new java.awt.Color(210, 211, 215));
        txt_cotex.setText("TOALLAS COTEX");
        txt_cotex.setBorder(null);
        txt_cotex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cotexActionPerformed(evt);
            }
        });
        jPanel15.add(txt_cotex, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 410, 120, -1));

        txt_nosotras.setEditable(false);
        txt_nosotras.setBackground(new java.awt.Color(210, 211, 215));
        txt_nosotras.setText("TOALLAS NOSOTRAS");
        txt_nosotras.setBorder(null);
        txt_nosotras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nosotrasActionPerformed(evt);
            }
        });
        jPanel15.add(txt_nosotras, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 410, 120, -1));

        txt_precio_nosotras.setEditable(false);
        txt_precio_nosotras.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_nosotras.setText("3.20");
        txt_precio_nosotras.setBorder(null);
        txt_precio_nosotras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_nosotrasActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_nosotras, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 440, 120, -1));

        txt_precio_cotex.setEditable(false);
        txt_precio_cotex.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_cotex.setText("3.50");
        txt_precio_cotex.setBorder(null);
        txt_precio_cotex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_cotexActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_cotex, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 440, 120, -1));

        txt_precio_dove_mujer.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_dove_mujer.setText("11.60");
        txt_precio_dove_mujer.setBorder(null);
        txt_precio_dove_mujer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_dove_mujerActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_dove_mujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 440, 120, -1));

        jLabel53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/desodorante 1_adobespark_adobespark.png"))); // NOI18N
        jLabel53.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel53MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel53MouseEntered(evt);
            }
        });
        jPanel15.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, 90, 110));

        txt_rexona_mujer.setEditable(false);
        txt_rexona_mujer.setBackground(new java.awt.Color(210, 211, 215));
        txt_rexona_mujer.setText("DESODORANTE REXONA HOMBRE");
        txt_rexona_mujer.setBorder(null);
        txt_rexona_mujer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_rexona_mujerActionPerformed(evt);
            }
        });
        jPanel15.add(txt_rexona_mujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, 200, -1));

        txt_precio_rexona_hombre.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_rexona_hombre.setText("12.00");
        txt_precio_rexona_hombre.setBorder(null);
        txt_precio_rexona_hombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_rexona_hombreActionPerformed(evt);
            }
        });
        jPanel15.add(txt_precio_rexona_hombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 440, 120, -1));

        spiner_nosotras.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_nosotrasStateChanged(evt);
            }
        });
        jPanel15.add(spiner_nosotras, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 470, 120, -1));

        spiner_cotex.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_cotexStateChanged(evt);
            }
        });
        jPanel15.add(spiner_cotex, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 470, 120, -1));

        spiner_dove_mujer.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_dove_mujerStateChanged(evt);
            }
        });
        jPanel15.add(spiner_dove_mujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 470, 120, -1));

        espiner_rexona_hombre.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                espiner_rexona_hombreStateChanged(evt);
            }
        });
        jPanel15.add(espiner_rexona_hombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, 120, -1));

        jScrollPane8.setViewportView(jPanel15);

        Productos.addTab("HIGIENE PERSONAL", jScrollPane8);

        jPanel12.setBackground(new java.awt.Color(210, 211, 215));
        jPanel12.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel12.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 10_adobespark_adobespark.png"))); // NOI18N
        jLabel54.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel54MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 10, 60, 110));

        txt_laive.setEditable(false);
        txt_laive.setBackground(new java.awt.Color(210, 211, 215));
        txt_laive.setText("YOGURT LAIVE");
        txt_laive.setBorder(null);
        txt_laive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_laiveActionPerformed(evt);
            }
        });
        jPanel12.add(txt_laive, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 120, -1));

        txt_precio_laive.setEditable(false);
        txt_precio_laive.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_laive.setText("1.30");
        txt_precio_laive.setBorder(null);
        txt_precio_laive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_laiveActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_laive, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 120, -1));

        spiner_laive.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_laiveStateChanged(evt);
            }
        });
        jPanel12.add(spiner_laive, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 120, -1));

        jLabel55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 2_adobespark_adobespark.png"))); // NOI18N
        jLabel55.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel55MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, 70, 100));

        txt_tinto.setEditable(false);
        txt_tinto.setBackground(new java.awt.Color(210, 211, 215));
        txt_tinto.setText("VINO TINTO");
        txt_tinto.setBorder(null);
        txt_tinto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_tintoActionPerformed(evt);
            }
        });
        jPanel12.add(txt_tinto, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 120, 120, -1));

        txt_precio_tinto.setEditable(false);
        txt_precio_tinto.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_tinto.setText("18.00");
        txt_precio_tinto.setBorder(null);
        txt_precio_tinto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_tintoActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_tinto, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, 120, -1));

        jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 3_adobespark_adobespark.png"))); // NOI18N
        jLabel56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel56MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel56, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 10, 100, 100));

        txt_pack_gaseosas.setEditable(false);
        txt_pack_gaseosas.setBackground(new java.awt.Color(210, 211, 215));
        txt_pack_gaseosas.setText("2 GASEOSAS ARO");
        txt_pack_gaseosas.setBorder(null);
        txt_pack_gaseosas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_pack_gaseosasActionPerformed(evt);
            }
        });
        jPanel12.add(txt_pack_gaseosas, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 120, 120, -1));

        txt_precio_pack_gaseosas.setEditable(false);
        txt_precio_pack_gaseosas.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_pack_gaseosas.setText("23.00");
        txt_precio_pack_gaseosas.setBorder(null);
        txt_precio_pack_gaseosas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_pack_gaseosasActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_pack_gaseosas, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 150, 120, -1));

        jLabel57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 4_adobespark_adobespark.png"))); // NOI18N
        jLabel57.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel57MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 90, 110));

        txt_kr.setEditable(false);
        txt_kr.setBackground(new java.awt.Color(210, 211, 215));
        txt_kr.setText("GASEOSA K R");
        txt_kr.setBorder(null);
        txt_kr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_krActionPerformed(evt);
            }
        });
        jPanel12.add(txt_kr, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 120, 120, -1));

        txt_precio_kr.setEditable(false);
        txt_precio_kr.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_kr.setText("4.00");
        txt_precio_kr.setBorder(null);
        txt_precio_kr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_krActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_kr, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 150, 120, -1));

        spiner_tinto.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_tintoStateChanged(evt);
            }
        });
        jPanel12.add(spiner_tinto, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 190, 120, -1));

        spiner_pack_gaseosas.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_pack_gaseosasStateChanged(evt);
            }
        });
        jPanel12.add(spiner_pack_gaseosas, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 190, 120, -1));

        spiner_kr.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_krStateChanged(evt);
            }
        });
        jPanel12.add(spiner_kr, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 180, 120, -1));

        jLabel58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 8_adobespark_adobespark.png"))); // NOI18N
        jLabel58.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel58MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 290, 80, 110));

        jLabel59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 7_adobespark_adobespark.png"))); // NOI18N
        jLabel59.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel59MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel59, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 300, 80, 110));

        jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 6_adobespark_adobespark.png"))); // NOI18N
        jLabel60.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel60MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 310, 90, 100));

        txt_pulp_2.setEditable(false);
        txt_pulp_2.setBackground(new java.awt.Color(210, 211, 215));
        txt_pulp_2.setText("2  PULP GLORIA DE LITRO ");
        txt_pulp_2.setBorder(null);
        txt_pulp_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_pulp_2ActionPerformed(evt);
            }
        });
        jPanel12.add(txt_pulp_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 410, 140, -1));

        txt_yogurt_fresa.setEditable(false);
        txt_yogurt_fresa.setBackground(new java.awt.Color(210, 211, 215));
        txt_yogurt_fresa.setText("YOGURT GLORIA DE LITRO FRESA");
        txt_yogurt_fresa.setBorder(null);
        txt_yogurt_fresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_yogurt_fresaActionPerformed(evt);
            }
        });
        jPanel12.add(txt_yogurt_fresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 410, 190, -1));

        txt_yogurt_durazno.setEditable(false);
        txt_yogurt_durazno.setBackground(new java.awt.Color(210, 211, 215));
        txt_yogurt_durazno.setText("YOGURT GLORIA DE DURAZNO");
        txt_yogurt_durazno.setBorder(null);
        txt_yogurt_durazno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_yogurt_duraznoActionPerformed(evt);
            }
        });
        jPanel12.add(txt_yogurt_durazno, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 410, 160, -1));

        txt_precio_yogurt_durazno.setEditable(false);
        txt_precio_yogurt_durazno.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_yogurt_durazno.setText("3.00");
        txt_precio_yogurt_durazno.setBorder(null);
        txt_precio_yogurt_durazno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_yogurt_duraznoActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_yogurt_durazno, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 440, 120, -1));

        txt_precio_yogurt_fresa.setEditable(false);
        txt_precio_yogurt_fresa.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_yogurt_fresa.setText("3.50");
        txt_precio_yogurt_fresa.setBorder(null);
        txt_precio_yogurt_fresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_yogurt_fresaActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_yogurt_fresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 440, 120, -1));

        txt_precio_pulp_2.setEditable(false);
        txt_precio_pulp_2.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_pulp_2.setText("6.00");
        txt_precio_pulp_2.setBorder(null);
        txt_precio_pulp_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_pulp_2ActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_pulp_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 440, 120, -1));

        jLabel61.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cargando/bebidas 5_adobespark_adobespark.png"))); // NOI18N
        jLabel61.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel61MouseClicked(evt);
            }
        });
        jPanel12.add(jLabel61, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, -1, 110));

        txt_pulp_litro_medio.setEditable(false);
        txt_pulp_litro_medio.setBackground(new java.awt.Color(210, 211, 215));
        txt_pulp_litro_medio.setText("PULP LITRO Y MEDIO ");
        txt_pulp_litro_medio.setBorder(null);
        txt_pulp_litro_medio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_pulp_litro_medioActionPerformed(evt);
            }
        });
        jPanel12.add(txt_pulp_litro_medio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, 130, -1));

        txt_precio_pulp_litro_medio.setEditable(false);
        txt_precio_pulp_litro_medio.setBackground(new java.awt.Color(210, 211, 215));
        txt_precio_pulp_litro_medio.setText("2.90");
        txt_precio_pulp_litro_medio.setBorder(null);
        txt_precio_pulp_litro_medio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_precio_pulp_litro_medioActionPerformed(evt);
            }
        });
        jPanel12.add(txt_precio_pulp_litro_medio, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 440, 120, -1));

        spiner_yogurt_durazno.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_yogurt_duraznoStateChanged(evt);
            }
        });
        jPanel12.add(spiner_yogurt_durazno, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 470, 120, -1));

        spiner_yogurt_fresa.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_yogurt_fresaStateChanged(evt);
            }
        });
        jPanel12.add(spiner_yogurt_fresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 470, 120, -1));

        spiner_pulp_2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_pulp_2StateChanged(evt);
            }
        });
        jPanel12.add(spiner_pulp_2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 470, 120, -1));

        spiner_pul_litro_medio.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spiner_pul_litro_medioStateChanged(evt);
            }
        });
        jPanel12.add(spiner_pul_litro_medio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 120, -1));

        jScrollPane6.setViewportView(jPanel12);

        Productos.addTab("BEBIDAS", jScrollPane6);

        jPanel16.setBackground(new java.awt.Color(210, 211, 215));
        jPanel16.setPreferredSize(new java.awt.Dimension(1100, 700));
        jPanel16.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_desc.setText("SUBTOTAL");
        btn_desc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_descActionPerformed(evt);
            }
        });
        jPanel1.add(btn_desc, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 590, 100, 40));

        jButton1.setText("ELIMINAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 590, 110, 40));

        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabla2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nombre del producto", "Precio por unidad", "Cantidad", "Importe"
            }
        ));
        jScrollPane10.setViewportView(tabla2);

        jPanel5.add(jScrollPane10, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 570, 170));

        jLabel1.setFont(new java.awt.Font("Arial Black", 0, 24)); // NOI18N
        jLabel1.setText("CHINININ & CHIN S.A.C");
        jPanel5.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 370, 40));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("RUC : 20529741601");
        jPanel5.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 30, 170, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("TIENDA DE INSUMOS ");
        jPanel5.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 190, -1));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txt_subtotal.setEditable(false);
        txt_subtotal.setBackground(new java.awt.Color(255, 255, 255));
        txt_subtotal.setText(" 0.00");
        txt_subtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_subtotalActionPerformed(evt);
            }
        });
        jPanel6.add(txt_subtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 400, 70, 30));

        jLabel7.setText("Igv :");
        jPanel6.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 350, -1, -1));

        jLabel8.setText("Subtotal :");
        jPanel6.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 310, 60, 30));

        jLabel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 400, 80, 30));

        jLabel10.setText("  Descuento :");
        jPanel6.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 370, 80, 30));

        txt_total.setEditable(false);
        txt_total.setBackground(new java.awt.Color(255, 255, 255));
        txt_total.setText(" 0.00");
        txt_total.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_totalActionPerformed(evt);
            }
        });
        jPanel6.add(txt_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 310, 70, 30));

        txt_igv.setEditable(false);
        txt_igv.setBackground(new java.awt.Color(255, 255, 255));
        txt_igv.setText(" 0.00");
        txt_igv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_igvActionPerformed(evt);
            }
        });
        jPanel6.add(txt_igv, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 340, 70, 30));

        txt_descuento.setEditable(false);
        txt_descuento.setBackground(new java.awt.Color(255, 255, 255));
        txt_descuento.setText(" 0.00");
        txt_descuento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_descuentoActionPerformed(evt);
            }
        });
        jPanel6.add(txt_descuento, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 370, 70, 30));

        jLabel11.setText("Total :");
        jPanel6.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 410, -1, -1));

        jLabel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 310, 80, 30));

        jLabel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 340, 80, 30));

        jLabel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 370, 80, 30));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("en la calle Cuatro Nro. 226 Cent. Sullana (Cerca a Mercadillo de Sullana)");
        jPanel6.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 480, 580, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Encuentranos en Sullana ");
        jPanel6.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 440, 250, 30));

        jPanel5.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, -10, 640, 530));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Encuentranos en Sullana ");
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 390, 250, -1));

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 50, 880, 520));

        jToggleButton1.setText("TOTAL");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jToggleButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 590, 100, 40));

        btn_desc1.setText("LIMPIAR");
        btn_desc1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_desc1ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_desc1, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 590, 100, 40));

        jPanel16.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 670));

        Productos.addTab("BOLETA", jPanel16);

        getContentPane().add(Productos, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 950, 700));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_cerrarActionPerformed

    private void minimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizarActionPerformed
        this.setExtendedState(1);
    }//GEN-LAST:event_minimizarActionPerformed

    private void ampliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ampliarActionPerformed
        this.setExtendedState(7);
    }//GEN-LAST:event_ampliarActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed

    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseClicked

        Panel f = new Panel();
        f.setVisible(true);
        f.setBounds(100, 110, 1200, 500);
    }//GEN-LAST:event_jButton8MouseClicked

    private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseClicked
        calculadora f = new calculadora();
        f.setVisible(true);
    }//GEN-LAST:event_jButton9MouseClicked

    private void btn_descActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_descActionPerformed
        double total = Double.parseDouble(txt_total.getText());

        Productos t = new Productos(total);

        txt_descuento.setText(String.format("%.2f", t.CalcularDescuento()));

        txt_subtotal.setText(String.format("%.2f", t.CalculartotalDes()));
        txt_igv.setText(String.format("%.2f", t.CalcularIgv()));

    }//GEN-LAST:event_btn_descActionPerformed

    private void spiner_pul_litro_medioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_pul_litro_medioStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_pul_litro_medioStateChanged

    private void spiner_pulp_2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_pulp_2StateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_pulp_2StateChanged

    private void spiner_yogurt_fresaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_yogurt_fresaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_yogurt_fresaStateChanged

    private void spiner_yogurt_duraznoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_yogurt_duraznoStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_yogurt_duraznoStateChanged

    private void txt_precio_pulp_litro_medioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_pulp_litro_medioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_pulp_litro_medioActionPerformed

    private void txt_pulp_litro_medioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_pulp_litro_medioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_pulp_litro_medioActionPerformed

    private void jLabel61MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel61MouseClicked
        int cantidad = (int) spiner_pul_litro_medio.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_pulp_litro_medio.getText()); //precio
        String nombre = txt_pulp_litro_medio.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);

        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel61MouseClicked

    private void txt_precio_pulp_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_pulp_2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_pulp_2ActionPerformed

    private void txt_precio_yogurt_fresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_yogurt_fresaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_yogurt_fresaActionPerformed

    private void txt_precio_yogurt_duraznoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_yogurt_duraznoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_yogurt_duraznoActionPerformed

    private void txt_yogurt_duraznoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_yogurt_duraznoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_yogurt_duraznoActionPerformed

    private void txt_yogurt_fresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_yogurt_fresaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_yogurt_fresaActionPerformed

    private void txt_pulp_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_pulp_2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_pulp_2ActionPerformed

    private void jLabel60MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel60MouseClicked
        int cantidad = (int) spiner_pulp_2.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_pulp_2.getText()); //precio
        String nombre = txt_pulp_2.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);

        mostrar();
    }//GEN-LAST:event_jLabel60MouseClicked

    private void jLabel59MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel59MouseClicked
        int cantidad = (int) spiner_yogurt_fresa.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_yogurt_fresa.getText()); //precio
        String nombre = txt_yogurt_fresa.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel59MouseClicked

    private void jLabel58MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel58MouseClicked
        int cantidad = (int) spiner_yogurt_durazno.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_yogurt_durazno.getText()); //precio
        String nombre = txt_yogurt_durazno.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel58MouseClicked

    private void spiner_krStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_krStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_krStateChanged

    private void spiner_pack_gaseosasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_pack_gaseosasStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_pack_gaseosasStateChanged

    private void spiner_tintoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_tintoStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_tintoStateChanged

    private void txt_precio_krActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_krActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_krActionPerformed

    private void txt_krActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_krActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_krActionPerformed

    private void jLabel57MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel57MouseClicked
        int cantidad = (int) spiner_kr.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_kr.getText()); //precio
        String nombre = txt_kr.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel57MouseClicked

    private void txt_precio_pack_gaseosasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_pack_gaseosasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_pack_gaseosasActionPerformed

    private void txt_pack_gaseosasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_pack_gaseosasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_pack_gaseosasActionPerformed

    private void jLabel56MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel56MouseClicked
        int cantidad = (int) spiner_pack_gaseosas.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_pack_gaseosas.getText()); //precio
        String nombre = txt_pack_gaseosas.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel56MouseClicked

    private void txt_precio_tintoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_tintoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_tintoActionPerformed

    private void txt_tintoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_tintoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_tintoActionPerformed

    private void jLabel55MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel55MouseClicked
        int cantidad = (int) spiner_tinto.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_tinto.getText()); //precio
        String nombre = txt_tinto.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel55MouseClicked

    private void spiner_laiveStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_laiveStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_laiveStateChanged

    private void txt_precio_laiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_laiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_laiveActionPerformed

    private void txt_laiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_laiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_laiveActionPerformed

    private void jLabel54MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel54MouseClicked
        int cantidad = (int) spiner_laive.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_laive.getText()); //precio
        String nombre = txt_laive.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel54MouseClicked

    private void espiner_rexona_hombreStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_espiner_rexona_hombreStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_espiner_rexona_hombreStateChanged

    private void spiner_dove_mujerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_dove_mujerStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_dove_mujerStateChanged

    private void spiner_cotexStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_cotexStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_cotexStateChanged

    private void spiner_nosotrasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_nosotrasStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_nosotrasStateChanged

    private void txt_precio_rexona_hombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_rexona_hombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_rexona_hombreActionPerformed

    private void txt_rexona_mujerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_rexona_mujerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_rexona_mujerActionPerformed

    private void jLabel53MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel53MouseClicked
        int cantidad = (int) espiner_rexona_hombre.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_rexona_hombre.getText()); //precio
        String nombre = txt_rexona_mujer.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel53MouseClicked

    private void txt_precio_dove_mujerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_dove_mujerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_dove_mujerActionPerformed

    private void txt_precio_cotexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_cotexActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_cotexActionPerformed

    private void txt_precio_nosotrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_nosotrasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_nosotrasActionPerformed

    private void txt_nosotrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nosotrasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nosotrasActionPerformed

    private void txt_cotexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cotexActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_cotexActionPerformed

    private void txt_dove_mujerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_dove_mujerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_dove_mujerActionPerformed

    private void jLabel52MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel52MouseClicked
        int cantidad = (int) spiner_dove_mujer.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_dove_mujer.getText()); //precio
        String nombre = txt_dove_mujer.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel52MouseClicked

    private void jLabel51MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel51MouseClicked
        int cantidad = (int) spiner_cotex.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_cotex.getText()); //precio
        String nombre = txt_cotex.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel51MouseClicked

    private void jLabel50MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel50MouseClicked
        int cantidad = (int) spiner_nosotras.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_nosotras.getText()); //precio
        String nombre = txt_nosotras.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel50MouseClicked

    private void spiner_jabon_platanoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_jabon_platanoStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_jabon_platanoStateChanged

    private void spiner_jabon_fresaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_jabon_fresaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_jabon_fresaStateChanged

    private void spiner_pack_colinosStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_pack_colinosStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_pack_colinosStateChanged

    private void txt_precio_jabon_platanoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_jabon_platanoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_jabon_platanoActionPerformed

    private void txt_jabon_platanoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_jabon_platanoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_jabon_platanoActionPerformed

    private void jLabel49MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel49MouseClicked
        int cantidad = (int) spiner_jabon_platano.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_jabon_platano.getText()); //precio
        String nombre = txt_jabon_platano.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);

        mostrar();
    }//GEN-LAST:event_jLabel49MouseClicked

    private void txt_precio_jabon_fresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_jabon_fresaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_jabon_fresaActionPerformed

    private void txt_jabon_fresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_jabon_fresaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_jabon_fresaActionPerformed

    private void jLabel48MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel48MouseClicked
        int cantidad = (int) spiner_jabon_fresa.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_jabon_fresa.getText()); //precio
        String nombre = txt_jabon_fresa.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel48MouseClicked

    private void txt_precio_pak_colinosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_pak_colinosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_pak_colinosActionPerformed

    private void txt_pack_colinosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_pack_colinosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_pack_colinosActionPerformed

    private void jLabel47MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel47MouseClicked
        int cantidad = (int) spiner_pack_colinos.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_pak_colinos.getText()); //precio
        String nombre = txt_pack_colinos.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel47MouseClicked

    private void spiner_colgateStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_colgateStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_colgateStateChanged

    private void txt_precio_colgateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_colgateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_colgateActionPerformed

    private void txt_colgateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_colgateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_colgateActionPerformed

    private void jLabel46MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel46MouseClicked
        int cantidad = (int) spiner_colgate.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_colgate.getText()); //precio
        String nombre = txt_colgate.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel46MouseClicked

    private void spiner_leche_nestleStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_nestleStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_nestleStateChanged

    private void spiner_leche_liveStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_liveStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_liveStateChanged

    private void spiner_leche_vitaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_vitaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_vitaStateChanged

    private void spiner_leche_azulStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_azulStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_azulStateChanged

    private void txt_precio_leche_nestleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_nestleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_nestleActionPerformed

    private void txt_leche_nestleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_nestleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_nestleActionPerformed

    private void jLabel45MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel45MouseClicked
        int cantidad = (int) spiner_leche_nestle.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_nestle.getText()); //precio
        String nombre = txt_leche_nestle.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel45MouseClicked

    private void txt_precio_leche_liveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_liveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_liveActionPerformed

    private void txt_precio_leche_vitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_vitaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_vitaActionPerformed

    private void txt_precio_leche_azulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_azulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_azulActionPerformed

    private void txt_leche_azulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_azulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_azulActionPerformed

    private void txt_leche_vitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_vitaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_vitaActionPerformed

    private void txt_leche_liveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_liveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_liveActionPerformed

    private void jLabel44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel44MouseClicked
        int cantidad = (int) spiner_leche_live.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_live.getText()); //precio
        String nombre = txt_leche_live.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel44MouseClicked

    private void jLabel43MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel43MouseClicked
        int cantidad = (int) spiner_leche_vita.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_vita.getText()); //precio
        String nombre = txt_leche_vita.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel43MouseClicked

    private void jLabel42MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel42MouseClicked
        int cantidad = (int) spiner_leche_azul.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_azul.getText()); //precio
        String nombre = txt_leche_azul.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel42MouseClicked

    private void spiner_leche_rojaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_rojaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_rojaStateChanged

    private void spiner_sin_lactosaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_sin_lactosaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_sin_lactosaStateChanged

    private void spiner_leche_amarillaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_amarillaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_amarillaStateChanged

    private void txt_precio_leche_rojaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_rojaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_rojaActionPerformed

    private void txt_leche_rojaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_rojaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_rojaActionPerformed

    private void jLabel41MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel41MouseClicked
        int cantidad = (int) spiner_leche_roja.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_roja.getText()); //precio
        String nombre = txt_leche_roja.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel41MouseClicked

    private void txt_precio_sin_lactosaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_sin_lactosaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_sin_lactosaActionPerformed

    private void txt_sin_lactosaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_sin_lactosaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_sin_lactosaActionPerformed

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
        int cantidad = (int) spiner_sin_lactosa.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_sin_lactosa.getText()); //precio
        String nombre = txt_sin_lactosa.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel40MouseClicked

    private void txt_precio_leche_amarillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_amarillaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_amarillaActionPerformed

    private void txt_leche_amarillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_amarillaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_amarillaActionPerformed

    private void jLabel39MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseClicked
        int cantidad = (int) spiner_leche_amarilla.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_amarilla.getText()); //precio
        String nombre = txt_leche_amarilla.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel39MouseClicked

    private void spiner_leche_celesteStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_leche_celesteStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_leche_celesteStateChanged

    private void txt_precio_leche_celesteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_leche_celesteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_leche_celesteActionPerformed

    private void txt_leche_celesteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_leche_celesteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_leche_celesteActionPerformed

    private void jLabel38MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel38MouseClicked
        int cantidad = (int) spiner_leche_celeste.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_leche_celeste.getText()); //precio
        String nombre = txt_leche_celeste.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel38MouseClicked

    private void spiner_cartavioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_cartavioStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_cartavioStateChanged

    private void spiner_menestra2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_menestra2StateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_menestra2StateChanged

    private void spiner_azucar_casaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_azucar_casaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_azucar_casaStateChanged

    private void spiner_azucarStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_azucarStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_azucarStateChanged

    private void txt_precio_cartavioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_cartavioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_cartavioActionPerformed

    private void txt_cartavioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cartavioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_cartavioActionPerformed

    private void jLabel37MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel37MouseClicked
        int cantidad = (int) spiner_cartavio.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_cartavio.getText()); //precio
        String nombre = txt_cartavio.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel37MouseClicked

    private void txt_precio_menestra2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_menestra2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_menestra2ActionPerformed

    private void txt_precio_azucar_casaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_azucar_casaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_azucar_casaActionPerformed

    private void txt_precio_azucarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_azucarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_azucarActionPerformed

    private void txt_azucarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_azucarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_azucarActionPerformed

    private void txt_azucar_casaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_azucar_casaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_azucar_casaActionPerformed

    private void txt_menestra2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_menestra2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_menestra2ActionPerformed

    private void jLabel36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel36MouseClicked
        int cantidad = (int) spiner_casserita.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_casserita.getText()); //precio
        String nombre = txt_casserita.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel36MouseClicked

    private void jLabel35MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel35MouseClicked
        int cantidad = (int) spiner_menestra.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_menestra.getText()); //precio
        String nombre = txt_menestra.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel35MouseClicked

    private void jLabel34MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel34MouseClicked
        int cantidad = (int) spiner_azucar.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_azucar.getText()); //precio
        String nombre = txt_azucar.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel34MouseClicked

    private void spiner_menestraStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_menestraStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_menestraStateChanged

    private void spiner_aro2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_aro2StateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_aro2StateChanged

    private void spiner_aroStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_aroStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_aroStateChanged

    private void txt_precio_menestraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_menestraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_menestraActionPerformed

    private void txt_menestraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_menestraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_menestraActionPerformed

    private void jLabel33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel33MouseClicked
        int cantidad = (int) spiner_aro2.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_aro2.getText()); //precio
        String nombre = txt_aro2.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel33MouseClicked

    private void txt_precio_aro2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_aro2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_aro2ActionPerformed

    private void txt_aro2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_aro2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_aro2ActionPerformed

    private void jLabel32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel32MouseClicked
        int cantidad = (int) spiner_azucar_casa.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_azucar_casa.getText()); //precio
        String nombre = txt_azucar_casa.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel32MouseClicked

    private void txt_precio_aroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_aroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_aroActionPerformed

    private void txt_aroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_aroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_aroActionPerformed

    private void jLabel31MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel31MouseClicked
        int cantidad = (int) spiner_aro.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_aro.getText()); //precio
        String nombre = txt_aro.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel31MouseClicked

    private void spiner_casseritaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_casseritaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_casseritaStateChanged

    private void txt_precio_casseritaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_casseritaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_casseritaActionPerformed

    private void txt_casseritaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_casseritaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_casseritaActionPerformed

    private void jLabel30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseClicked
        int cantidad = (int) spiner_menestra2.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_menestra2.getText()); //precio
        String nombre = txt_menestra2.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel30MouseClicked

    private void spiner_buon_nataleStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_buon_nataleStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_buon_nataleStateChanged

    private void spiner_gloriaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_gloriaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_gloriaStateChanged

    private void spiner_santiagoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_santiagoStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_santiagoStateChanged

    private void spiner_bimboStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_bimboStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_bimboStateChanged

    private void txt_precio_nataleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_nataleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_nataleActionPerformed

    private void txt_nataleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nataleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nataleActionPerformed

    private void jLabel29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel29MouseClicked
        int cantidad = (int) spiner_buon_natale.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_natale.getText()); //precio
        String nombre = txt_natale.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel29MouseClicked

    private void txt_precio_gloriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_gloriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_gloriaActionPerformed

    private void txt_precio_santiagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_santiagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_santiagoActionPerformed

    private void txt_precio_bimboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_bimboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_bimboActionPerformed

    private void txt_bimboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_bimboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_bimboActionPerformed

    private void txt_santiagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_santiagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_santiagoActionPerformed

    private void txt_gloriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_gloriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_gloriaActionPerformed

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
        int cantidad = (int) spiner_gloria.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_gloria.getText()); //precio
        String nombre = txt_gloria.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel28MouseClicked

    private void jLabel27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel27MouseClicked
        int cantidad = (int) spiner_santiago.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_santiago.getText()); //precio
        String nombre = txt_santiago.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel27MouseClicked

    private void jLabel26MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel26MouseClicked
        int cantidad = (int) spiner_bimbo.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_bimbo.getText()); //precio
        String nombre = txt_bimbo.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel26MouseClicked

    private void spiner_wintersStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_wintersStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_wintersStateChanged

    private void spiner_florStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_florStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_florStateChanged

    private void spiner_chotoxStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_chotoxStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_chotoxStateChanged

    private void txt_precio_wintersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_wintersActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_wintersActionPerformed

    private void txt_wintersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_wintersActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_wintersActionPerformed

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked
        int cantidad = (int) spiner_winters.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_winters.getText()); //precio
        String nombre = txt_winters.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel21MouseClicked

    private void txt_precio_florActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_florActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_florActionPerformed

    private void txt_florActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_florActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_florActionPerformed

    private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
        int cantidad = (int) spiner_flor.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_flor.getText()); //precio
        String nombre = txt_flor.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel18MouseClicked

    private void txt_precio_chotoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_chotoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_chotoxActionPerformed

    private void txt_chotoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_chotoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_chotoxActionPerformed

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
        int cantidad = (int) spiner_chotox.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_chotox.getText()); //precio
        String nombre = txt_chotox.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel20MouseClicked

    private void spiner_donofrioStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_donofrioStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_donofrioStateChanged

    private void txt_precio_donofrioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_donofrioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_donofrioActionPerformed

    private void txt_donofrioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_donofrioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_donofrioActionPerformed

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
        int cantidad = (int) spiner_donofrio.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_donofrio.getText()); //precio
        String nombre = txt_donofrio.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_jLabel16MouseClicked

    private void imagen_durazno_aricaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_durazno_aricaMouseClicked
        int cantidad = (int) spiner_africa.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_africa.getText()); //precio
        String nombre = txt_africa.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_durazno_aricaMouseClicked

    private void txt_africaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_africaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_africaActionPerformed

    private void txt_precio_africaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_africaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_africaActionPerformed

    private void spiner_africaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_africaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_africaStateChanged

    private void imagen_durazno_timonelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_durazno_timonelMouseClicked
        int cantidad = (int) spiner_timonel.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_timonel.getText()); //precio
        String nombre = txt_timonel.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_durazno_timonelMouseClicked

    private void txt_timonelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_timonelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_timonelActionPerformed

    private void txt_precio_timonelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_timonelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_timonelActionPerformed

    private void spiner_timonelStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_timonelStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_timonelStateChanged

    private void spiner_chilenosStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_chilenosStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_chilenosStateChanged

    private void imagen_chilenosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_chilenosMouseClicked
        int cantidad = (int) spiner_chilenos.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_chinelos.getText()); //precio
        String nombre = txt_chilenos.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_chilenosMouseClicked

    private void txt_chilenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_chilenosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_chilenosActionPerformed

    private void txt_precio_chinelosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_chinelosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_chinelosActionPerformed

    private void imagen_sardinas_hatchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_sardinas_hatchMouseClicked
        int cantidad = (int) spiner_sardinas.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_sardinas.getText()); //precio
        String nombre = txt_sardinas_hatch.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_sardinas_hatchMouseClicked

    private void txt_sardinas_hatchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_sardinas_hatchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_sardinas_hatchActionPerformed

    private void txt_precio_sardinasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_sardinasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_sardinasActionPerformed

    private void spiner_sardinasStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_sardinasStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_sardinasStateChanged

    private void imagen_atun_unoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_atun_unoMouseClicked
        int cantidad = (int) spiner_vega.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_vega.getText()); //precio
        String nombre = txt_atun_vega.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_atun_unoMouseClicked

    private void spiner_aceitunaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_aceitunaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_aceitunaStateChanged

    private void imagen_sardinasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_sardinasMouseClicked
        int cantidad = (int) spiner_campiñones.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_champiñones.getText()); //precio
        String nombre = txt_champiñones.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_sardinasMouseClicked

    private void txt_champiñonesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_champiñonesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_champiñonesActionPerformed

    private void txt_precio_champiñonesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_champiñonesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_champiñonesActionPerformed

    private void spiner_campiñonesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_campiñonesStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_campiñonesStateChanged

    private void txt_cardinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cardinalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_cardinalActionPerformed

    private void txt_precio_cardinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_cardinalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_cardinalActionPerformed

    private void spiner_cardinalStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_cardinalStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_cardinalStateChanged

    private void imagen_atun_cartinalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_atun_cartinalMouseClicked
        int cantidad = (int) spiner_cardinal.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_cardinal.getText()); //precio
        String nombre = txt_cardinal.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_atun_cartinalMouseClicked

    private void txt_atun_vegaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_atun_vegaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_atun_vegaActionPerformed

    private void txt_precio_vegaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_vegaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_vegaActionPerformed

    private void spiner_vegaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spiner_vegaStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_spiner_vegaStateChanged

    private void imagen_aceitunaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imagen_aceitunaMouseClicked

        int cantidad = (int) spiner_aceituna.getValue(); // cantidad
        double precio_prod = Double.parseDouble(txt_precio_pan3.getText()); //precio
        String nombre = txt_aceituna.getText(); //nombre del producto
        Productos a = new Productos(nombre, precio_prod, cantidad);
        listpro.add(a);
        mostrar();
    }//GEN-LAST:event_imagen_aceitunaMouseClicked

    private void txt_aceitunaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_aceitunaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_aceitunaActionPerformed

    private void txt_precio_pan3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_precio_pan3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_precio_pan3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (tabla2.getSelectedRow() != -1) {
            DefaultTableModel modelo = (DefaultTableModel) tabla2.getModel();

            modelo.removeRow(tabla2.getSelectedRow());

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        suma();

    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jLabel53MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel53MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel53MouseEntered

    private void jLabel50MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel50MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel50MouseEntered

    private void txt_subtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_subtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_subtotalActionPerformed

    private void txt_totalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_totalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_totalActionPerformed

    private void txt_igvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_igvActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_igvActionPerformed

    private void txt_descuentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_descuentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_descuentoActionPerformed

    private void btn_desc1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_desc1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_desc1ActionPerformed

    public void mostrar() {
        matriz = new Object[Arbol.cuentaNodos(listpro.getRaiz())][4];
        contador = 0;
        listpro.inorder(listpro.getRaiz(), new AddContend() {
            @Override
            public void addmatix(Comparable elemen) {
                Productos pro = (Productos) elemen;
                matriz[contador][0] = pro.getNombre();
                matriz[contador][1] = pro.getPrecio();
                matriz[contador][2] = pro.getCantidad();
                matriz[contador][3] = pro.calcularMontoU();
                contador++;
            }
        });
        tabla2.setModel(new javax.swing.table.DefaultTableModel(
                matriz,
                new String[]{
                    "Nombre del producto", "Precio Unidad", "Cantidad", "Importe"
                }
        ));
    }

    public void suma() {
        double precio = 0;

        for (int i = 0; i < tabla2.getRowCount(); i++) {

            double a = (double) tabla2.getValueAt(i, 3);

            precio += a;
        }

        txt_total.setText(String.valueOf(precio));

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane Productos;
    private javax.swing.JButton ampliar;
    private javax.swing.JButton btn_desc;
    private javax.swing.JButton btn_desc1;
    private javax.swing.JButton cerrar;
    private javax.swing.JSpinner espiner_rexona_hombre;
    private javax.swing.JLabel imagen_aceituna;
    private javax.swing.JLabel imagen_atun_cartinal;
    private javax.swing.JLabel imagen_atun_uno;
    private javax.swing.JLabel imagen_chilenos;
    private javax.swing.JLabel imagen_durazno_arica;
    private javax.swing.JLabel imagen_durazno_timonel;
    private javax.swing.JLabel imagen_sardinas;
    private javax.swing.JLabel imagen_sardinas_hatch;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JButton minimizar;
    private javax.swing.JSpinner spiner_aceituna;
    private javax.swing.JSpinner spiner_africa;
    private javax.swing.JSpinner spiner_aro;
    private javax.swing.JSpinner spiner_aro2;
    private javax.swing.JSpinner spiner_azucar;
    private javax.swing.JSpinner spiner_azucar_casa;
    private javax.swing.JSpinner spiner_bimbo;
    private javax.swing.JSpinner spiner_buon_natale;
    private javax.swing.JSpinner spiner_campiñones;
    private javax.swing.JSpinner spiner_cardinal;
    private javax.swing.JSpinner spiner_cartavio;
    private javax.swing.JSpinner spiner_casserita;
    private javax.swing.JSpinner spiner_chilenos;
    private javax.swing.JSpinner spiner_chotox;
    private javax.swing.JSpinner spiner_colgate;
    private javax.swing.JSpinner spiner_cotex;
    private javax.swing.JSpinner spiner_donofrio;
    private javax.swing.JSpinner spiner_dove_mujer;
    private javax.swing.JSpinner spiner_flor;
    private javax.swing.JSpinner spiner_gloria;
    private javax.swing.JSpinner spiner_jabon_fresa;
    private javax.swing.JSpinner spiner_jabon_platano;
    private javax.swing.JSpinner spiner_kr;
    private javax.swing.JSpinner spiner_laive;
    private javax.swing.JSpinner spiner_leche_amarilla;
    private javax.swing.JSpinner spiner_leche_azul;
    private javax.swing.JSpinner spiner_leche_celeste;
    private javax.swing.JSpinner spiner_leche_live;
    private javax.swing.JSpinner spiner_leche_nestle;
    private javax.swing.JSpinner spiner_leche_roja;
    private javax.swing.JSpinner spiner_leche_vita;
    private javax.swing.JSpinner spiner_menestra;
    private javax.swing.JSpinner spiner_menestra2;
    private javax.swing.JSpinner spiner_nosotras;
    private javax.swing.JSpinner spiner_pack_colinos;
    private javax.swing.JSpinner spiner_pack_gaseosas;
    private javax.swing.JSpinner spiner_pul_litro_medio;
    private javax.swing.JSpinner spiner_pulp_2;
    private javax.swing.JSpinner spiner_santiago;
    private javax.swing.JSpinner spiner_sardinas;
    private javax.swing.JSpinner spiner_sin_lactosa;
    private javax.swing.JSpinner spiner_timonel;
    private javax.swing.JSpinner spiner_tinto;
    private javax.swing.JSpinner spiner_vega;
    private javax.swing.JSpinner spiner_winters;
    private javax.swing.JSpinner spiner_yogurt_durazno;
    private javax.swing.JSpinner spiner_yogurt_fresa;
    private javax.swing.JTable tabla2;
    private javax.swing.JTextField txt_aceituna;
    private javax.swing.JTextField txt_africa;
    private javax.swing.JTextField txt_aro;
    private javax.swing.JTextField txt_aro2;
    private javax.swing.JTextField txt_atun_vega;
    private javax.swing.JTextField txt_azucar;
    private javax.swing.JTextField txt_azucar_casa;
    private javax.swing.JTextField txt_bimbo;
    private javax.swing.JTextField txt_cardinal;
    private javax.swing.JTextField txt_cartavio;
    private javax.swing.JTextField txt_casserita;
    private javax.swing.JTextField txt_champiñones;
    private javax.swing.JTextField txt_chilenos;
    private javax.swing.JTextField txt_chotox;
    private javax.swing.JTextField txt_colgate;
    private javax.swing.JTextField txt_cotex;
    private javax.swing.JTextField txt_descuento;
    private javax.swing.JTextField txt_donofrio;
    private javax.swing.JTextField txt_dove_mujer;
    private javax.swing.JTextField txt_flor;
    private javax.swing.JTextField txt_gloria;
    private javax.swing.JTextField txt_igv;
    private javax.swing.JTextField txt_jabon_fresa;
    private javax.swing.JTextField txt_jabon_platano;
    private javax.swing.JTextField txt_kr;
    private javax.swing.JTextField txt_laive;
    private javax.swing.JTextField txt_leche_amarilla;
    private javax.swing.JTextField txt_leche_azul;
    private javax.swing.JTextField txt_leche_celeste;
    private javax.swing.JTextField txt_leche_live;
    private javax.swing.JTextField txt_leche_nestle;
    private javax.swing.JTextField txt_leche_roja;
    private javax.swing.JTextField txt_leche_vita;
    private javax.swing.JTextField txt_menestra;
    private javax.swing.JTextField txt_menestra2;
    private javax.swing.JTextField txt_natale;
    private javax.swing.JTextField txt_nosotras;
    private javax.swing.JTextField txt_pack_colinos;
    private javax.swing.JTextField txt_pack_gaseosas;
    private javax.swing.JTextField txt_precio_africa;
    private javax.swing.JTextField txt_precio_aro;
    private javax.swing.JTextField txt_precio_aro2;
    private javax.swing.JTextField txt_precio_azucar;
    private javax.swing.JTextField txt_precio_azucar_casa;
    private javax.swing.JTextField txt_precio_bimbo;
    private javax.swing.JTextField txt_precio_cardinal;
    private javax.swing.JTextField txt_precio_cartavio;
    private javax.swing.JTextField txt_precio_casserita;
    private javax.swing.JTextField txt_precio_champiñones;
    private javax.swing.JTextField txt_precio_chinelos;
    private javax.swing.JTextField txt_precio_chotox;
    private javax.swing.JTextField txt_precio_colgate;
    private javax.swing.JTextField txt_precio_cotex;
    private javax.swing.JTextField txt_precio_donofrio;
    private javax.swing.JTextField txt_precio_dove_mujer;
    private javax.swing.JTextField txt_precio_flor;
    private javax.swing.JTextField txt_precio_gloria;
    private javax.swing.JTextField txt_precio_jabon_fresa;
    private javax.swing.JTextField txt_precio_jabon_platano;
    private javax.swing.JTextField txt_precio_kr;
    private javax.swing.JTextField txt_precio_laive;
    private javax.swing.JTextField txt_precio_leche_amarilla;
    private javax.swing.JTextField txt_precio_leche_azul;
    private javax.swing.JTextField txt_precio_leche_celeste;
    private javax.swing.JTextField txt_precio_leche_live;
    private javax.swing.JTextField txt_precio_leche_nestle;
    private javax.swing.JTextField txt_precio_leche_roja;
    private javax.swing.JTextField txt_precio_leche_vita;
    private javax.swing.JTextField txt_precio_menestra;
    private javax.swing.JTextField txt_precio_menestra2;
    private javax.swing.JTextField txt_precio_natale;
    private javax.swing.JTextField txt_precio_nosotras;
    private javax.swing.JTextField txt_precio_pack_gaseosas;
    private javax.swing.JTextField txt_precio_pak_colinos;
    private javax.swing.JTextField txt_precio_pan3;
    private javax.swing.JTextField txt_precio_pulp_2;
    private javax.swing.JTextField txt_precio_pulp_litro_medio;
    private javax.swing.JTextField txt_precio_rexona_hombre;
    private javax.swing.JTextField txt_precio_santiago;
    private javax.swing.JTextField txt_precio_sardinas;
    private javax.swing.JTextField txt_precio_sin_lactosa;
    private javax.swing.JTextField txt_precio_timonel;
    private javax.swing.JTextField txt_precio_tinto;
    private javax.swing.JTextField txt_precio_vega;
    private javax.swing.JTextField txt_precio_winters;
    private javax.swing.JTextField txt_precio_yogurt_durazno;
    private javax.swing.JTextField txt_precio_yogurt_fresa;
    private javax.swing.JTextField txt_pulp_2;
    private javax.swing.JTextField txt_pulp_litro_medio;
    private javax.swing.JTextField txt_rexona_mujer;
    private javax.swing.JTextField txt_santiago;
    private javax.swing.JTextField txt_sardinas_hatch;
    private javax.swing.JTextField txt_sin_lactosa;
    private javax.swing.JTextField txt_subtotal;
    private javax.swing.JTextField txt_timonel;
    private javax.swing.JTextField txt_tinto;
    private javax.swing.JTextField txt_total;
    private javax.swing.JTextField txt_winters;
    private javax.swing.JTextField txt_yogurt_durazno;
    private javax.swing.JTextField txt_yogurt_fresa;
    // End of variables declaration//GEN-END:variables
   
}
