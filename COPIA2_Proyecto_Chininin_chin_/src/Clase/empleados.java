package Clase;
import java.util.Objects; 

public class empleados {
    private String dni;
    private String apellidos;       //variables
    private String nombres;
    private String sexo;
    private int edad;
    private String telefono;

    public empleados(String dni, String apellidos, String nombres, String sexo, int edad, String telefono) {
        this.dni = dni;
        this.apellidos = apellidos;
        this.nombres = nombres;         //Constructor
        this.sexo = sexo;
        this.edad = edad;
        this.telefono = telefono  ;
    }
    
    public empleados(String dni){      // Constructor para dni
       this.dni=dni;
       
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

   

    @Override 
    public String toString() { // convierte cualquier tipo de dato a String 
        return "Persona{" + "dni=" + dni + ", apellidos=" + apellidos + ", nombres=" + nombres + ", sexo=" + sexo + ", edad=" + edad + ", telefono =" + telefono  + '}';
    }

  @Override 
    public boolean equals(Object obj) { // buscar los datos en la tabla y si estan los podemos consulta   
        if (this == obj) { // Obj (es una varible de tipo object "DNI")
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) { 
            return false;
        }
        final empleados other = (empleados) obj;
        if(!Objects.equals(this.dni, other.dni)){
            return false;
        }
        return true;

    
    }
    
}
