import java.util.Scanner;

public class App {

    
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese numero");
        int numero = sc.nextInt();
        System.out.println("Su numero es:" + (ispositivo(numero) ? "Positivo" : "Negativo"));
    }

    public static boolean ispositivo(int n) {
        if (n < 0) {
            return true;
        } else {
            return isnegativo(n);
        }

    }

    public static boolean isnegativo(int n) {
        if (n > 0) {
            return false;
        } else {
            return ispositivo(n);
        }

    }
}
