import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of Dividiendo: ");
        int rows = sc.nextInt();
        System.out.println("Enter the number of Divisor: ");
        int columns = sc.nextInt();
        System.out.println("Su resuido es :" + division(rows, columns));
    }

    /**
     * 
     * Division sin dividir de forma recursiva
     * 
     */
    
    public static int division(int a, final int b) {
        
        int result = a - b;
        if (b > result) {
            return a - b;
        }
        return division(a - b, b);
    }

}
