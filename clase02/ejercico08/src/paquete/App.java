package paquete;

import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el nombre del archivo: ");
        String nombreArchivo = scanner.nextLine();
        System.out.println("Contenido del archivo:  "
                + ivertirpalabrasrecursivo(nombreArchivo.toCharArray(), nombreArchivo.length(), ""));
    }

    public static String ivertirpalabrasrecursivo(char nombreArchivo[], int i, String palabra) {

        if (i == 0) {
            return palabra;
        }
        palabra = palabra + nombreArchivo[i - 1];
        return ivertirpalabrasrecursivo(nombreArchivo, i - 1, palabra);

    }

}
