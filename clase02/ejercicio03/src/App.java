import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el numero a evualuar");
        int numero = sc.nextInt();
        System.out.println("La suma es :" + sumarnumeros(numero));
    }

    public static int sumarnumeros(int numero) {
        if (numero <= 1) {
            return numero;
        }
        return numero + sumarnumeros(numero - 1);
    }

}
