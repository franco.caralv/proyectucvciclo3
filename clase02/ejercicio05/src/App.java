import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el primer numero");
        int num1 = sc.nextInt();
        System.out.println("Ingrese el segundo numero");
        int num2 = sc.nextInt();
        System.out.println("EL comun divisor es: " + comundivisorrecursivo(num1, num2));

    }

    public static int comundivisorrecursivo(int num1, int num2) {
        if (num1 == num2) {
            return num1;
        } else if (num1 > num2) {
            return comundivisorrecursivo(num1 - num2, num2);
        } else {
            return comundivisorrecursivo(num1, num2 - num1);
        }
    }

}
