import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc=new Scanner(System.in);
        System.out.println("Ingrese su numero");
        int numero=sc.nextInt();
        System.out.println("Su resultado es: "+ sumadigitosrecursivo(numero));
    }

    public static int sumadigitosrecursivo(int numero){
        if(numero<10){
            return numero;
        }else{
            return numero%10+sumadigitosrecursivo(numero/10);
        }
    }

}
