import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el numuero a averriguar");
        int numero = sc.nextInt();

        System.out.println("Su numero es " + decontenarnumero(numero, 0));

    }

    public static int decontenarnumero(final int number, int contador) {
        contador++;
        if (number < 10) {
            return contador;
        }
        return decontenarnumero(number / 10, contador);
    }

}
