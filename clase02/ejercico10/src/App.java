import java.util.Scanner;

public class App {

    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter el numero ah ingresar");
        int rows = scanner.nextInt();
        int numeros[] = new int[rows];
        for (int i = 0; i < rows; i++) {
            System.out.println((i + 1) + ")Enter el numero ah en la lista");
            numeros[i] = scanner.nextInt();
        }

        System.out.println("Enter el numero multiplicado  " + Sumarvectorrecursivo(1, numeros, numeros.length - 1));
    }

    public static int Sumarvectorrecursivo(int suma, int vector[], int n) {
        if (n == -1) {
            return 0;
        }
        suma = vector[n] * suma;
        if (n == 0) {
            return suma;
        } else {
            return Sumarvectorrecursivo(suma, vector, n - 1);
        }
    }
}
