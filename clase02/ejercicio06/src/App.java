import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        llamadonumeros();
    }

    public static void llamadonumeros() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un numero");
        int numero = sc.nextInt();
        if (numero >= 0) {
            if (vereficado(numero)) {
                System.out.println("El numero es perfecto");
            } else {
                System.out.println("El numero no es perfecto");
            }
            llamadonumeros();
        } else {
            System.out.println("El numero ingresado es negativo");
        }
    }

    public static boolean vereficado(int numero) {
        if (sumadedivisores(numero,numero, 0) == numero) {
            return true;
        }
        return false;
    }

    public static int sumadedivisores(int numero, int numerodevaja, int suma) {
        numerodevaja--;
        if (numerodevaja == 1) {
            return suma + 1;
        }

        if (numero % numerodevaja == 0) {
            suma = suma + numerodevaja;
        }
        return sumadedivisores(numero, numerodevaja, suma);
    }

}
